#!/bin/bash

rsync --dry-run -aHxi / root@192.168.1.102:/ --exclude-from '/root/clone-to-reye/excludelist'
read -p "Proceed?"
while true
do
  case $answer in
   [yY]* ) echo "Okay, will clone now!"
           break;;

   [nN]* ) exit;;

   * )     echo "Dude, just enter Y or N, please."; break ;;
  esac
done

rsync -aHxv / root@192.168.1.102:/ --exclude-from '/root/clone-to-reye/excludelist'
