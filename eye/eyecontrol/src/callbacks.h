#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <gst/gst.h>
#include <gst/video/colorbalance.h>
#include <gst/rtsp-server/rtsp-server.h>
#include "types.h"



void quit(gpointer data);
cbret_t change_color_settings(GstElement** p, const char * what, int new_value, bool increment);
cbret_t change_shutter_speed(GstElement** p, int new_value, bool increment);
cbret_t store_config(ec_settings_t ** settings);

void get_pipeline (GstRTSPMediaFactory *gstrtspmediafactory,
               GstRTSPMedia        *arg1,
               gpointer             user_data);
