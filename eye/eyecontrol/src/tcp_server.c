/*
TCP server that sucks
*/
#include <gst/gst.h>
#include <stdio.h>
#include <string.h>    //strlen
#include <stdlib.h>    //strlen
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <pthread.h> //for threading , link with lpthread
#include "types.h"
#include "configuration.h"
#include "protocol.h"

 
 
//the thread function
void *connection_handler(void *);
 
void * start_serving(void * info)
{
    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
    
    gstinfo_t * gsti = *( (gstinfo_t**) info);
    handlerinfo_t hinfo;
     
    //printf("Info in sock is : %p\n", );
    printf("*Info.loop in sock is : %p\n", *gsti->loop);
    printf("*Info.mainrunning in sock is : %d\n", (*gsti).main_running);
     
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8888 );
     
    // set options
    int yes = 1;
    if ( setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
    {
        perror("setsockopt");
    }     
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return info;
    }
    puts("bind done");
     
    //Listen
    listen(socket_desc , 3);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");

    c = sizeof(struct sockaddr_in);
	pthread_t thread_id;
	
    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        puts("Connection accepted");
        if (!(*gsti).main_running)
        {
            close(client_sock);
            break;
        }
        
        
        hinfo.sock = &client_sock;
        hinfo.gstcontrol = info;
        
        puts("Creating thread...");
        if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &hinfo) < 0 )
        {
            perror("could not create thread");
            return info;
        }
         
        //Now join the thread , so that we dont terminate before the thread
        pthread_join( thread_id , NULL );
        if (!(*gsti).main_running)
            return info;
    }
     
    if (client_sock < 0)
    {
        perror("accept failed");
        return info;
    }
     
    return info;
}
 

command_t parse_msg(char* msg)
{
    command_t comm;
    
    // THE PROTOCOL:
    // EC_SAVE_SETTINGS,
    // EC_SET_SHUTTERSPEED,
    // EC_SET_BRIGHTNESS,
    // EC_SET_CONTRAST,
    // EC_SET_SATURATION,
    // EC_ACTION_INCREMENT,
    // EC_ACTION_SET    
    
    // default value
    comm.cmd = cmd_none;
    comm.increment = true;
    comm.value = -1;
    
    
    protocol cmdbyte = *msg;
    protocol actionbyte = *(msg+1);
    
    printf("command is : %d, %d\n", msg[0], msg[1]);
    printf("command is : %d, %d\n",cmdbyte , actionbyte);
    
    switch (cmdbyte)
    {
        case EC_SET_BRIGHTNESS:
            comm.cmd = cmd_brightness;
            break;
        case EC_SET_CONTRAST:
            comm.cmd = cmd_contrast;
            break;
        case EC_SET_SATURATION:
            comm.cmd = cmd_saturation;
            break;
        case EC_SET_SHUTTERSPEED:
            comm.cmd = cmd_shutter_speed;
            break;
        case EC_SAVE_SETTINGS:
            comm.cmd = cmd_save_config;
            break;
        case EC_QUIT:
            comm.cmd = cmd_quit;
            break;            
    }
    
    switch (actionbyte)
    {
        case EC_ACTION_SET:
            comm.increment = false;
            break;
        case EC_ACTION_INCREMENT:
            comm.increment = true;
            break;
    }
    
    // what about endiannes?
    // little:
    // uint32_t myInt1 = msg[2] + (msg[3] << 8) + (msg[4] << 16) + (msg[5] << 24);
    // the other way around for big.
    // 
    // here we use native:
    //comm.value = *((int32_t *)(msg+2));
    
    comm.value = chartoint32(msg+2);
    printf("Received value: %d\n\n", comm.value);
    return comm;
} 

cbret_t execute_command(command_t cmd, gstinfo_t * gsti)
{
    cbret_t ret;
    
    ret.retval = -1;
    ret.current_value = -1;
    
    switch (cmd.cmd)
    {
        case cmd_brightness:
            ret = (*gsti->change_color_settings)(&(*gsti->pipeline),"brightness",cmd.value,cmd.increment);
            if (ret.retval == 0)
            {
                g_printerr("Updating settings\n");
                if (validate_camera_setting("brightness",ret.current_value))
                    (*(gsti->settings))->brightness = ret.current_value;
                else
                    g_printerr("Something very wrong while updating settings\n");
            }
            break;
            
        case cmd_contrast:
            ret = (*gsti->change_color_settings)(&(*gsti->pipeline),"contrast",cmd.value,cmd.increment);
            if (ret.retval == 0)
            {
                g_printerr("Updating settings\n");
                if (validate_camera_setting("contrast",ret.current_value))
                    (*(gsti->settings))->contrast = ret.current_value;
                else
                    g_printerr("Something very wrong while updating settings\n");
            }
            break;

        case cmd_saturation:
            ret = (*gsti->change_color_settings)(&(*gsti->pipeline),"saturation",cmd.value,cmd.increment);
            if (ret.retval == 0)
            {
                g_printerr("Updating settings\n");
                if (validate_camera_setting("saturation",ret.current_value))
                    (*(gsti->settings))->saturation = ret.current_value;
                else
                    g_printerr("Something very wrong while updating settings\n");
            }
            break;

        case cmd_shutter_speed:
            ret = (*gsti->change_shutter_speed)(&(*gsti->pipeline),cmd.value,cmd.increment);
            if (ret.retval == 0)
            {
                g_printerr("Updating settings\n");
                if (validate_camera_setting("shutter_speed",ret.current_value))
                    (*(gsti->settings))->shutter_speed = ret.current_value;
                else
                    g_printerr("Something very wrong while updating settings\n");
            }            
            break;
            
        case cmd_quit:
            (*gsti->quit)((*gsti->loop));
            ret.retval=0;//to be omplemented
            break;
            
        case cmd_save_config:
            ret = (*gsti->store_config)(&(*gsti->settings));
            
        case cmd_none:
            //(*gsti->quit)((*gsti->loop));
            break;
        default:
            g_printerr("Error parsing command!\n");
    }
    printf("Ret value is: %d\n\n", ret.retval);
    printf("Property value is : %d\n\n", ret.current_value);
    return ret;
}
 
/*
 * This will handle connection for each client
 * */
void *connection_handler(void *info)
{
    //Get the socket descriptor
    handlerinfo_t * hi = (handlerinfo_t*) info;
    
    int sock = *(int*)(hi->sock);
    gstinfo_t * gsti = *( (gstinfo_t**) hi->gstcontrol);
     
    //printf("Info in sock is : %p\n", );
    printf("*Info.loop in handler is : %p\n", *gsti->loop);
    
    int read_size;
    int recv_size;
    char client_message[10];
    char response_message[5];
    //write(sock , message , strlen(message));
     
    //Receive a message from client
    while( 1 )
    {

        //
        read_size = 0;
        recv_size = 0;
        while ( (recv_size = recv(sock , client_message + read_size , 10-read_size , 0)) > 0 )
        {
            printf("Received %d bytes\n",recv_size);
            read_size = recv_size + read_size;
            if (read_size == 10)
                break;
        }
        if (read_size < 10) break;
        
        // check if there is something left?
        //    ioctl(fd,FIONREAD,&bytes_available)
        
		//Send the message back to client
        //write(sock , client_message , strlen(client_message));
		command_t comm = parse_msg(client_message);
        cbret_t ret = execute_command(comm, gsti);
        int8_t funcret = (int8_t) ret.retval;
        int32_t valset = (int32_t) ret.retval;
        
        char *ptr1 = (char*)&funcret;
        response_message[0] = *ptr1;  
        
        //char *ptr2 = (char*)&valset;
        //
        
        char ptr2[4]={0};
        
        int32tochar(&ptr2,valset);
        
        for(int i=1;i<5;++i)     // note da 1 a 5!!
            response_message[i] = ptr2[i-1];
        
        write(sock , response_message , 5);

        
		//clear the message buffers
		memset(client_message, 0, 10);
		memset(response_message, 0, 5);
        printf("Memset done\n");
    }
     
    if(read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("recv failed");
    }
         
    return 0;
} 
