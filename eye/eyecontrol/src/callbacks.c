#include "callbacks.h"
#include "configuration.h"

/*!
 * \brief quit closes the main loop
 * \param data void * pointer to the GMainLoop loop
 * \return None
 *
 * This function is used to exit from the main program.
 */
void quit(gpointer data)
{
   GMainLoop *loop = (GMainLoop*) data;
   g_main_loop_quit(loop);
}

/*!
 * \brief call configuration save
 * \param data void * pointer to the GMainLoop loop
 * \return None
 *
 * This function is saves the configurations to the file
 */
cbret_t store_config(ec_settings_t ** settings)
{
  cbret_t ret;
  ret.current_value = 0;
  ret.retval = 0;
  if (!save_configuration(settings))
    ret.retval = -1;
  return ret;
}


/*!
 * \brief change_shutter_speed This function is used to set the shutter speed
 * \param p GstElement** pointer to the GstElement * pointer containing the pipeline
 * \param new_value int, value to be set or to increment
 * \param increment bool, wether to increment by new_value or set to new_value the shutter speed.
 * \return -1 or -2 if something went wrong, the settting value otherwise.
 *
 * This function changes the shutter speed of the RPi camera.
 */

cbret_t change_shutter_speed(GstElement** p, int new_value, bool increment)
{
  GstElement* pipeline = (GstElement*) *p;
  GstElement* src;
  GstColorBalance* balance;
  gint * gss=NULL;
  int  ss=0;
  cbret_t ret;
  ret.current_value = 0;
  ret.retval = -3;
  g_printerr("Setting shutter speed\n");
  printf("Pipeline is : %p\n", pipeline);

  if (pipeline == NULL)
  {
    ret.retval = -2;
    return ret;
  }
  
  src = gst_bin_get_by_name(GST_BIN(pipeline), "src");
  if (!src) {
    g_printerr("Source element not found\n");
    ret.retval = -2;
    return ret;
  }
  g_printerr("Got source element\n");
  if (increment)
  {
    gss = malloc(sizeof(gint));
    g_object_get(G_OBJECT(src), "shutter-speed", gss, NULL);
    ss=*gss;
    free(gss);
    g_printerr("got ss value %d\n",ss);
    if (ss>0) {
      ss = ss + new_value;
    } else {
      ret.retval = -1;
      return ret;
    }
  } else {
    ss = new_value;
  }
    
  if (!validate_camera_setting("shutter_speed", ss))
  {
    ret.retval = -1;
    return ret;    
  }
  
  g_object_set(G_OBJECT(src), "shutter-speed", ss, NULL);
  
  // get the value back
  gss = malloc(sizeof(gint));
  g_object_get(G_OBJECT(src), "shutter-speed", gss, NULL);
  ss=*gss;
  free(gss);  
  
  if (ss>0)
  {
    ret.retval = 0;
    ret.current_value = ss;
    return ret;
  }
  else
  {
    ret.retval = -1;
    return ret;    
  }
}

/*!
 * \brief switch_off_awb This function is used to set avoid a bug in the rpi firmware
 * \param p GstElement** pointer to the GstElement * pointer containing the pipeline
 * \param new_value int, value to be set or to increment
 * \param increment bool, wether to increment by new_value or set to new_value the shutter speed.
 * \return -1 or -2 if something went wrong, the settting value otherwise.
 *
 * This function changes the shutter speed of the RPi camera.
 */

cbret_t switch_off_awb(GstElement** p, float new_value_r, float new_value_b)
{
  GstElement* pipeline = (GstElement*) *p;
  GstElement* src;
  GstColorBalance* balance;
  gint * gss=NULL;
  int  ss=0;
  cbret_t ret;
  ret.current_value = 0;
  ret.retval = -3;
  
  g_printerr("Switching of AWB\n");
  printf("Pipeline is : %p\n", pipeline);

  if (pipeline == NULL)
  {
    ret.retval = -2;
    return ret;
  }
  
  src = gst_bin_get_by_name(GST_BIN(pipeline), "src");
  if (!src) {
    g_printerr("Source element not found\n");
    ret.retval = -2;
    return ret;
  }
  g_printerr("Got source element\n");
      
  g_object_set(G_OBJECT(src), "awb-mode", 0, NULL);
  g_object_set(G_OBJECT(src), "awb-gain-blue", new_value_b, NULL);
  g_object_set(G_OBJECT(src), "awb-gain-red", new_value_r, NULL);
  
  // get the value back
  gss = malloc(sizeof(gint));
  g_object_get(G_OBJECT(src), "awb-mode", gss, NULL);
  ss=*gss;
  free(gss);  
  
  if (ss==0)
  {
    ret.retval = 0;
    ret.current_value = ss;
    return ret;
  }
  else
  {
    ret.retval = -1;
    return ret;    
  }
}

/*!
 * \brief change_color_settings This function is used to set brightness, saturation and contrast
 * \param p GstElement** pointer to the GstElement * pointer containing the pipeline
 * \param what const char *, used to select among brightness, saturation and contrast
 * \param new_value int, value to be set or to increment
 * \param increment bool, wether to increment by new_value or set to new_value the shutter speed.
 * \return -1 or -2 if something went wrong, the settting value otherwise.
 *
 * This function is used to change color settings like brightness, contrast and saturation.
 */

cbret_t change_color_settings(GstElement** p, const char * what, int new_value, bool increment)
{
  GstElement* pipeline = (GstElement*) *p;
  GstElement* src;
  GstColorBalance* balance;
  char * what_uprc;
  cbret_t ret;
  ret.current_value = 0;
  ret.retval = -3;  
  
  if (pipeline == NULL)
  {
    ret.retval = -2;
    return ret;
  }
  
  src = gst_bin_get_by_name(GST_BIN(pipeline), "src");
  if (!src) {
    g_printerr("Source element not found\n");
    ret.retval = -2;
    return ret;
  }
  
  
  balance = GST_COLOR_BALANCE(src);
  
  const GList* controls;
  GstColorBalanceChannel* channel;
  const GList* item;
  gint index, current_value;
  
  controls = gst_color_balance_list_channels(balance);
  
  if (controls == NULL) {
    g_printerr("There is no list of colorbalance controls\n");
    ret.retval = -1;
    return ret;
  }
  
  printf("Setting property : %s\n", what);
  if (!g_strcmp0(what,"brightness"))
    what_uprc = "BRIGHTNESS";
  else if (!g_strcmp0(what,"contrast"))
    what_uprc = "CONTRAST";
  else if (!g_strcmp0(what,"saturation"))
    what_uprc = "SATURATION";
  else
  {
    gst_object_unref(src);
    ret.retval = -1;
    return ret;
  }
  
  for (item = controls, index = 0; item != NULL;
       item = item->next, ++index) {
    channel = item->data;
    current_value = gst_color_balance_get_value(balance, channel);
    if (!g_strcmp0(channel->label, what_uprc)) {
      
      // if we want to increment by new_value
      if (increment)
        new_value = current_value + new_value;
      
      if (!validate_camera_setting(what, new_value))
      {
        gst_object_unref(src);
        ret.retval = -1;
        return ret;
      }

      // apply settings
      gst_color_balance_set_value(balance, channel, new_value);  
      printf("Setting %s : %d\n", what, new_value);  
      break;
    }
  }
  gst_object_unref(src);
  ret.retval = 0;
  ret.current_value = new_value;
  return ret;
}


// this is used to get the GstElement containing the pipeline.
// We need the pipeline to control options like saturation, brightness etc
void get_pipeline (GstRTSPMediaFactory *gstrtspmediafactory,
               GstRTSPMedia        *arg1,
               gpointer             user_data)
{
  gstinfo_t * gsti = *( (gstinfo_t**) user_data);
  *gsti->pipeline = gst_rtsp_media_get_element(arg1);
  // now set config values
  //printf("Now I should set %s to %d\n", "brightness", (*(gsti->settings))->brightness );  
  
  // TODO : CHECK IF INITIAL VALUES ARE CORRECT!!!
  (*gsti->change_color_settings)(&(*gsti->pipeline),"brightness",(*(gsti->settings))->brightness,FALSE);
  (*gsti->change_color_settings)(&(*gsti->pipeline),"contrast",(*(gsti->settings))->contrast,FALSE);
  (*gsti->change_color_settings)(&(*gsti->pipeline),"saturation",(*(gsti->settings))->saturation,FALSE);
  (*gsti->change_shutter_speed) (&(*gsti->pipeline),(*(gsti->settings))->shutter_speed,FALSE);
  
  //switch_off_awb(&(*gsti->pipeline),1,1.4);
  
}
