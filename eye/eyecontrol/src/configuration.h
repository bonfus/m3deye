#include "types.h"

void set_default_values(ec_settings_t ** settings);

void dump_configuration(ec_settings_t ** settings);

bool load_configuration(ec_settings_t ** settings );


bool save_configuration(ec_settings_t ** settings);


static void parse_config_line (char *var, char * value, ec_settings_t ** settings);

void prepare_pipelines(ec_settings_t ** settings);

bool validate_camera_setting(const char * var, int value);
