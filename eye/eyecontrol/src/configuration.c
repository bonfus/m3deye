#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "configuration.h"
#include "str_tools.h"

#define SHUTTER_SPEED "SS"
#define FRAME_RATE "FR"
#define CONTRAST "CO"
#define SATURATION "SA"
#define BRIGHTNESS "BR"

#define MIN_SHUTTER_SPEED 2000
#define MAX_SHUTTER_SPEED 20000

#define MIN_FRAME_RATE 2
#define MAX_FRAME_RATE 42

#define MIN_CONTRAST 0
#define MAX_CONTRAST 100

#define MIN_BRIGHTNESS 0
#define MAX_BRIGHTNESS 100

#define MIN_SATURATION 0
#define MAX_SATURATION 100



static void parse_config_line (char *var, char * value, ec_settings_t ** settings);

void set_default_values(ec_settings_t ** settings)
{
    ec_settings_t * s = *(settings);
    s->config_file=malloc(sizeof(char)*100);
    s->config_file="./config";
    s->saturation=0;
    s->contrast=0;
    s->brightness=0;
    s->shutter_speed=6000;
    s->frame_rate=25;
    s->num_factories=0;
}

void dump_configuration(ec_settings_t ** settings)
{
    printf("==== CURRENT SETTINGS ====\n");
    ec_settings_t * s = *(settings);
    printf("\tSaturation: %d\n" , s->saturation);
    printf("\tContrast: %d\n" , s->contrast);
    printf("\tBrightness: %d\n" , s->brightness);
    printf("\tShutter speed: %d\n" , s->shutter_speed);
    printf("\tFrame rate: %d\n" , s->frame_rate);
    int i =0;
    printf("\n\tAvailable mountpoints\n" );
    for (i=0;i<s->num_factories;i++)
    {
        printf("\t\t%s, with pipeline: %s\n", s->factories[i].mountpoint, s->factories[i].pipeline );
    }
    printf("==== CURRENT SETTINGS ====\n");
}


bool load_configuration(ec_settings_t ** settings )
{
    FILE * fp;
    char * line = NULL;
    char * var;
    char * value;
    size_t len = 0;
    ssize_t read;
    int counter = 0;
    bool valid = true;
    
    ec_settings_t * s = *(settings);

    fp = fopen(s->config_file, "r");
    if (fp == NULL)
    {
        perror("(!!) Cannot open configuration file");
        return false;
    }
    char * pch;
    while ((read = getline(&line, &len, fp)) != -1) {
        if (*line == '\n' || *line == '#')
            continue;
        
        pch = strtok (line,":");
        counter = 0;
        valid = false;
        while (pch != NULL)
        {
            if (counter == 0)
                var = strdup(pch);
            else if (counter == 1)
            {
                value = strdup(pch);
                valid = true;
            }
            else
            {
                fprintf(stderr,"(!!) Invalid configuration line: %s\n",line);
                valid = false;
            }
            pch = strtok (NULL, ":");
            counter++;
        }
        
        if (valid)
            parse_config_line(var, value, settings);
    }

    fclose(fp);
    if (line) free(line);
    if (var) free(var);
    if (value) free(value);
}

bool save_configuration(ec_settings_t ** settings)
{
    FILE * fp;
    
    ec_settings_t * s = *(settings);
    
    fp = fopen(s->config_file, "w");
    if (fp == NULL)
    {
        perror("(!!) Cannot save configuration file");
        return false;
    }

    fprintf(fp,"saturation: %d\n" , s->saturation);
    fprintf(fp,"contrast: %d\n" , s->contrast);
    fprintf(fp,"brightness: %d\n" , s->brightness);
    fprintf(fp,"shutter_speed: %d\n" , s->shutter_speed);
    fprintf(fp,"frame_rate: %d\n" , s->frame_rate);
    fprintf(fp,"\n");
    int i =0;
   
    for (i=0;i<s->num_factories;i++)
    {
        fprintf(fp,"service: %s|%s\n", s->factories[i].pipeline, s->factories[i].mountpoint);
    }
    fclose(fp);
}

static void parse_config_line (char *var, char * value, ec_settings_t ** settings)
{

    
    ec_settings_t * s = *(settings);
    
	// Brightness
	if (strstr(var, "brightness"))
    {
        if (validate_camera_setting("brightness",atoi(value)))
            s->brightness = atoi(value);
        else
            fprintf(stderr, "(!!) Invalid value %d for brightness\n", atoi(value));
        return;
    }

	// Contrast
	if (strstr(var, "contrast"))
    {
        if (validate_camera_setting("contrast",atoi(value)))
            s->contrast = atoi(value);
        else
            fprintf(stderr, "(!!) Invalid value %d for contrast\n", atoi(value));
        return;
    }

	// Saturation
	if (strstr(var, "saturation"))
    {
        if (validate_camera_setting("saturation",atoi(value)))
            s->saturation = atoi(value);
        else
            fprintf(stderr, "(!!) Invalid value %d for saturation\n", atoi(value));
        return;
    }

	// Framerate
	if (strstr(var, "frame_rate"))
    {
        if (validate_camera_setting("frame_rate",atoi(value)))
            s->frame_rate = atoi(value);
        else
            fprintf(stderr, "(!!) Invalid value %d for frame_rate\n", atoi(value));
        return;
    }
    
	// Shutter_speed
	if (strstr(var, "shutter_speed"))
    {
        if (validate_camera_setting("shutter_speed",atoi(value)))
            s->shutter_speed = atoi(value);
        else
            fprintf(stderr, "(!!) Invalid value %d for shutter_speed\n", atoi(value));
        return;
    }
    
	if (strstr(var, "service"))
    { 
        if (s->num_factories == 0)
        {
            s->factories = malloc(sizeof(rtsp_factory_element_t) * (s->num_factories + 1));
        } else {
            s->factories = realloc(s->factories, sizeof(rtsp_factory_element_t) * (s->num_factories + 1));
        }
        
        if (s->factories == NULL)
        {
            perror ("Cannot allocate factory!");
            return;
        }

        
        char * pch;
        pch = strtok (value,"|");
        if (pch != NULL)
            s->factories[s->num_factories].pipeline = strdup(trim(pch));
        else
        {
            perror("Cannot parse pipeline!\n");
            return;
        }
        
        pch = strtok (NULL,"|");
        if (pch != NULL)
            s->factories[s->num_factories].mountpoint = strdup(trim(pch));
        else
        {
            perror("Cannot parse mountpoint!\n");
            return;
        }        
        s->num_factories++;
        return; 
    }
    
    // if we arrive here there's something strange in this line...
    
    fprintf(stderr, "Error parsing: %s %s\n", var, value);
}

void prepare_pipelines(ec_settings_t ** settings)
{
    ec_settings_t * s = *(settings);
    char * template_pipeline;
    
    for (int i = 0; i < s->num_factories; i++)
    {
        
        // SHUTTER SPEED
        template_pipeline = s->factories[i].pipeline;
        char ss[12];
        sprintf(ss, "%d", s->shutter_speed);
        s->factories[i].pipeline = str_replace(template_pipeline, SHUTTER_SPEED, ss);
        free(template_pipeline);
        
        // FRAME RATE
        template_pipeline = s->factories[i].pipeline;
        char fr[12];
        sprintf(fr, "%d", s->frame_rate);
        s->factories[i].pipeline = str_replace(template_pipeline, FRAME_RATE, ss);
        free(template_pipeline);        
        
        // CONTRAST
        template_pipeline = s->factories[i].pipeline;
        char co[12];
        sprintf(co, "%d", s->contrast);
        s->factories[i].pipeline = str_replace(template_pipeline, CONTRAST, co);
        free(template_pipeline);

        // BRIGHTNESS
        template_pipeline = s->factories[i].pipeline;
        char br[12];
        sprintf(br, "%d", s->brightness);
        s->factories[i].pipeline = str_replace(template_pipeline, BRIGHTNESS, br);
        free(template_pipeline);

        // SATURATION
        template_pipeline = s->factories[i].pipeline;
        char sa[12];
        sprintf(sa, "%d", s->saturation);
        s->factories[i].pipeline = str_replace(template_pipeline, SATURATION, sa);
        free(template_pipeline);

    }
}

bool validate_camera_setting(const char * var, int value)
{
	if (strstr(var, "brightness"))					{ if (MIN_BRIGHTNESS <= value && value  <= MAX_BRIGHTNESS) return true; else return false;}
	if (strstr(var, "saturation"))		            { if (MIN_SATURATION <= value && value  <= MAX_SATURATION) return true; else return false;}
	if (strstr(var, "contrast"))                   { if (MIN_CONTRAST <= value && value  <= MAX_CONTRAST) return true; else return false;}
	if (strstr(var, "frame_rate"))                 { if (MIN_FRAME_RATE <= value && value  <= MAX_FRAME_RATE) return true; else return false;}
	if (strstr(var, "shutter_speed"))                 { if (MIN_SHUTTER_SPEED <= value && value  <= MAX_SHUTTER_SPEED) return true; else return false;}
    perror("(!!) Invalid camera setting specified!\n");
    return false;
}
