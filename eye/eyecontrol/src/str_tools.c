#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char *
trim(char *str)
{
  char *end;

  // ltrim
  while (isspace(*str)||*str=='\n') {
    str++;
  }

  if (*str == 0) // only spaces
    return str;

  // rtrim
  end = str + strlen(str) - 1;
  while (end > str && (isspace(*end)||*end=='\n')) {
    end--;
  }

  // null terminator
  *(end+1) = 0;

  return str;
}

//
// occurrences.c
//
// Copyright (c) 2013 Stephen Mathieson
// MIT licensed
//

/*
 * Get the number of occurrences of `needle` in `haystack`
 */

int occurrences(const char *needle, const char *haystack) {
  if (NULL == needle || NULL == haystack) return -1;

  char *pos = (char *)haystack;
  int i = 0;
  int l = strlen(needle);

  while ((pos = strstr(pos, needle))) {
    pos += l;
    i++;
  }

  return i;
}

//
// str-replace.c
//
// Copyright (c) 2013 Stephen Mathieson
// MIT licensed
//

/*
 * Replace all occurrences of `sub` with `replace` in `str`
 */

char *str_replace(const char *str, const char *sub, const char *replace) {
  char *pos = (char *) str;
  int count = occurrences(sub, str);

  if (0 >= count) return strdup(str);

  int size = (
        strlen(str)
      - (strlen(sub) * count)
      + strlen(replace) * count
    ) + 1;

  char *result = (char *) malloc(size);
  if (NULL == result) return NULL;
  *result='\0';
  char *current;
  while ((current = strstr(pos, sub))) {
    int len = current - pos;
    strncat(result, pos, len);
    strncat(result, replace, strlen(replace));
    pos = current + strlen(sub);
  }

  if (pos != (str + strlen(str))) {
    strncat(result, pos, (str - pos));
  }

  return result;
}
