#include <gst/net/net.h>

GstClock *global_clock;

#define TEST_TYPE_RTSP_MEDIA_FACTORY      (test_rtsp_media_factory_get_type ())
#define TEST_TYPE_RTSP_MEDIA              (test_rtsp_media_get_type ())

GType test_rtsp_media_factory_get_type (void);
GType test_rtsp_media_get_type (void);

static GstRTSPMediaFactory *test_rtsp_media_factory_new (void);
static GstElement *create_pipeline (GstRTSPMediaFactory * factory,
    GstRTSPMedia * media);

typedef struct TestRTSPMediaFactoryClass TestRTSPMediaFactoryClass;
typedef struct TestRTSPMediaFactory TestRTSPMediaFactory;

struct TestRTSPMediaFactoryClass
{
  GstRTSPMediaFactoryClass parent;
};

struct TestRTSPMediaFactory
{
  GstRTSPMediaFactory parent;
};

typedef struct TestRTSPMediaClass TestRTSPMediaClass;
typedef struct TestRTSPMedia TestRTSPMedia;

struct TestRTSPMediaClass
{
  GstRTSPMediaClass parent;
};

struct TestRTSPMedia
{
  GstRTSPMedia parent;
};

GstRTSPMediaFactory *
test_rtsp_media_factory_new (void)
{
  GstRTSPMediaFactory *result;

  result = g_object_new (TEST_TYPE_RTSP_MEDIA_FACTORY, NULL);

  return result;
}

G_DEFINE_TYPE (TestRTSPMediaFactory, test_rtsp_media_factory,
    GST_TYPE_RTSP_MEDIA_FACTORY);

static gboolean custom_setup_rtpbin (GstRTSPMedia * media, GstElement * rtpbin);

static void
test_rtsp_media_factory_class_init (TestRTSPMediaFactoryClass * test_klass)
{
  GstRTSPMediaFactoryClass *mf_klass =
      (GstRTSPMediaFactoryClass *) (test_klass);
  mf_klass->create_pipeline = create_pipeline;
}

static void
test_rtsp_media_factory_init (TestRTSPMediaFactory * factory)
{
}

static GstElement *
create_pipeline (GstRTSPMediaFactory * factory, GstRTSPMedia * media)
{
  GstElement *pipeline;

  pipeline = gst_pipeline_new ("media-pipeline");
  if (global_clock != NULL)
  {
    gst_pipeline_use_clock (GST_PIPELINE (pipeline), global_clock);
  } else {
    g_print ("No clocks for syncronization!!!!!\n");
  }
  gst_rtsp_media_take_pipeline (media, GST_PIPELINE_CAST (pipeline));

  return pipeline;
}

G_DEFINE_TYPE (TestRTSPMedia, test_rtsp_media, GST_TYPE_RTSP_MEDIA);

static void
test_rtsp_media_class_init (TestRTSPMediaClass * test_klass)
{
  GstRTSPMediaClass *klass = (GstRTSPMediaClass *) (test_klass);
  klass->setup_rtpbin = custom_setup_rtpbin;
}

static void
test_rtsp_media_init (TestRTSPMedia * media)
{
}

static gboolean
custom_setup_rtpbin (GstRTSPMedia * media, GstElement * rtpbin)
{
  g_object_set (rtpbin, "ntp-time-source", 3, NULL);
  return TRUE;
}


void sync_clocks (GstRTSPServer *gstrtspserver,
               GstRTSPClient *client,
               gpointer       user_data)
{
  GstRTSPConnection * conn = (GstRTSPConnection*) gst_rtsp_client_get_connection (client);
  g_print ("Connection from client ip %s\n",
        gst_rtsp_connection_get_ip (conn));

  if (global_clock == NULL) {
    g_print ("No clocks already available\n");
  } else {
    g_print ("A clock was present, unref it!.\n");
    g_object_unref(global_clock);
  }
  
  
  //static gint domain = 0;
  global_clock = gst_ptp_clock_new ("net_clock", 0);
  if (global_clock == NULL) {
    g_print ("Failed to create ptp clock, fallback on standard clock\n");
  } else {

    g_print ("Waiting ptp clock...");
    /* Wait for the clock to stabilise */
    gst_clock_wait_for_sync (GST_CLOCK(global_clock), 3e9 ); //3 secs //GST_CLOCK_TIME_NONE
    if (gst_clock_is_synced (global_clock))
    {
        g_print ("Synked with ptp! Accuracy %d ns!\n", (int)(gst_clock_get_resolution (global_clock)));
        return;
    } else {
        g_print ("No synk! Fallback to standard clock\n");
        global_clock = NULL;
    }
  }
  
  
  global_clock = gst_net_client_clock_new ("net_clock", gst_rtsp_connection_get_ip (conn), 8554, 0);

  if (global_clock == NULL) {
    g_print ("Failed to create net clock client for %s:%d\n",
        "192.168.0.1", 8554);
    return;
  }

  g_print ("Waiting clock...");
  /* Wait for the clock to stabilise */
  gst_clock_wait_for_sync (GST_CLOCK(global_clock), 3e9 ); //3 secs //GST_CLOCK_TIME_NONE
  if (gst_clock_is_synced (global_clock))
  {
    g_print ("Synked with accuracy %d ns!\n", (int)(gst_clock_get_resolution (global_clock)));
  } else {
    g_print ("No synk!\n");
    global_clock = NULL;
  }
}
