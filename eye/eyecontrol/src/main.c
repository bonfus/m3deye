#include <signal.h>
#include <stdio.h>
#include <pthread.h> //for threading , link with lpthread
#include <stdlib.h>
#include <string.h>

#include <gst/video/colorbalance.h>

#include <gst/net/gstnettimeprovider.h>
#include <gst/rtsp-server/rtsp-server.h>

#include "callbacks.h"
#include "configuration.h"
#include "cleaners.h"
#include "rtsp.h"


#define PIPELINE "rpicamsrc name=src preview=0 fullscreen=0 shutter-speed=3000 ! video/x-h264,width=320,height=240 ! h264parse ! rtph264pay name=pay0 pt=96"
#define PIPELINEA "videotestsrc name=src is-live=true ! x264enc ! rtph264pay name=pay0 pt=96"
#define PIPELINEB "videotestsrc name=src is-live=true pattern=ball ! x264enc ! rtph264pay name=pay0 pt=96"



int
main (int argc, char *argv[])
{
  
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory=NULL;
  GstRTSPMediaFactory *factory2=NULL;
  GstRTSPMediaFactory ** factories;
  

  ec_settings_t * settings;

  // try to get pipeline
  GstElement * pipeline = NULL;
    
  
  gst_init (&argc, &argv);

  // Allocate configuration variable
  settings = malloc(sizeof(ec_settings_t));
  if (settings==NULL)
  {
    g_print("Error allocating settings");
    return -1;
  }
  
  // Prepare configuration
  set_default_values(&settings);

  
  // Retrieve the options:
  int opt;
  opterr = 0;
  while ( (opt = getopt(argc, argv, "hc:")) != -1 ) {  // for each option...
      switch ( opt ) {
          case 'h':
                g_print ("usage: %s -c config_file \n"
                    "example: %s ./config\n"
                    "\n"
                    "Example config file:\n"
                    "saturation: 10\n"
                    "contrast: 10\n"
                    "brightness: 10\n"
                    "\n"
                    "\n"
                    "service: videotestsrc name=src is-live=true ss=SS co=CO ! x264enc ! rtph264pay name=pay0 pt=96|/test\n"
                    "service: videotestsrc name=src is-live=true pattern=ball ! x264enc ! rtph264pay name=pay0 pt=96|/prova\n"
                    "service: rpicamsrc name=src preview=0 fullscreen=0 shutter-speed=SS contrast=CO brightness=BR saturation=SA ! video/x-h264,width=320,height=240 ! h264parse ! rtph264pay name=pay0 pt=96|/rpilowres\n"
                    "\n"
                    "Please note: pipeline must be live for synchronisation to work properly with this method!\n",
                    argv[0], argv[0]);
              break;
          case 'c':
            settings->config_file = strdup(optarg);
            break;
              
          case '?':  // unknown option...
                  if (optopt == 'c')
                    perror("For option 'c\', argument is mandatory!\n");
                  fprintf(stderr,"(!!) Unknown option %c", optopt);
              break;
      }
  }

  
  // Parse configuration
  load_configuration(&settings);
  prepare_pipelines(&settings);
  dump_configuration(&settings);
  
  loop = g_main_loop_new (NULL, FALSE);
  
  pthread_t thread_id;
  
  gstinfo_t * info = malloc (sizeof (struct gstinfo));
  
  info->main_running = true;
  info->settings = &settings;
  info->pipeline = &pipeline;
  info->loop = &loop;
  info->change_color_settings = &change_color_settings;
  info->change_shutter_speed= &change_shutter_speed;
  info->store_config = &store_config;
  info->quit = &quit;
  
  
  if( pthread_create( &thread_id , NULL ,  start_serving , (void*) &info) < 0)
  {
      perror("Could not create comunication thread!");
      return 1;
  }

  global_clock = NULL;

  /* create a server instance */
  server = gst_rtsp_server_new ();

  /* callback for clients, used to sync clocks */
  g_signal_connect (server, "client-connected", G_CALLBACK (sync_clocks), &info);

  /* get the mount points for this server, every server has a default object
   * that be used to map uri mount points to media factories */
  mounts = gst_rtsp_server_get_mount_points (server);
  
  factories = malloc (sizeof (GstRTSPMediaFactory*) * settings->num_factories);
  for (int ifactories=0; ifactories < settings->num_factories;ifactories++)
  {
    /* make a media factory for a test stream. The default media factory can use
    * gst-launch syntax to create pipelines.
    * any launch line works as long as it contains elements named pay%d. Each
    * element with pay%d names will be a stream */
    
    factories[ifactories] = test_rtsp_media_factory_new ();
    
    gst_rtsp_media_factory_set_launch (factories[ifactories], settings->factories[ifactories].pipeline);
    gst_rtsp_media_factory_set_shared (GST_RTSP_MEDIA_FACTORY (factories[ifactories]), TRUE);
    gst_rtsp_media_factory_set_media_gtype (GST_RTSP_MEDIA_FACTORY (factories[ifactories]),
        TEST_TYPE_RTSP_MEDIA);
  
    /* attach the test factory to the /test url */
    gst_rtsp_mount_points_add_factory (mounts, settings->factories[ifactories].mountpoint, factories[ifactories]);
    
    g_signal_connect (factories[ifactories], "media-constructed", G_CALLBACK (get_pipeline), &info);
    
    g_print ("stream ready at rtsp://127.0.0.1:8554%s\n", settings->factories[ifactories].mountpoint);
  }
      
  // /* add another factory */
  // 
  // factory2 = test_rtsp_media_factory_new ();
  // gst_rtsp_media_factory_set_launch (factory2, PIPELINE);
  // gst_rtsp_media_factory_set_shared (GST_RTSP_MEDIA_FACTORY (factory2), TRUE);
  // gst_rtsp_media_factory_set_media_gtype (GST_RTSP_MEDIA_FACTORY (factory2),
  //     TEST_TYPE_RTSP_MEDIA);  
  // 
  // /* attach the test factory to the /test url */
  // gst_rtsp_mount_points_add_factory (mounts, "/prova", factory2);

  /* don't need the ref to the mapper anymore */
  g_object_unref (mounts);

  /* attach the server to the default maincontext */
  gst_rtsp_server_attach (server, NULL);

  //g_signal_connect (factory, "media-constructed", G_CALLBACK (get_pipeline), &pipeline); 
  //g_signal_connect (factory2, "media-constructed", G_CALLBACK (get_pipeline), &pipeline); 
  

  
  
  
  //g_timeout_add_seconds(1, process, &pipeline);
  //g_timeout_add_seconds(1, quit, loop);
  

  /* start serving */  
  g_main_loop_run(loop);
  info->main_running = false;
  printf("Out of g_main_loop\n");  
   
  pthread_join(thread_id, NULL);
  
  
  // FREE MEMORY:
  
  // we need to free what's inside first!!!
  clean_ec_settings(&settings);
  free(settings);

  //clean_gst_info(&info);
  free(info);

  free(factories);

  return 0;
}
