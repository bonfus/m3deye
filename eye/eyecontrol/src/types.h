#include <glib.h>
#include <gst/gst.h>
#include <inttypes.h>
#ifndef TYPES_H_
#define TYPES_H_


typedef int bool;
#define true 1
#define false 0 


typedef struct cbret
{
     int current_value;
     int retval;
} cbret_t;

typedef enum  cmdname {
    cmd_none,
    cmd_save_config,
    cmd_brightness,
    cmd_contrast,
    cmd_saturation,
    cmd_shutter_speed,
    cmd_frame_rate,
    cmd_quit
} cmdname;

void * start_serving(void * info);

typedef struct command {
    cmdname cmd;
    int32_t value;
    bool increment;
} command_t;

typedef struct rtsp_factory_element {
    char * pipeline;
    char * mountpoint;
} rtsp_factory_element_t;


typedef struct ec_settings {
    int saturation;
    int contrast;
    int brightness;
    int shutter_speed;
    int frame_rate;
    rtsp_factory_element_t * factories;
    int num_factories;
    char * config_file;
} ec_settings_t;



typedef struct gstinfo {
  bool main_running;
  GstElement ** pipeline;
  GMainLoop **loop;
  ec_settings_t ** settings;
  cbret_t (*change_color_settings)(GstElement** p, const char * what, int new_value, bool increment);
  cbret_t (*change_shutter_speed)(GstElement** p, int new_value, bool increment);
  cbret_t (*store_config)(ec_settings_t ** settings);
  void (*quit)(gpointer data);
} gstinfo_t;
 
typedef struct handlerinfo {
  gstinfo_t * gstcontrol;
  int * sock;
} handlerinfo_t; 

#endif /* TYPES_H_ */
