#include <stdlib.h>
#include "types.h"

void clean_ec_settings(ec_settings_t ** settings)
{
    ec_settings_t * s = *(settings);
    for (int ifactories=0; ifactories < s->num_factories;ifactories++)
    {
        free(s->factories[ifactories].pipeline);
        free(s->factories[ifactories].mountpoint);
    }
    free(s->factories);
    free(s->config_file);
}


