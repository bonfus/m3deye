#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <unistd.h>



#include "GstInput.hpp"

int main(int argc, char *argv[])
{
    int sync;
    IplImage * frameL;
    IplImage * frameR;
    
    cv::Mat frame1;
    cv::Mat frame2;
    cv::Mat diff;
    cvNamedWindow( "frames", CV_WINDOW_AUTOSIZE );
    cvNamedWindow( "diff", CV_WINDOW_AUTOSIZE );
   cvMoveWindow("frames",100,100);    
   cvMoveWindow("diff",200,200);      
    
    GstInput * test;

    test = new GstInput;
    if(!test->openFile(argv[1],argv[2])) {
        std::cout << " cacca!" << std::endl;
        delete test;
        return 0;
    }



   int iFrame(0);
   while(1)
   {
        if (!test->grabFrames()){
            std::cout << " nograb!" << std::endl;
            return 0;}
        if (!test->retriveFrames(frameL, frameR, sync)){
            std::cout << " noretrive!" << std::endl;
            return 0;}
        //std::cout << " Diff " << std::fixed<< (capture2.get(9999)-capture1.get(9999)) << " " << std::fixed<< (capture1.get(10000) - capture2.get(10000)) << std::endl;
            
        iFrame++;   
        frame1=cv::Mat(frameL,false);
        frame2=cv::Mat(frameR,false);
        if( !frame1.data || ! frame2.data )
        {
        break;
        }
        if(iFrame>-1)
        {
            cv::Mat dest;
            absdiff(frame1, frame2, diff);
            hconcat(frame1, frame2, dest);
            std::ostringstream str;
            str <<"Sync: " << sync;
            
            cv::putText(diff, str.str(), cv::Point(600,100), CV_FONT_HERSHEY_PLAIN, 3, CV_RGB(0,255,0));
            cv::putText(dest, str.str(), cv::Point(600,100), CV_FONT_HERSHEY_PLAIN, 3, CV_RGB(0,255,0));
                    
            imshow( "diff", diff );
            imshow( "frames", dest );
        }
        
        char c = cvWaitKey(0);
        if( c == 10 )
        {
        continue;
        } else if ( c == 27 )
        {
            break;
        }
   }
    cvReleaseImage(&frameL);
    cvReleaseImage(&frameR);
    
    delete test;
    return 0;
}
