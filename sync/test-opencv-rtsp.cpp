// g++ -std=gnu++11 test-opencv-rtsp.cpp `pkg-config opencv --cflags --libs`

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <unistd.h>

// to estimate speed
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;

int main(int argc, char *argv[])
{

cv::Mat frame1;
cv::Mat frame2;
cv::Mat diff;
     cv::VideoCapture capture1;
     cv::VideoCapture capture2;
    
    cvNamedWindow( "title", CV_WINDOW_AUTOSIZE );
   cvMoveWindow("title",100,100);    
     
     //capture1.open("rtsp://127.0.0.1:8554/test 127.0.0.1 8554");
     
     //sleep a little bit
     sleep(3);
     
     //capture2.open("rtsp://127.0.0.1:8554/test 127.0.0.1 8554");
     
    //capture1.open("rtsp://192.168.1.100:8554/rpi1296x972-42fps 192.168.1.100 8554");
    capture1.open("rtsp://192.168.1.100:8554/rpi320x240-25fps 192.168.1.100 8554");
        //sleep(3);
    //capture2.open("rtsp://192.168.1.102:8554/rpi1296x972-42fps 192.168.1.102 8554");     
    capture2.open("rtsp://192.168.1.102:8554/rpi320x240-25fps 192.168.1.102 8554");     

   
   
    Clock::time_point t0 = Clock::now();
    Clock::time_point newt = Clock::now();
   
   //std::string info("");
   std::cout.precision(15);
   int iFrame(0);
   while(1)
   {
    capture1 >> frame1;
    capture2 >> frame2;
    std::cout << " Diff " << std::fixed<< (capture2.get(9999)-capture1.get(9999)) << " " << std::fixed<< (capture1.get(10000) - capture2.get(10000)) << std::endl;
          
    iFrame++;   
    if( !frame1.data || ! frame2.data )
    {
     break;
    }
    if(iFrame>-1)
    {
        absdiff(frame1, frame2, diff);
        imshow( "title", diff );
    }
    if (iFrame > 1)
    {
        newt = Clock::now();
        milliseconds ms = std::chrono::duration_cast<milliseconds>(newt - t0);
        //std::cout << "Time since 1st frame" << ms.count() << " ms, fps: "<< ((double)iFrame)/(ms.count()/1000.0f) << std::endl;
    } else {
        t0 = Clock::now();
    }
    
    char c = cvWaitKey(10);
    if( c == 27 )
    {
     break;
    }
   }



  return 0;
}
