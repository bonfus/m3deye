#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <exception>

#include <utility>      // std::pair, std::make_pair
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

using namespace cv;
using namespace std;

void ShowRectifiedImages(const Mat & R1,const Mat & R2,const Mat & P1,const Mat & P2, Mat *cameraMatrix, Mat *distCoeffs, const 	vector<pair<Mat, Mat> > &goodStereoImages, Rect* validRoi);
void StereoCalib(const vector<pair<Mat, Mat> >& stereoImages, const Size boardSize, const float squareSize, const Mat & K, const Mat & dist, const bool bVerbose);
vector<pair<Mat, Mat> > AssembleStereoImagesFromVideos(VideoCapture &captureL, VideoCapture &captureR, int takeFrameFrequency, int nFrames);

int main(int argc, char** argv)
{
    try{
        if(argc < 2)
            throw std::runtime_error("need to provide as argument a configuration file <configFile>.yml ! aborting.");

        string  fileName(argv[1]);
        cout << " using parameters file : " << fileName <<endl;
		Size boardSize;
		float chessBoardEdgeMeters;
		int takeFrameFrequency, nFrames;
		string calibrationVideoLeft, calibrationVideoRight, intrinsicsInputFile, tsFileL, tsFileR;
		bool bVerbose(0);
		FileStorage fs2(fileName, FileStorage::READ);
		if (!fs2.isOpened())
			throw string(string("Aborting : Failed to open: !")  + fileName);

		fs2["chessBoardEdgeMeters"] >> chessBoardEdgeMeters;
		fs2["chessBoardWidth"] >> boardSize.width;
		fs2["chessBoardHeight"] >> boardSize.height;
		fs2["calibrationVideoLeft"] >> calibrationVideoLeft;
		fs2["calibrationVideoRight"] >> calibrationVideoRight;
		fs2["takeFrameFrequency"] >> takeFrameFrequency;
		fs2["calibrationVideoRight"] >> calibrationVideoRight;
		fs2["nFrames"] >> nFrames;
		fs2["intrinsicsInputFile"]>> intrinsicsInputFile;
		fs2["bVerbose"]>> bVerbose;

		fs2.release();

		Mat internalParametersMatrix, distortionCoeff;
		FileStorage fsIntrinsics(intrinsicsInputFile, FileStorage::READ);
		if (!fsIntrinsics.isOpened())
			throw string(string("Aborting : Failed to open: !")  + intrinsicsInputFile);
		fsIntrinsics["Camera_Matrix"] >> internalParametersMatrix;
		fsIntrinsics["Distortion_Coefficients"]>> distortionCoeff;
		fsIntrinsics.release();


		cout << " chessboard size : " << boardSize <<endl;
		cout << " calibrationVideoLeft : " << calibrationVideoLeft <<endl;
		cout << " calibrationVideoRight : " << calibrationVideoRight <<endl;
		cout << "nFrames " <<nFrames<<endl;
		cout << " take frame freq." << takeFrameFrequency<<endl;
		cout << " internal matrix : " << internalParametersMatrix<<endl;
		cout << " distortion coeff: " << distortionCoeff<<endl;
		cout << "verbose output : "<<bVerbose <<endl;
		
		VideoCapture captureL;
		VideoCapture captureR;

		//OpenSynchronizedVideoCaptures(captureL, captureR, calibrationVideoLeft, calibrationVideoRight, tsFileL, tsFileR);
                
                captureL.open(calibrationVideoLeft);
                captureR.open(calibrationVideoRight);
                
                
		vector<pair<Mat, Mat> > stereoPairs = AssembleStereoImagesFromVideos(captureL, captureR, takeFrameFrequency, nFrames);

		cout << " Finished acquiring images. Starting calibration."<<endl;

		StereoCalib(stereoPairs, boardSize, chessBoardEdgeMeters, internalParametersMatrix, distortionCoeff, bVerbose);
		return 0;
	} catch (std::exception &ex)
	{
		cout<<"Exception :"<<ex.what()<<endl;
	}
	return 0;

}



void StereoCalib(const vector<pair<Mat, Mat> >& stereoImages, const Size boardSize, const float squareSize, const Mat & K, const Mat & dist, const bool bVerbose)
{
	const int maxScale = 1; //TODO remove  chessboard search over many scales ( or enable it)
	vector<vector<Point2f> > imagePoints[2];
	vector<vector<Point3f> > objectPoints;

	int i, nGoodPairs(0), k;
	int nStereoPairs = (int)stereoImages.size();

	imagePoints[0].resize(nStereoPairs);
	imagePoints[1].resize(nStereoPairs);

	vector<pair<Mat, Mat> >goodStereoImages;
	goodStereoImages.reserve(stereoImages.size());

	cout << "searching for good stereo image pairs ..."<<endl;
	for( i = 0; i < nStereoPairs; i++ )
	{
		for( k = 0; k < 2; k++ )
		{
			Mat img = (k==0? stereoImages[i].first : stereoImages[i].second);
                        char key = (char)waitKey(10);
			bool found = false;
			vector<Point2f>& corners = imagePoints[k][nGoodPairs];
			for( int scale = 1; scale <= maxScale; scale++ )
			{
				Mat timg;
				if( scale == 1 )
					timg = img;
				else
					resize(img, timg, Size(), scale, scale);
				found = findChessboardCorners(timg, boardSize, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);
				if( found )
				{
					if( scale > 1 )
					{
						Mat cornersMat(corners);
						cornersMat *= 1./scale;
					}
					break;
				}
			}

			if( !found )
				break;
			cornerSubPix(img, corners, Size(11,11), Size(-1,-1), TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 30, 0.01));
			if(bVerbose)
			{
				Mat cimg, cimg1;
				cvtColor(img, cimg, COLOR_GRAY2BGR);
				drawChessboardCorners(cimg, boardSize, corners, found);
				double sf = 640./MAX(img.rows, img.cols);
				resize(cimg, cimg1, Size(), sf, sf);
				imshow("corners", cimg1);
				char c = (char)waitKey(500);
			}
		}
		if( k == 2 )
		{
			goodStereoImages.push_back(stereoImages[i]);
			nGoodPairs++;
		}
	}
	cout << nGoodPairs << " pairs have been successfully detected." << stereoImages.size()<<endl;

	if (bVerbose)
	{
		for(int i(0); i < goodStereoImages.size(); ++i)
		{
			namedWindow("Stereo images- press a key to continue", WINDOW_NORMAL);
			Mat frameToDisplay;
			hconcat(goodStereoImages[i].first, goodStereoImages[i].second, frameToDisplay);
			imshow("Stereo images- press a key to continue", frameToDisplay);
			waitKey(0);
			cout << i<<endl;
		}
	}

	nStereoPairs = nGoodPairs;
	if( nStereoPairs < 10 )
	{
		cout << "Error: too little pairs to run the calibration\n";
		return;
	}

	imagePoints[0].resize(nStereoPairs);
	imagePoints[1].resize(nStereoPairs);
	objectPoints.resize(nStereoPairs);

	for( i = 0; i < nStereoPairs; i++ )
	{
		for( int ii = 0; ii < boardSize.height; ii++ )
		{
			for( k = 0; k < boardSize.width; k++ )
				objectPoints[i].push_back(Point3f(ii*squareSize, k*squareSize, 0));
		}
	}

	cout << "Running stereo calibration ...\n";
	Size imageSize = stereoImages[0].first.size();

    //	double f = 3.5674;// tutto in mm
    //	double wSensor = 3.6288;
    //	double hSensor = 2.7216;
    //	double wPicture = stereoImages[0].first.cols;
    //	double hPicture = stereoImages[0].first.rows;
    //	Mat K2 = Mat::eye(3, 3, CV_64F);
	//
	//	K2.at<double>(0,0) = 2.6450864591531204e+03;
	//	K2.at<double>(1,1) = 2.6450864591531204e+03;
	//	K2.at<double>(0,2) = 9.5636481015076583e+02;
	//	K2.at<double>(1,2) = 5.3834185468273870e+02;
	//
	//
	//	Mat dist =   Mat::zeros(5,1,CV_64F);
	//	dist.at<double>(0,0) = 2.4706338206913120e-01 ;
	//	dist.at<double>(1,0) = -7.7716623571605759e-01;
	//	dist.at<double>(2,0) = -6.6330527631429746e-03 ;
	//	dist.at<double>(3,0) = 9.4234654123262068e-03;
	//	dist.at<double>(4,0) = 1.5016852818686233e+00;
	//
    //	Mat K = Mat::eye(3, 3, CV_64F);
    //	K.at<double>(0,0) = wPicture * f /wSensor;
    //	K.at<double>(1,1) = hPicture * f /hSensor;
    //	K.at<double>(0,2) = wPicture/2;
    //	K.at<double>(1,2) = hPicture/2;
    //	cout << "Using fixed internal parameters matrix : "<< K <<endl<<K2<<endl;

	Mat cameraMatrix[2], distCoeffs[2];

	distCoeffs[0] = dist.clone();
	distCoeffs[1] = dist.clone();

    cameraMatrix[0] = K.clone();
    cameraMatrix[1] = K.clone();
    Mat R, T, E, F;

	double rms = stereoCalibrate(objectPoints, imagePoints[0], imagePoints[1],
			cameraMatrix[0], distCoeffs[0],
			cameraMatrix[1], distCoeffs[1],
			imageSize, R, T, E, F,
			TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),
			CV_CALIB_FIX_INTRINSIC);

	cout << "done with RMS error=" << rms << endl;
    FileStorage fs;
    fs.open("out/extrinsicsStereoCalibrate.yml", CV_STORAGE_WRITE);
    if( fs.isOpened() )
    {
        fs << "R" << R << "T" << T << "E" << E << "F" << F ;
        fs.release();
    }
    else
        cout << "Error: can not save the extrinsic parameters\n";




	// CALIBRATION QUALITY CHECK
	// because the output fundamental matrix implicitly
	// includes all the output information,
	// we can check the quality of calibration using the
	// epipolar geometry constraint: m2^t*F*m1=0
	double err = 0;
	int npoints = 0;
	vector<Vec3f> lines[2];
	for( i = 0; i < nStereoPairs; i++ )
	{
		int npt = (int)imagePoints[0][i].size();
		Mat imgpt[2];
		for( k = 0; k < 2; k++ )
		{
			imgpt[k] = Mat(imagePoints[k][i]);
			undistortPoints(imgpt[k], imgpt[k], cameraMatrix[k], distCoeffs[k], Mat(), cameraMatrix[k]);
			computeCorrespondEpilines(imgpt[k], k+1, F, lines[k]);
		}
		for( nGoodPairs = 0; nGoodPairs < npt; nGoodPairs++ )
		{
			double errij = fabs(imagePoints[0][i][nGoodPairs].x*lines[1][nGoodPairs][0] +
					imagePoints[0][i][nGoodPairs].y*lines[1][nGoodPairs][1] + lines[1][nGoodPairs][2]) +
							fabs(imagePoints[1][i][nGoodPairs].x*lines[0][nGoodPairs][0] +
									imagePoints[1][i][nGoodPairs].y*lines[0][nGoodPairs][1] + lines[0][nGoodPairs][2]);
			err += errij;
		}
		npoints += npt;
	}
	cout << "average reprojection err = " <<  err/npoints << endl;

	// save intrinsic parameters
    fs.open("out/intrinsics.yml", CV_STORAGE_WRITE);
	if( fs.isOpened() )
	{
        fs << "Camera_MatrixL" << cameraMatrix[0];
        fs << "Distortion_CoefficientsL" << distCoeffs[0] ;
        fs << "Camera_MatrixR" << cameraMatrix[1] ;
        fs << "Distortion_CoefficientsR" << distCoeffs[1];
		fs.release();
	}
	else
		cout << "Error: can not save the intrinsic parameters\n";

	Rect validRoi[2];
	Mat R1, R2, P1, P2, Q;
	stereoRectify(cameraMatrix[0], distCoeffs[0], cameraMatrix[1], distCoeffs[1], imageSize, R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, 1, imageSize, &validRoi[0], &validRoi[1]);
	fs.open("out/extrinsics.yml", CV_STORAGE_WRITE);
	if( fs.isOpened() )
	{
		fs << "R" << R << "T" << T << "R1" << R1 << "R2" << R2 << "P1" << P1 << "P2" << P2 << "Q" << Q;
		fs.release();
	}
	else
		cout << "Error: can not save the extrinsic parameters\n";

	if (bVerbose)
		ShowRectifiedImages(R1, R2, P1, P2, cameraMatrix, distCoeffs, goodStereoImages, validRoi);
}


vector<pair<Mat, Mat> > AssembleStereoImagesFromVideos(VideoCapture &captureL, VideoCapture &captureR, int takeFrameFrequency, int nFrames)
{
	vector<pair<Mat, Mat> > stereoPairs;
	stereoPairs.reserve(nFrames);
	for (int i(0); i < nFrames; ++i)
	{
		Mat imgLeft, imgRight;
		for (int j(0); j < takeFrameFrequency; ++j)
		{
			captureL >> imgLeft;
			captureR >> imgRight;
			if(imgRight.size() != imgLeft.size() )
			{
				cout<< "Images have diffeent sizes!. Continue\n"<<endl;
				continue;
			}

			if(!imgLeft.data || ! imgRight.data)
			{
				cout<<"Videos are not long enough. Try to put lower nFrames or lower takeFrameFrequency. Will try to calibrate with "<< i << " pairs."<<endl;
				return stereoPairs;
			}
		}
		Mat tempLeft;
		cvtColor(imgLeft, tempLeft, CV_RGB2GRAY);
		Mat tempRight;
		cvtColor(imgRight, tempRight, CV_RGB2GRAY);
		pair<Mat, Mat> tempPair(tempLeft, tempRight);
		stereoPairs.push_back(tempPair);
	}
	return stereoPairs;
}


void ShowRectifiedImages(const Mat & R1,const Mat & R2,const Mat & P1,const Mat & P2, Mat *cameraMatrix, Mat *distCoeffs, const 	vector<pair<Mat, Mat> > &goodStereoImages, Rect* validRoi)
{
	Size imageSize(goodStereoImages[0].first.cols, goodStereoImages[0].first.rows);
	Mat rmap[2][2];
	//Precompute maps for cv::remap()
	initUndistortRectifyMap(cameraMatrix[0], distCoeffs[0], R1, P1, imageSize, CV_16SC2, rmap[0][0], rmap[0][1]);
	initUndistortRectifyMap(cameraMatrix[1], distCoeffs[1], R2, P2, imageSize, CV_16SC2, rmap[1][0], rmap[1][1]);
	Mat canvas;
	double sf;
	int w, h;
	sf = 600./MAX(imageSize.width, imageSize.height);
	w = cvRound(imageSize.width*sf);
	h = cvRound(imageSize.height*sf);
	canvas.create(h, w*2, CV_8UC3);

	for(int  i = 0; i < goodStereoImages.size(); i++ )
	{
		for(int  k = 0; k < 2; k++ )
		{
			//			Mat img = imread(goodImageList[i*2+k], 0), rimg, cimg;
			Mat img , rimg, cimg;
            img = (k == 0 ? goodStereoImages[i].first : goodStereoImages[i].second);
			remap(img, rimg, rmap[k][0], rmap[k][1], CV_INTER_LINEAR);
			cvtColor(rimg, cimg, COLOR_GRAY2BGR);
			Mat canvasPart = canvas(Rect(w*k, 0, w, h));
			resize(cimg, canvasPart, canvasPart.size(), 0, 0, CV_INTER_AREA);
			Rect vroi(cvRound(validRoi[k].x*sf), cvRound(validRoi[k].y*sf), cvRound(validRoi[k].width*sf), cvRound(validRoi[k].height*sf));
			rectangle(canvasPart, vroi, Scalar(0,0,255), 3, 8);

		}
		for(int nGoodPairs = 0; nGoodPairs < canvas.rows; nGoodPairs += 16 )
			line(canvas, Point(0, nGoodPairs), Point(canvas.cols, nGoodPairs), Scalar(0, 255, 0), 1, 8);
		imshow("rectified", canvas);
		char c = (char)waitKey();
		if( c == 27 || c == 'q' || c == 'Q' )
			break;
	}
}
