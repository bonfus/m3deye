#include <iostream>
#include <stdio.h>
#include "libST.hpp"
#include <sigc++/sigc++.h>
#include <sigc++config.h>

#define ST_PARAMS "/home/pie/Dev/m3deye/libST/config.yml"

void got_points(Point3D_t * points, int nPoints, int iFrame, int sync)
{
    
    std::cout << "Ma dear point from frame " << iFrame <<std::endl;
    for (int i=0; i<nPoints; i++)
    {
        std::cout<<points[i].x<<" "<<points[i].y<<" "<<points[i].z<<std::endl;
    }
    delete [] points;
    
}

int main()
{
    ST * s = new ST();
    
    // connect signal when points are ready
    s->points_ready.connect( sigc::ptr_fun(got_points) );
    

    s->parse_config(ST_PARAMS); //s->parse_config(/path/to/config/file);
    
    s->searchEyes(2000); // timeout = 2000 ms
    s->setSource(ST_EYES,0);
    s->run(true,false,true); // 3 arguments: 1) emit on points ready 2) show video 3) set blocking run
    
    //add two files to the source manager
    
    s->addFiles("/home/pie/medusa/1434271888/eye1.h264", "/home/pie/medusa/1434271888/eye2.h264","test"); 
    
    // seleziona sorgente 0
    s->setSource(ST_FILES,0);
    
    // triangola
    s->run(true,false,true); // 3 arguments: 1) emit on points ready 2) show video 3) set blocking run
    delete s;
}
