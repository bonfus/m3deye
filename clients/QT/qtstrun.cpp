#include <QtGui> 
#include <iostream>
#include "qtstrun.hpp"



#ifdef Q_WS_X11
#include <QX11EmbedContainer>
#endif


STThread::STThread()
{
    s = new ST();
    s->points_ready.connect( sigc::mem_fun(this, &STThread::stGotPoints) );
    s->messages.connect( sigc::mem_fun(this, &STThread::stGotMessage) );
    s->status.connect( sigc::mem_fun(this, &STThread::stGotStatus) );
    
    qRegisterMetaType<QVector<Source_t> >("QVector<Source_t>");
    qRegisterMetaType<Source_t >("Source_t");
    qRegisterMetaType<QList<Point3D_t> >("QList<Point3D_t>");
    qRegisterMetaType<Point3D_t >("Point3D_t");
    qRegisterMetaType<CmdTypes >("CmdTypes");
    qRegisterMetaType<InfoMSG >("InfoMSG");
    qRegisterMetaType<StatusMSG >("StatusMSG");
}

STThread::~STThread()
{
    delete s;
}


void STThread::stInitRender(int winid)
{
    VideoRenderType rt = X11;
    s->initRender(rt);
    s->render->setDrawable(winid);
}

void STThread::stParseConfig(QString fname)
{
    s->parse_config(fname.toUtf8().constData());
}

void STThread::stGotMessage(stMessages msg)
{
    InfoMSG m;
    switch(msg)
    {
        case ST_NO_TRIANGULATION_CONFIG_AVAILABLE:
            m.type = ERROR;
            m.message = QString("No valid data for triangulation at selected resolution.");
            Q_EMIT(reportMsg(m));
            break;
        case ST_NOT_IDLE:
            m.type = ERROR;
            m.message = QString("System is not idle. Please stop current operation.");
            Q_EMIT(reportMsg(m));
            break;
        case ST_NO_SOURCE:
            m.type = WARNING;
            m.message = QString("No sources selected. Please select one.");
            Q_EMIT(reportMsg(m));
            break;            
    }
}

void STThread::stGotStatus(stStatus status)
{
    StatusMSG m;
    switch(status.task)
    {
        case ST_IDLE:
            m.task = IDLE;
            m.message = QString("Finished acquisition.");
            Q_EMIT(reportMsg(m));
            break;
        case ST_SAVE:
            m.task = SAVE;
            m.message = QString("Saved %1 seconds of data.").arg(status.seconds);
            Q_EMIT(reportMsg(m));
            break;
    }
}

void STThread::stGotPoints(Point3D_t * points, int nPoints, int iFrame, int sync)
{
    QList<Point3D_t> recvPoints;
    // this must be changed to check all points!
    //std::cout<< "NPOINTS " << nPoints <<std::endl;
    for (int i=0; i<nPoints; i++)
    {
        recvPoints.append(points[i]);
        //std::cout<< points[i].x<<" "<<points[i].y<<std::endl;
        //x[i]=points[i].x;
        //y[i]=points[i].y;
    }
    
    // TODO ! CHANGE THIS !!!
    // just for the presentation, less updates
    if ((iFrame%5)==0)
          Q_EMIT(reportPoints(recvPoints));
          
    delete [] points;
}


//void STThread::stAddFiles(const char * lfile, const char * rfile, const char* name) {
void STThread::stAddFiles(QString lfile, QString rfile, QString name) {
    //s->addFiles("/home/pie/medusa/1434271888/eye1.h264", "/home/pie/medusa/1434271888/eye2.h264","test"); 
    s->addFiles(lfile.toUtf8().constData(),rfile.toUtf8().constData(),name.toUtf8().constData());
}

void STThread::stSetSource(Source_t source)
{
     Q_EMIT clearState();
     s->setSource(source.type,source.index);
}

void STThread::stSearchEyes(unsigned int timeout)
{
     Q_EMIT clearState();
     InfoMSG m;
     m.type = INFO;
     m.message = QString("Searching for kinopsys...");
     Q_EMIT(reportMsg(m));
     s->searchEyes(timeout);   
}

QVector<Source_t> STThread::stGrabSources(){
     QVector<std::string> names;
     QVector<int> types;
     Source_t * data=NULL;
     int ret = s->getSources(&data);
     QVector<Source_t> sources;
     if (ret>0)
     {
          for (unsigned int i =0; i < ret; i++)
          {
               sources.push_back(data[i]);
          }
     }
     
     if (data != NULL)
          free(data); // ALSO INTERNAL VARIABLES SHOULD BE FREEEEED!!!!
     return sources;
}

void STThread::stUpdateSources()
{
    Q_EMIT clearState();
    if (s->queryNewSources()) {
        InfoMSG m;
        m.type = INFO;
        m.message = QString("New sources available.");
        Q_EMIT(reportMsg(m));
    }
    // maybe avoid this if we no that there are no new sources?
    Q_EMIT reportSources(stGrabSources());
}

void STThread::stShow() {
    Q_EMIT clearState();
    s->show();
}

void STThread::stSave(QString Lfilename, QString Rfilename) {
    Q_EMIT clearState();
    s->save(Lfilename.toUtf8().constData(), Rfilename.toUtf8().constData());
}

void STThread::stRun(const bool bSendPoints, const bool bShowVideo) {
    Q_EMIT clearState();
    s->run(bSendPoints,bShowVideo);
}

void STThread::stStop() {
    Q_EMIT clearState();
    s->stop();
}     


void STThread::stSetCameraProperty(CmdTypes what, bool raise)
{
     int val = 10;
     if (what == SET_SHUTTERSPEED)
     {
          val = 500;
     }
     if (raise == false)
          val = -val;
     //TODO: CHECK return val!
     s->cam_adjust_property(what, val, true);
}
