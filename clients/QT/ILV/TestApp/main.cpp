//#include <sigc++/sigc++.h>
#include <QApplication>
#include <QObject>
#include "InteractiveLiveViewer.hpp"
#include "testILV.hpp"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    InteractiveLiveViewer * v = new InteractiveLiveViewer();

    //QRect rec = QApplication::desktop()->screenGeometry();
    //int height = rec.height();
    //int width = rec.width();
    v->resize(600,400);

 
    v->show();
    
    TestILV test(v);
    
    return app.exec();
    
    
    
}
