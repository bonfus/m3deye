#include "testILV.hpp"

TestILV::TestILV (InteractiveLiveViewer * v) : _v(v) 
{
    Point3D_t p1 = {0,0,0.5,0};
    ps.append(p1);
    Point3D_t p2 = {0.5,0,0.5,1};
    ps.append(p2);
    Point3D_t p3 = {0,0.5,0.5,2};
    ps.append(p3);
    Point3D_t p4 = {0.5,0,1.,3};
    ps.append(p4);
    Point3D_t p5 = {0,0.5,2.,4};
    ps.append(p5);
    
    QTimer * timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(throwPoints()));
    timer->start(1000); 
}
TestILV::~TestILV(){}
    
void TestILV::throwPoints()
{
    // create random points
    //int np = rand() % 4 + 2; //max 5 p
    int np=5;
    qDebug() << "TESTILV: Updating " << np << " points";
    QList<Point3D_t> nps;
    for (int i=0; i<5;i++)
    {
        //ps[i].x += (float)((rand()%10)-5)/100.;
        //ps[i].y += (float)((rand()%10)-5)/100.;
    }
    for (int i=0; i<np;i++)
    {
        //ps[i].z += (float)((rand()%100)-50);
        nps.append(ps[i]);
    }
    qDebug() << "TESTILV: Size of newPoints " << nps.size();
    _v->newPoints(nps);
}

