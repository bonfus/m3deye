#include <QWidget>
#include <QDebug>
#include <QTimer>
#include "InteractiveLiveViewer.hpp"

class TestILV : public QObject
{
    Q_OBJECT
public:
    TestILV (InteractiveLiveViewer * v);
    ~TestILV();
public Q_SLOTS:
    void throwPoints();

private:    
    InteractiveLiveViewer * _v;
    QList<Point3D_t> ps;    
};
