//DEBUG
#include <QTimer>
#include <QDebug>


#include "InteractiveLiveViewer.hpp"
#include "graphicsscene.hpp"

InteractiveLiveViewer::InteractiveLiveViewer(QWidget *parent) : QWidget( parent ), _scale(500.)
{
    scene = new GraphicsScene(this);
    viewer = new QGraphicsView(scene, this);
    QVBoxLayout * l = new  QVBoxLayout(this);
    l->addWidget(viewer);
    setLayout(l);
    
    viewer->show();    
    viewer->setSceneRect(-300,-200, 600, 400);
    
}

InteractiveLiveViewer::~InteractiveLiveViewer()
{
    
}


QList<QPointF3DProj> InteractiveLiveViewer::project(const QList<Point3D_t> p)
{
    QList<QPointF3DProj> newlist;
    
    for (int i=0; i<p.size();i++)
    {
        newlist.append(QPointF3DProj(p[i],_scale));
    }
    return newlist;
}

void InteractiveLiveViewer::newPoints(QList<Point3D_t> points)
{
    scene->updatePoints(project(points));
}
