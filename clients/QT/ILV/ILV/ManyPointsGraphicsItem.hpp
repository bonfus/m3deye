#ifndef ARCGRAPHICSITEM_H
#define ARCGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QLineF>
#include <QPointF>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QWidget>

class ManyPointsGraphicsItem : public QGraphicsItem
{
public:
    ManyPointsGraphicsItem();//QGraphicsObject * parent = 0);//QGraphicsItem *parent = 0);
    ManyPointsGraphicsItem(QVector<QPointF> points);//, QGraphicsObject * parent = 0);//QGraphicsItem* parent = 0);
    ~ManyPointsGraphicsItem();
    
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    int type() const { return Type;}

    //QPainterPath shape() const;
protected:
    // overriding mouse events
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    void init();
    
    QRectF boundingRectTemp;
    QVector<QPointF> _points;

};

#endif
