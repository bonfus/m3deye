#include "geometry.hpp"

Point3D_t VecSum(const Point3D_t u, const Point3D_t v)
{
    Point3D_t p;
    p.x = u.x+v.x;
    p.y = u.y+v.y;
    p.z = u.z+v.z;
    p.id=-1;
    return p;
}
Point3D_t VecDiff(const Point3D_t u, const Point3D_t v)
{
    Point3D_t p;
    p.x = u.x-v.x;
    p.y = u.y-v.y;
    p.z = u.z-v.z;
    p.id=-1;
    return p;
}

Point3D_t operator+(const Point3D_t u, const Point3D_t p)
{
    return VecSum(u,p);
}
Point3D_t operator-(const Point3D_t u, const Point3D_t p)
{
    return VecDiff(u,p);
}
Point3D_t operator*(const Point3D_t u, float s)
{
    Point3D_t p;
    p.x = u.x*s;
    p.y = u.y*s;
    p.z = u.z*s;    
    p.id=u.id;
    return p;
}
Point3D_t operator*(const Point3D_t u, double s)
{
    Point3D_t p;
    p.x = u.x*s;
    p.y = u.y*s;
    p.z = u.z*s;  
    p.id=u.id;  
    return p;
}

Point3D_t operator/(const Point3D_t u, float s)
{
    Point3D_t p;
    p.x = u.x/s;
    p.y = u.y/s;
    p.z = u.z/s;    
    p.id=u.id;
    return p;
}

float DotProd(const Point3D_t u, const Point3D_t v)
{
    return u.x*v.x+u.y*v.y+u.z*v.z;
}

float VecNorm(const Point3D_t u)
{
    return sqrt(DotProd(u,u));
}

Point3D_t VecProd(const Point3D_t u, const Point3D_t v)
{
    // s = u x v
    //  s_1 = u_2v_3-u_3v_2
    //  s_2 &= u_3v_1-u_1v_3
    //  s_3 &= u_1v_2-u_2v_1
    
    Point3D_t p;
    p.x = u.y*v.z - u.z*v.y;
    p.y = u.z*v.x - u.x*v.z;
    p.z = u.x*v.y - u.y*v.x;
    p.id=-1;
    return p;
}

float VecAngle(const Point3D_t a, const Point3D_t b, const Point3D_t c)
{
    Point3D_t v1=b-a;
    Point3D_t v2=c-b;
    return acos(DotProd(v1/VecNorm(v1),v2/VecNorm(v2)));
}
