#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QList>
#include <QGraphicsItem>
#include "PointsGraphicsItem.hpp"
#include "DistanceGraphicsItem.hpp"
#include "geometry.hpp"



class GraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GraphicsScene(QObject *parent = 0);
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent * mouseEvent);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * mouseEvent);
    
    void updatePoints(QList<QPointF3DProj>);
    
Q_SIGNALS:

public Q_SLOTS:

private:

    QList<PointsGraphicsItem*> _points_items;
    QList<DistanceGraphicsItem*> _distance_items;
};

#endif // GRAPHICSSCENE_H
