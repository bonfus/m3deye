#ifndef INTERACTIVELIVEVIEWER_H
#define INTERACTIVELIVEVIEWER_H

#include <QVector>
#include <QWidget>
#include <QGraphicsView>
#include <QVBoxLayout>
#include "geometry.hpp"
#include "graphicsscene.hpp"


class InteractiveLiveViewer : public QWidget
{
    Q_OBJECT
public:
    InteractiveLiveViewer( QWidget *parent = 0 );
    ~InteractiveLiveViewer();
    
    
    float getScale(){return _scale;}
    
public Q_SLOTS:
    void newPoints(QList<Point3D_t> points);
    void setScale(float scale){if (scale>0){_scale=scale; return;} return;}
    
private:
    
    QList<QPointF3DProj> project(const QList<Point3D_t>);

    GraphicsScene* scene;
    QGraphicsView*  viewer;
    
    float _scale;
    
};
#endif
