#include "PointsGraphicsItem.hpp"
#include <QGraphicsItem>
#include <QLineF>
#include <QPointF>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QDebug>

PointsGraphicsItem::PointsGraphicsItem(QPointF3DProj p) : QGraphicsItem(), _point(p), _rad(10), _selected(false)
{
    qDebug() << "New point with id: " << _point.id;
    qDebug() << "New point x,y,z: " << _point.x() << " " << _point.y();
    this->setFlags(QGraphicsItem::ItemIsSelectable);
    this->setCursor(Qt::PointingHandCursor);
    init();
}

PointsGraphicsItem::~PointsGraphicsItem()
{

}

void PointsGraphicsItem::init()
{
    switch(_point.id%5)
    {
        case 0:
          _color = Qt::darkRed;
          break;
        case 1:
          _color = Qt::darkGreen;
          break;
        case 2:
          _color = Qt::darkBlue;
          break;
        case 3:
          _color = Qt::darkMagenta;
          break;
        case 4:
          _color = Qt::darkYellow;
    }
    // outer most edges
    _boundingRect = QRectF(- _rad, - _rad, _rad*2, _rad*2);  
    _boundingRect = _boundingRect.adjusted(-5*_rad, -5*_rad, 5*_rad, 5*_rad);
    qDebug() << "Done with init...";
}

QPainterPath PointsGraphicsItem::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

QRectF PointsGraphicsItem::boundingRect() const
{
    // outer most edges
    return _boundingRect;
}

void PointsGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen paintpen;
    QBrush brush;
    
    painter->setRenderHint(QPainter::Antialiasing);
    paintpen.setWidth(3);
    brush.setStyle(Qt::SolidPattern);
    // Draw lines

    
    paintpen.setColor(_color);
    if(isSelected())
    {
        brush.setColor(Qt::red);
    }
    else
    {
        brush.setColor(Qt::black);
    }
    
    painter->setBrush(brush);
    painter->setPen(paintpen);

    qreal ptRad = _rad;
    painter->drawEllipse(QPointF(0,0), ptRad, ptRad);
    
    painter->drawText(QPointF(-40,25), QString("%1 %2 %3").arg(
            QString::number(_point.p.x*100.,'f', 1),
            QString::number(_point.p.y*100.,'f', 1),
            QString::number(_point.p.z*100.,'f', 1))
            );

}



void PointsGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << "Item: " << Q_FUNC_INFO << mouseEvent->scenePos().x() << " " << mouseEvent->scenePos().y();
    QGraphicsItem::mousePressEvent(mouseEvent);
}

void PointsGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * me)
{
    //qDebug() << "Item: "<< Q_FUNC_INFO << me->scenePos().x() << " " << me->scenePos().y();
    QGraphicsItem::mouseReleaseEvent(me);
    
}
