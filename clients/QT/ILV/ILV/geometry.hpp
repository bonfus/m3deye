#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <math.h>       /* sqrt, acos */
#include <QPointF>


#ifndef POINTS3D_TYPE
#define POINTS3D_TYPE
//typedef struct Point3D {
//union {
//  float data[3];
//  struct {float x; float y; float z;};
//};
//int id;
//} Point3D_t;

typedef struct Point3D
{
    float x;
    float y;
    float z;
    int id;
} Point3D_t;            // OLD DEFINITION

#endif

class QPointF3DProj : public QPointF
{
public:    
    QPointF3DProj(Point3D_t _orig, float scale=1.0) : QPointF(scale*(_orig.x/_orig.z),scale*(_orig.y/_orig.z)), p(_orig), id(_orig.id) {}
//    QPointFID(const QPoint & point, int _id) : QPointF(point), id(_id) {} 
//	QPointFID(qreal xpos, qreal ypos, int _id) : QPointF(xpos, ypos), id(_id) {}
    Point3D_t p;
    int id;
};

Point3D_t VecSum(const Point3D_t u, const Point3D_t v);
Point3D_t VecDiff(const Point3D_t u, const Point3D_t v);
Point3D_t operator+(const Point3D_t u, const Point3D_t p);
Point3D_t operator-(const Point3D_t u, const Point3D_t p);
Point3D_t operator/(const Point3D_t u, float s);
Point3D_t operator*(const Point3D_t u, double s);
Point3D_t operator*(const Point3D_t u, float s);
float DotProd(const Point3D_t u, const Point3D_t v);
float VecNorm(const Point3D_t u);
Point3D_t VecProd(const Point3D_t u, const Point3D_t v);
float VecAngle(const Point3D_t a, const Point3D_t b, const Point3D_t c);



#endif
