#include "DistanceGraphicsItem.hpp"
#include <QGraphicsItem>
#include <QLineF>
#include <QPointF>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsLineItem>
#include <QDebug>


#define PI 3.14159265

DistanceGraphicsItem::DistanceGraphicsItem(QPointF3DProj p1, QPointF3DProj p2) : QGraphicsItem(), _p1(p1), _p2(p2)
{
    qDebug() << "New distance";
    _distance=VecNorm(VecDiff(_p1.p,_p2.p));
    init();
}

DistanceGraphicsItem::~DistanceGraphicsItem()
{

}

void DistanceGraphicsItem::init()
{
    // outer most edges
    _boundingRect = QRectF(_p1, _p2).normalized();
    
    _boundingRect = _boundingRect.adjusted(-40, -40, 40, 40);
    //qDebug() << "Done with init...";
}

//QPainterPath DistanceGraphicsItem::shape() const
//{
//    QPainterPath path;
//    path.addEllipse(boundingRect());
//    return path;
//}

QRectF DistanceGraphicsItem::boundingRect() const
{
    // outer most edges
    return _boundingRect;
}

void DistanceGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen paintpen;
    
    painter->setRenderHint(QPainter::Antialiasing);
    paintpen.setWidth(2);
    // Draw lines

    paintpen.setColor(Qt::black);
    painter->setPen(paintpen);
    
    QLineF l = QLineF(_p2,_p1);

    //painter->save(); // saves current painter state
    //float slope = (_p2.y()-_p1.y())/(_p2.x()-_p1.x());
    //painter->rotate(-l.angle());
    // TODO : better handling of cm conversion
    painter->drawText(l.pointAt(0.5), QString::number(_distance*100.,'f', 1));
    
    //painter->restore(); // restores painter state


    
    painter->drawLine(l);
}
