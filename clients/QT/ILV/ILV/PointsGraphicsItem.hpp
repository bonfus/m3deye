#ifndef POINTSGRAPHICSITEM_H
#define POINTSGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QLineF>
#include <QPointF>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QWidget>
#include <geometry.hpp>


class PointsGraphicsItem : public QGraphicsItem
{
public:
    PointsGraphicsItem(QPointF3DProj);//, QGraphicsObject * parent = 0);//QGraphicsItem* parent = 0);
    ~PointsGraphicsItem();
    
    
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    int type() const { return Type;}
    int id() const {return _point.id;}
    QPointF3DProj getPoint3DProj() const {return _point;}
    void setPoint3DProj(QPointF3DProj p) {_point = p;}

    QPainterPath shape() const;
    QRectF boundingRect() const;
    
protected:
    // overriding mouse events
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);


private:
    void init();
    QPointF3DProj _point;
    int _rad;
    QColor _color;
    // outer most edges
    QRectF _boundingRect;    
    bool _selected;

};

#endif
