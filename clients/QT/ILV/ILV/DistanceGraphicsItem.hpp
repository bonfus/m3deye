#ifndef DISTANCEGRAPHICSITEM_H
#define DISTANCEGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QLineF>
#include <QPointF>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QWidget>
#include <geometry.hpp>

class DistanceGraphicsItem : public QGraphicsItem
{
public:
    DistanceGraphicsItem(QPointF3DProj p1, QPointF3DProj p2);//, QGraphicsObject * parent = 0);//QGraphicsItem* parent = 0);
    ~DistanceGraphicsItem();
    
    
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    int type() const { return Type;}

    //QPainterPath shape() const;
    QRectF boundingRect() const;
    
protected:
    // overriding mouse events


private:
    void init();
    
    QPointF3DProj _p1;
    QPointF3DProj _p2;
    int _rad;
    QColor _color;
    // outer most edges
    QRectF _boundingRect;    
    float _distance;

};

#endif
