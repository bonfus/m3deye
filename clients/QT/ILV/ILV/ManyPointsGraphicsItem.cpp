#include "ManyPointsGraphicsItem.hpp"
#include <QGraphicsItem>
#include <QLineF>
#include <QPointF>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QDebug>

ManyPointsGraphicsItem::ManyPointsGraphicsItem(QVector<QPointF> points) : _points(points)
{
    init();
}


ManyPointsGraphicsItem::ManyPointsGraphicsItem()
{
    init();
}

ManyPointsGraphicsItem::~ManyPointsGraphicsItem()
{

}

void ManyPointsGraphicsItem::init()
{
    float minx=1000000.;
    float maxx=-1000000.;
    float miny=1000000.;
    float maxy=-1000000.;
    
    for (int i =0; i< _points.size(); i++)
    {
        if (_points[i].x() < minx)
            minx=_points[i].x();
        if (_points[i].y() < miny)
            miny=_points[i].y();
        if (_points[i].x() > maxx)
            maxx=_points[i].x();
        if (_points[i].y() > maxy)
            maxy=_points[i].y();
            
        if (i==0)
            continue;
        //QLineF(_points[0], _points[i]);
    }
    qDebug() << " minx, miny, maxx, maxy " << minx<< miny<< maxx<< maxy;
    QRectF circle = QRectF(minx, miny, maxx-minx, maxy-miny);
    int w = 10;
    boundingRectTemp = circle.adjusted(-w, -w, w, w);
    qDebug() << "Done with init...";
}

QRectF ManyPointsGraphicsItem::boundingRect() const
{
    // outer most edges
    qDebug() << "BBBBBBounding...";
    return boundingRectTemp;
}

void ManyPointsGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen paintpen;
    painter->setRenderHint(QPainter::Antialiasing);
    paintpen.setWidth(1);
    // Draw lines

    //if(isSelected())
    //{
    //    painter->setBrush(Qt::SolidPattern);
    //    paintpen.setColor(Qt::black);
    //    painter->setPen(paintpen);
    //}
    //else
    //{
    //    paintpen.setStyle(Qt::DashLine);
    //    paintpen.setColor(Qt::black);
    //    painter->setPen(paintpen);
    //}

    //QPainterPath path;
    //path.arcMoveTo(circle,startAngle);
    //path.arcTo(circle, startAngle, spanAngle);

    // Draw points
    qDebug() << "PPPPPainting...";
    //if (isSelected())
    //{
        // sets brush for end points
        painter->setBrush(Qt::SolidPattern);
        paintpen.setColor(Qt::red);
        painter->setPen(paintpen);

        qreal ptRad = 10;
        for (int i =0; i< _points.size(); i++)
          painter->drawEllipse(_points[i], ptRad, ptRad);
    //}

    //painter->setBrush(Qt::NoBrush);
    //painter->drawPath(path);
}

//QPainterPath ManyPointsGraphicsItem::shape() const
//{
//  qDebug() << "SSSSSShaping...";
//    QPainterPath path;
//    path.lineTo(_points[0]);
//    return path;
//}


void ManyPointsGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << "Item: " << Q_FUNC_INFO << mouseEvent->scenePos().x() << " " << mouseEvent->scenePos().y();
    QGraphicsItem::mousePressEvent(mouseEvent);
}

void ManyPointsGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * me)
{
    //qDebug() << "Item: "<< Q_FUNC_INFO << me->scenePos().x() << " " << me->scenePos().y();
    
    
    
    QGraphicsItem::mouseReleaseEvent(me);
    
}
