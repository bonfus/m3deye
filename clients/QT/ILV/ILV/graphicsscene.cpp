#include "graphicsscene.hpp"
#include <QDebug>
#include <Qt>
#include <QGraphicsEllipseItem>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include "qmath.h"

GraphicsScene::GraphicsScene(QObject *parent) :
    QGraphicsScene(parent)
{
    this->setBackgroundBrush(Qt::gray);
}

void GraphicsScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << Q_FUNC_INFO << mouseEvent->scenePos();
    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
}

void GraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << Q_FUNC_INFO << mouseEvent->scenePos();
    QGraphicsScene::mouseMoveEvent(mouseEvent);
    //if (mouseEvent->button() == Qt::MidButton)
    {
        //if (points_items.size() > 0)
        //{   
        //    QPointF p = points_items[0]->boundingRect().center();
        //    points_items[0]->setPos(mouseEvent->scenePos()-p);
        //}
        //return;
    }    
}

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << Q_FUNC_INFO << mouseEvent->scenePos();
    QGraphicsScene::mousePressEvent(mouseEvent);
    this->update();
}

void GraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent * me)
{
    qDebug() << Q_FUNC_INFO << me->scenePos();
    
    Qt::MouseButtons btn = Qt::RightButton;

    
    if (me->button() != Qt::RightButton)
        return;
    
    int radius = 2;
    //QGraphicsEllipseItem * ellipse = this->addEllipse(me->scenePos().x() - radius, me->scenePos().y() - radius, radius*2, radius*2);

    
    //points_items.append( );
    //this->addItem(new PointsGraphicsItem(me->scenePos()));
    
    //ellipse->setBrush(Qt::white);
//    m_points.append(me->scenePos());
//    if(m_points.size() == 3)
//    {
//
//        qDebug() << "making arc";
//        static int i = 0;
////           ArcGraphicsItem * arc = ;
//
//        m_items.append(new ArcGraphicsItem(i++, m_points.at(0), m_points.at(1), m_points.at(2)));
//        this->addItem(m_items.last());
////           arc->update();
//
//        m_points.clear();
//    }

    QGraphicsScene::mouseReleaseEvent(me);
}


void GraphicsScene::updatePoints(QList<QPointF3DProj> p)
{
    //QList<QGraphicsItem *> items = this->items(Qt::AscendingOrder);
    //qDebug() << "items " <<  _points_items.size();
    //qDebug() << "p " << p.size();
    
    int selected_idx=-1;
    
    
    if (p.size() == 0)
    {
        _points_items.clear();
    } else if (_points_items.size() == 0)
    {
        // Add everything
        for (int idx = 0; idx < p.size(); idx++)
        {
            _points_items.append(new PointsGraphicsItem(p[idx]));
            _points_items.last()->setPos(p[idx]);
            this->addItem(_points_items.last());
        }           
    } else {
    
        int npidxs[p.size()] = {-1};
        int opidxs[_points_items.size()] = {-1};
        
    
        for (int idx = 0; idx<p.size(); idx++)
        {
            npidxs[idx] = -1;
            for (int j=0; j < _points_items.size(); j++)
            {
                if (idx == 0) {
                    //initialize opidxs
                    opidxs[j] = -1;
                }
                
                if (_points_items[j]->id() == p[idx].id)
                {
                    //qDebug() << "Settin position " << p[idx].x() << " " <<  p[idx].y() << " for idx: " << idx;
                    _points_items[j]->setPoint3DProj(p[idx]);
                    _points_items[j]->setPos(p[idx]);
                    npidxs[idx] = j;
                    opidxs[j] = idx;
                    continue;
                }
            }
        }
        
        // remove old items
        int sz = _points_items.size();
        for (int j = sz -1; j >= 0; j--)
        {
            if (opidxs[j] == -1) {
                //qDebug() << "Removing item " << j;
                this->removeItem(_points_items[j]);
                _points_items.removeAt(j);
            }
        }
        
        // Add new items
        for (int idx = 0; idx < p.size(); idx++)
        {
            if (npidxs[idx] == -1) {
                //qDebug() << "Adding item " << idx;
                _points_items.append(new PointsGraphicsItem(p[idx]));
                _points_items.last()->setPos(p[idx]);
                this->addItem(_points_items.last());
            }
        } 
    }   

    
    for (int j=0; j < _points_items.size(); j++)
    {
        if (_points_items[j]->isSelected() == true)
        {
            selected_idx = j;
            break;
        }
    }

    
    // delete all distances...update planned but boring
    for (int i = 0; i < _distance_items.size(); i++)
    {
        this->removeItem(_distance_items[i]);
    }
    _distance_items.clear();
    
    if (selected_idx != -1)
    {
        for (int i = 0; i < _points_items.size(); i++)
        {
            // distance is 0 in this case
            if (i == selected_idx)
                continue;
            
            _distance_items.append(new DistanceGraphicsItem(_points_items[selected_idx]->getPoint3DProj(),
                                                    _points_items[i]->getPoint3DProj()));
            this->addItem(_distance_items.last());
        }
    }
    
}
