TEMPLATE = lib
QT       += core gui widgets 
CONFIG   += no_keywords
CONFIG   += staticlib

HEADERS     = DistanceGraphicsItem.hpp PointsGraphicsItem.hpp graphicsscene.hpp InteractiveLiveViewer.hpp geometry.hpp
SOURCES     = DistanceGraphicsItem.cpp PointsGraphicsItem.cpp graphicsscene.cpp geometry.cpp InteractiveLiveViewer.cpp 

LIBS += -lm
