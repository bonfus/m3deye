#include <QtGui> 
#include <QLabel> 
#include <QFileDialog>
#include <QInputDialog>
#include <QDir>
#include <QGroupBox>
#include <QRadioButton>
#include <QSignalMapper>
#include <iostream>
#include "qtstview.hpp"

#define ST_PARAMS "/home/pie/Dev/m3deye/libST/config.yml"

#define APPNAME "Kinopsys"

#ifdef Q_WS_X11
#include <QX11EmbedContainer>
#endif
 
QtSTView::QtSTView() : QMainWindow()
{
    
    this->setWindowTitle( APPNAME );
    
    this->setWindowIcon(getIcon("kinopsys"));    
    
    // PREPARE OUTPUT WIDGETS
#ifdef Q_WS_X11
    _videoWidget=new QX11EmbedContainer(this);
#else
    _videoWidget=new QFrame(this);

    //NOTE ...this may help
        // QPalette palette = frame->palette();
        // palette.setColor( backgroundRole(), QColor( 0, 0, 255 ) );
        // frame->setPalette( palette );
        // frame->setAutoFillBackground( true );    
    
#endif

    _ILV = new InteractiveLiveViewer(this);



    // PREPARE TOOLBAR -> move in its own class!
    
    _sourcesComboBox = new QComboBox(this);
    _sourcesComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    // parse and update sources
    // updateSources(false);
    
    _goButton = new QToolButton(this);
    _goButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _goButton->setText("Live");
    _goButton->setIcon(getGraphIcon());
    _goButton->setIconSize(QSize(32,32));
    
    _showButton = new QToolButton(this);
    _showButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _showButton->setText("Show");
    _showButton->setIcon(getEyeIcon());
    _showButton->setIconSize(QSize(32,32));
    
    _saveButton = new QToolButton(this);
    _saveButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _saveButton->setText("Save");
    _saveButton->setIcon(getIcon("save"));
    _saveButton->setIconSize(QSize(32,32));
    
    _stopButton = new QToolButton(this);
    _stopButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _stopButton->setText("Stop");
    _stopButton->setIcon(getStopIcon());
    _stopButton->setIconSize(QSize(32,32));

    _addSourceButton = new QToolButton(this);
    _addSourceButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _addSourceButton->setText("Add source");
    _addSourceButton->setIcon(getAddIcon());
    _addSourceButton->setIconSize(QSize(32,32));
    
    _searchEyesButton = new QToolButton(this);
    _searchEyesButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _searchEyesButton->setText("Search eyes");
    _searchEyesButton->setIcon(getSearchIcon());
    _searchEyesButton->setIconSize(QSize(32,32));    

    _mainToolBar = new QToolBar(this);
    _mainToolBar->addWidget(_goButton); 
    _mainToolBar->addWidget(_saveButton); 
    _mainToolBar->addWidget(_showButton); 
    _mainToolBar->addWidget(_stopButton); 
    _mainToolBar->addWidget(_addSourceButton);
    _mainToolBar->addWidget(_searchEyesButton);
    _mainToolBar->addWidget(_sourcesComboBox); 



    // PREPARE status bar
    _mainStatusBar = new QStatusBar(this);
    
    
    QWidget *msgWdg = new QWidget(this);
    QHBoxLayout *msgLayout = new QHBoxLayout(msgWdg);
    _msgImg = new QLabel(msgWdg);
    _msgImg->setScaledContents(true);
    _msgImg->setFixedSize(16,16);    
    
    _msgText = new QLabel(msgWdg);
    
    msgLayout->addWidget(_msgImg);
    msgLayout->addWidget(_msgText);
    msgWdg->setLayout(msgLayout);
    
    _mainStatusBar->addPermanentWidget(msgWdg);    
    
    
    
    // camera dock
    _cameraDock = new QDockWidget(this);
    _cameraDockWidget = new QtSTCameraControl(this);
    _cameraDock->setWidget(_cameraDockWidget);
    _cameraDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea);
    
    // PREPARE CENTRAL LAYOUT
    QWidget *winCenter = new QWidget(this);
    QVBoxLayout *centralLayout = new QVBoxLayout(winCenter); // THIS WILL NOT BE FREEED!!!
    _viewersStack = new QStackedWidget(this);
    
    _viewersStack->addWidget(_videoWidget);
    _viewersStack->addWidget(_ILV);
    
    QGroupBox    * outputSelectGroup = new  QGroupBox(this);
    QRadioButton * outputSelect[2];
    

	outputSelect[0] = new QRadioButton(QString("Video"), this);
	outputSelect[1] = new QRadioButton(QString("Interactive"), this);
    outputSelect[0]->setChecked(true);
    
    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(outputSelect[0]);
    hbox->addWidget(outputSelect[1]);
    hbox->addStretch(1);
    outputSelectGroup->setLayout(hbox);
    
    
    centralLayout->addWidget(outputSelectGroup);
    centralLayout->addWidget(_viewersStack);
    
    
    // Set layout in QWidget
    winCenter->setLayout(centralLayout);
    QSignalMapper * mSignalMapper = new QSignalMapper(this);
    
    for (int i=0; i<2; i++)
    {
        connect(outputSelect[i], SIGNAL(clicked()), mSignalMapper, SLOT(map()));
        mSignalMapper->setMapping(outputSelect[i], i);
    }
    connect(mSignalMapper, SIGNAL(mapped(int)), _viewersStack, SLOT(setCurrentIndex(int)));
    
    // FINISHED PREPARING CENTRAL LAYOUT
    
    
    //layout->addWidget(_mainToolBar);
    this->addToolBar(_mainToolBar);
    this->setCentralWidget(winCenter); 
    this->addDockWidget(Qt::RightDockWidgetArea, _cameraDock);
    this->setStatusBar(_mainStatusBar);
    
    
    
    connect(_goButton, SIGNAL(clicked(bool)), this, SLOT(btnGo()));
    connect(_showButton, SIGNAL(clicked(bool)), this, SLOT(btnShow()));
    connect(_saveButton, SIGNAL(clicked(bool)), this, SLOT(btnSave()));
    connect(_stopButton, SIGNAL(clicked(bool)), this, SLOT(btnStop()));
    connect(_addSourceButton, SIGNAL(clicked(bool)), this, SLOT(btnAddSource()));
    connect(_searchEyesButton, SIGNAL(clicked(bool)), this, SLOT(btnSearchEyes()));
    connect(_sourcesComboBox, SIGNAL(activated(int)), this, SLOT(cbSourcesOnActivated(int)));
    
    
    // connect object
    int windid = _videoWidget->winId();
    
    // init st thread

    STThread *worker = new STThread;
    worker->stParseConfig(QString(ST_PARAMS));
    worker->stInitRender(windid);
    _current_sources = worker->stGrabSources();
    worker->moveToThread(&stThr);
    
    // connection to thread
    connect(&stThr, SIGNAL(finished()), worker, SLOT(deleteLater()));
    
    //
    connect(this, SIGNAL(Run(const bool, const bool)), worker, SLOT(stRun(const bool, const bool)));
    connect(this, SIGNAL(Show()), worker, SLOT(stShow()));
    connect(this, SIGNAL(Save(QString,QString)), worker, SLOT(stSave(QString,QString)));
    connect(this, SIGNAL(Stop()), worker, SLOT(stStop()));
    connect(this, SIGNAL(AddFiles(QString, QString , QString)), worker, SLOT(stAddFiles(QString, QString , QString)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SetSource(Source_t)), worker, SLOT(stSetSource(Source_t)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SearchEyes(unsigned int)), worker, SLOT(stSearchEyes(unsigned int)));
    connect(this, SIGNAL(ParseConfig(QString)), worker, SLOT(stParseConfig(QString)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(updateSources()), worker, SLOT(stUpdateSources()));
    connect(_cameraDockWidget, SIGNAL(AdjustCamProp(CmdTypes,bool)), worker, SLOT(stSetCameraProperty(CmdTypes, bool)));
    
    connect(worker, SIGNAL(clearState()), this, SLOT(clearStatusMessages()));
    connect(worker, SIGNAL(reportSources(QVector<Source_t>)), this, SLOT(populateSources(QVector<Source_t>)));
    connect(worker, SIGNAL(reportMsg(InfoMSG)), this, SLOT(statusBarSetMsg(InfoMSG)));
    connect(worker, SIGNAL(reportMsg(StatusMSG)), this, SLOT(statusBarSetMsg(StatusMSG)));
    
    // just for now...better handling later...
    connect(worker, SIGNAL(reportPoints(QList<Point3D_t>)), _ILV, SLOT(newPoints(QList<Point3D_t>)));
    
    stThr.start();
    
    updateComboBox(false);
}

QtSTView::~QtSTView() {
         stThr.quit();
         stThr.wait();
         delete _cameraDockWidget;
}

void QtSTView::btnGo()
{
    //Q_EMIT(AddFiles("/home/pie/medusa/1434271888/eye1.h264", "/home/pie/medusa/1434271888/eye2.h264","test"));    
    //Q_EMIT(SetSource(0));
    Q_EMIT(Run(true,true));
}

void QtSTView::btnShow()
{
    //Q_EMIT(SetSource(0));
    Q_EMIT(Show());
    
}

void QtSTView::btnStop()
{
    Q_EMIT(Stop());
}

void QtSTView::cbSourcesOnActivated(int index)
{
    Q_EMIT(SetSource(_current_sources[index]));
}

void QtSTView::btnAddSource()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                             "/home",
                                             QFileDialog::ShowDirsOnly
                                             | QFileDialog::DontResolveSymlinks);
    
    if (dir.isEmpty())
        return;
    
    QDir d(dir);
    d.makeAbsolute();
    
    //QFileInfoList allfiles = d.entryInfoList();
    
    
    QString LFPath = d.filePath(d.dirName() + "-L.ts");
    QString RFPath = d.filePath(d.dirName() + "-R.ts");
    
    QFileInfo checkFileL(LFPath);
    QFileInfo checkFileR(RFPath);
    if (!(checkFileL.exists() && checkFileL.isFile() && checkFileR.exists() && checkFileR.isFile()))
    {
        // warn user
        //return;
        LFPath = d.filePath(d.dirName() + "-L.h264");
        RFPath = d.filePath(d.dirName() + "-R.h264");
        
        checkFileL.setFile(LFPath);
        checkFileR.setFile(RFPath);
        if (!(checkFileL.exists() && checkFileL.isFile() && checkFileR.exists() && checkFileR.isFile()))
        {
            WarningBox(APPNAME, "Source format invalid!");
            return;
            
            
        }        
        
    }
    
    Q_EMIT(AddFiles(LFPath, RFPath,d.dirName()));
    Q_EMIT(updateSources());
}

void QtSTView::btnSave()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                             "/home",
                                             QFileDialog::ShowDirsOnly
                                             | QFileDialog::DontResolveSymlinks);
    
    if (dir.isEmpty())
    {
        //WarningBox(APPNAME, "");
        return;
    }    
    
    QDir d(dir);
    d.makeAbsolute();
    
    bool ok;
    QString text = QInputDialog::getText(this, tr("Info required"),
                                         tr("Experiment name"), QLineEdit::Normal,
                                         "", &ok);
    if (!ok || text.isEmpty())
    {
        WarningBox(APPNAME, "Invalid acquisition name.");
        return;
    }

    if (d.exists(text))
    {
        WarningBox(APPNAME, "Acquisition already exists.");
        return;
    }

    
    if (!d.mkdir(text))
    {
        WarningBox(APPNAME, "Cannot prepare acquisition. Check writing permissions.");
        return;
    }

    if (!d.cd(text))
    {
        WarningBox(APPNAME, "Cannot prepare acquisition. Check access permissions.");
        return;
    }

    
    QString LFPath = d.filePath(text + "-L.ts");
    QString RFPath = d.filePath(text + "-R.ts");
    
    QFileInfo checkFileL(LFPath);
    QFileInfo checkFileR(RFPath);
    if (checkFileL.exists() || checkFileL.isFile() || checkFileR.exists() || checkFileR.isFile())
    {
        // warn user
        WarningBox(APPNAME, "Acquisition files already present. Not overwriting.");
    }
    
    Q_EMIT(Save(LFPath,RFPath));
}

void QtSTView::btnSearchEyes()
{
    Q_EMIT(SearchEyes(5000)); //timeout in ms
    Q_EMIT(updateSources());
}

void QtSTView::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, APPNAME,
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
        return;
    } else {
        Q_EMIT(Stop());
        event->accept();
    }
}

void QtSTView::clearStatusMessages()
{
    _mainStatusBar->clearMessage();
    _msgImg->setPixmap(QPixmap());
    _msgText->setText("");
}


void QtSTView::populateSources(QVector<Source_t> sources)
{
    _current_sources = sources;
    updateComboBox(true);
}

void QtSTView::statusBarSetMsg(InfoMSG msg)
{    
    switch (msg.type)
    {
        case INFO:
           _msgImg->setPixmap(getPixmap("info"));
           break;
        case WARNING:
           _msgImg->setPixmap(getPixmap("warning"));
           break;
        case ERROR:
           _msgImg->setPixmap(getPixmap("error"));
           break;
    }
    
    _msgText->setText(msg.message);
}

void QtSTView::statusBarSetMsg(StatusMSG msg)
{    
    _mainStatusBar->showMessage(msg.message);
}

void QtSTView::updateComboBox(bool alert)
{
    _sourcesComboBox->clear();
    for (unsigned int i=0; i< _current_sources.size(); i++)
    {
        _sourcesComboBox->addItem((_current_sources[i].type == ST_FILES) ? getMovieIcon() : getCameraIcon() , QString(_current_sources[i].name));
    }
}


bool QtSTView::YesNoBox(QString title, QString text) {
  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, title, text,
                                QMessageBox::Yes|QMessageBox::No);
  if (reply == QMessageBox::Yes) {
    return true;
  } else {
    return false;
  }
}

void QtSTView::WarningBox(QString title, QString text) {
  QMessageBox::StandardButton reply;
  QMessageBox::question(this, title, text,
                                QMessageBox::Ok);
}


// HERE STARTS THE OUTHER CLASS

QtSTCameraControl::QtSTCameraControl(QWidget *parent  ) : QWidget( parent )
{
    
    QGridLayout *layout = new QGridLayout(this);
    layout->setSizeConstraint (QLayout :: SetFixedSize);
    
    
    _moreBrBtn = new QPushButton(getIcon("morebrightness"),"", this);        
    _lessBrBtn = new QPushButton(getIcon("lessbrightness"),"",this);
    _moreCoBtn = new QPushButton(getIcon("morecontrast"),"",this);
    _lessCoBtn = new QPushButton(getIcon("lesscontrast"),"",this);
    _moreSaBtn = new QPushButton(getIcon("moresaturation"),"",this);
    _lessSaBtn = new QPushButton(getIcon("lesssaturation"),"",this);
    _moreExBtn = new QPushButton(getIcon("moreshutterspeed"),"",this);
    _lessExBtn = new QPushButton(getIcon("lessshutterspeed"),"",this);
    _saveCamSetsBtn = new QPushButton("Store",this);

	layout->addWidget(_moreBrBtn,0,1);
	layout->addWidget(_lessBrBtn,0,0);		
	layout->addWidget(_moreCoBtn,1,1);		
	layout->addWidget(_lessCoBtn,1,0);		
	layout->addWidget(_moreSaBtn,2,1);		
	layout->addWidget(_lessSaBtn,2,0);
	layout->addWidget(_moreExBtn,3,1);		
	layout->addWidget(_lessExBtn,3,0);
    layout->addWidget(_saveCamSetsBtn,4,0,1,2);
    
    connect(_moreBrBtn, SIGNAL(clicked(bool)), this, SLOT(btnMoreBrightness()));
    connect(_lessBrBtn, SIGNAL(clicked(bool)), this, SLOT(btnLessBrightness()));
    connect(_moreCoBtn, SIGNAL(clicked(bool)), this, SLOT(btnMoreContrast()));
    connect(_lessCoBtn, SIGNAL(clicked(bool)), this, SLOT(btnLessContrast()));
    connect(_moreSaBtn, SIGNAL(clicked(bool)), this, SLOT(btnMoreSaturation()));
    connect(_lessSaBtn, SIGNAL(clicked(bool)), this, SLOT(btnLessSaturation()));
    connect(_moreExBtn, SIGNAL(clicked(bool)), this, SLOT(btnMoreExposure()));
    connect(_lessExBtn, SIGNAL(clicked(bool)), this, SLOT(btnLessExposure()));
    
    setLayout(layout);
}
QtSTCameraControl::~QtSTCameraControl()
{
    
}
