
#include <sigc++/sigc++.h>
#include <QtGui> 
#include <QWidget>
#include <QStatusBar> 
#include <QDockWidget> 
#include <QMainWindow>
#include <QPushButton>
#include <QToolButton>
#include <QToolBar>
#include <QComboBox>
#include <QStackedWidget>
#include <QFrame>
#include <QLayout>
#include <QMessageBox>
#include "qtstrun.hpp"
#include "images.hpp"
#include "InteractiveLiveViewer.hpp"

class QtSTCameraControl : public QWidget
{
    Q_OBJECT

public:
    QtSTCameraControl(QWidget *parent = 0 );
    ~QtSTCameraControl();

public Q_SLOTS:
    void btnMoreBrightness(){Q_EMIT(AdjustCamProp(SET_BRIGHTNESS, true));};
    void btnLessBrightness(){Q_EMIT(AdjustCamProp(SET_BRIGHTNESS, false));};
    void btnMoreContrast()  {Q_EMIT(AdjustCamProp(SET_CONTRAST, true));};
    void btnLessContrast()  {Q_EMIT(AdjustCamProp(SET_CONTRAST, false));};
    void btnMoreSaturation(){Q_EMIT(AdjustCamProp(SET_SATURATION, true));};
    void btnLessSaturation(){Q_EMIT(AdjustCamProp(SET_SATURATION, false));};
    void btnMoreExposure()  {Q_EMIT(AdjustCamProp(SET_SHUTTERSPEED, true));};
    void btnLessExposure()  {Q_EMIT(AdjustCamProp(SET_SHUTTERSPEED, false));};
    
    void btnSaveCameraSettings()  {};
    
Q_SIGNALS:
    void AdjustCamProp(CmdTypes what, bool increment);

private:
    QPushButton * _moreBrBtn;
    QPushButton * _lessBrBtn;
    QPushButton * _moreCoBtn;
    QPushButton * _lessCoBtn;
    QPushButton * _moreSaBtn;
    QPushButton * _lessSaBtn;
    QPushButton * _moreExBtn;
    QPushButton * _lessExBtn;
    QPushButton * _saveCamSetsBtn;
};


class QtSTView : public QMainWindow
{
    Q_OBJECT
    QThread stThr;
public:
    QtSTView();
    ~QtSTView();
         
public Q_SLOTS:
//callbacks for widgets in application
    void btnGo(); // slot for button
    void btnShow(); // slot for show button
    void btnSave(); // slot for show button
    void btnStop(); // slot for stop button
    void btnAddSource();
    void btnSearchEyes();
    void cbSourcesOnActivated(int index); // combo box selected
    
//callbacks for thread
    void clearStatusMessages(); // slot for ST thread
    void populateSources(QVector<Source_t> sources);
    void statusBarSetMsg(InfoMSG); 
    void statusBarSetMsg(StatusMSG); 
    void updateComboBox(bool alert);

// these are the signals that are used to comunicate with the thread
// these are called by Q_EMIT and connect the stBlaBla functions
Q_SIGNALS:
    void Run(const bool, const bool); // signal to thread
    void Show(); // signal to thread
    void Save(QString, QString ); // signal to thread
    void Stop(); // signal to thread
    void AddFiles(QString, QString , QString);
    void SetSource(Source_t);
    void updateSources();
    void SearchEyes(unsigned int);
    void ParseConfig(QString);
    //void AdjustCamProperty(CmdTypes what, bool increment);
    
private:

    bool YesNoBox(QString, QString);
    void WarningBox(QString, QString);

    QToolBar *_mainToolBar;
    QStatusBar *_mainStatusBar;
    QDockWidget *_cameraDock;
    QtSTCameraControl * _cameraDockWidget;
    // VIEWERS WIDGETS
    QStackedWidget * _viewersStack;
    QWidget * _videoWidget;
    InteractiveLiveViewer * _ILV;
    
    QToolButton * _goButton;
    QToolButton * _showButton;
    QToolButton * _saveButton;
    QToolButton * _stopButton;
    QToolButton * _addSourceButton;
    QToolButton * _searchEyesButton;
    QComboBox * _sourcesComboBox;
    
    QLabel * _msgImg;
    QLabel * _msgText;
    
    QVector<Source_t> _current_sources;
    
    void closeEvent (QCloseEvent *event);
};

    
