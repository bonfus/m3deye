#include <QIcon>

QIcon getGraphIcon();
QIcon getAddIcon();
QIcon getStopIcon();
QIcon getCameraIcon();
QIcon getMovieIcon();
QIcon getSearchIcon();
QIcon getEyeIcon();
QIcon getIcon(const char * name);
QPixmap getPixmap(const char * name);
