#ifndef QSTRUN_H
#define QSTRUN_H

#include <QtGui> 
#include <iostream>

#include "libST.hpp"

#ifdef Q_WS_X11
#include <QX11EmbedContainer>
#endif

enum MSGType
{
    INFO,
    WARNING,
    ERROR
};

enum StatusTask
{
    IDLE,
    SHOW,
    LIVE,
    SAVE
};

typedef struct 
{
    MSGType type;
    QString message;
} InfoMSG;

typedef struct 
{
    StatusTask task;
    QString message;
} StatusMSG;

class STThread : public QObject
{
    Q_OBJECT

public:
    STThread();
    ~STThread();
    QVector<Source_t> stGrabSources();

public Q_SLOTS:
    void stShow();
    void stRun(const bool bSendPoints, const bool bShowVideo);
    void stSave(QString Lfilename, QString Rfilename);
    void stStop();
    
    void stInitRender(int winid);
    void stParseConfig(QString fname);
    
    
    void stAddFiles(QString lfile, QString rfile, QString name);
    void stSetSource(Source_t source);
    void stSearchEyes(unsigned int timeout);
    
    void stUpdateSources();
         
    void stSetCameraProperty(CmdTypes what, bool raise);
    
    //callbacks for ST
    void stGotPoints(Point3D_t *, int, int, int);
    void stGotMessage(stMessages);
    void stGotStatus(stStatus);

Q_SIGNALS:
    void clearState();
    void reportMsg(InfoMSG b);
    void reportMsg(StatusMSG b);
    void reportSources(QVector<Source_t> sources);
    void reportPoints(QList<Point3D_t> points);
    
private:
    
   ST * s;

};


#endif
