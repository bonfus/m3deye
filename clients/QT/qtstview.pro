QT       += core gui widgets 
CONFIG   += no_keywords

HEADERS     = images.hpp qtstrun.hpp qtstview.hpp
SOURCES     = images.cpp qtstrun.cpp qtstview.cpp main.cpp

INCLUDEPATH += . ../../libST ./ILV/ILV
INCLUDEPATH += /usr/include/sigc++-2.0 /usr/lib/sigc++-2.0/include

LIBS += -lsigc-2.0  
LIBS += -L../../libST -lST -L./ILV/ILV -lILV

QMAKE_CXXFLAGS += -DHAVE_X11 -g -O0 -std=c++11 -pthread

# install
target.path = myqtapp
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS *.pro
sources.path = .

INSTALLS += target sources
