#include <iostream>
#include <stdio.h>
#include <sigc++/sigc++.h>
#include <ncurses.h>
#include <limits>
#include "libST.hpp"

#define ST_PARAMS "/home/pie/Dev/m3deye/libST/config.yml"

int row,col;
float minx = std::numeric_limits<float>::max();
float maxx = std::numeric_limits<float>::min();
float miny = std::numeric_limits<float>::max();
float maxy = std::numeric_limits<float>::min();

void got_points(Point3D_t * points, int nPoints, int iFrame, int sync)
{
    
    //Draws the 'Y' triangle
    attrset(COLOR_PAIR(1));
    clear() ;    
    
    float x[nPoints], y[nPoints];
    float posx;
    float posy;
    
    // this must be changed to check all points!
    int i =0;
    for (int i=0; i<nPoints; i++)
    {
        //std::cout<<(int)(50+(100*points[i].x))<<" "<<(int)(50+(100*points[i].y))<<std::endl;
        x[i]=points[i].x;
        y[i]=points[i].y;
        
        if (x[i]< minx)
            minx=x[i];
        if (y[i] < miny)
            miny=y[i];
        if (x[i]>maxx)
            maxx=x[i];
        if (y[i]>maxy)
            maxy=y[i];
    }
    
    float deltax = (maxx-minx)+0.0001;
    float deltay = (maxy-miny)+0.0001;
        
    for (int j=0; j<nPoints; j++)
    {
        posx= ((x[j]-minx)/deltax)*((float)row);
        posy = ((y[j]-miny)/deltay)*((float)col);
        
        move ((int)posx,(int)posy);
        printw("o");
    
        refresh();
        
    }
    delete [] points;
    
}

int main()
{
    
    ST s;
    s.points_ready.connect( sigc::ptr_fun(got_points) );
    s.parse_config(ST_PARAMS);
    
    // start ncurses
    initscr();
    getbegyx(stdscr,row,col);
    if (row != 0 || col != 0)
        return 0;
    
    getmaxyx(stdscr,row,col);
    start_color();
    //Defines the Color pairs
    init_pair(1, 0, 1);
    init_pair(2, 3, 0);
    init_pair(3, 4, 7);
    init_pair(4, 0, 3);

    s.addFiles("/home/pie/medusa/1434271888/eye1.h264", "/home/pie/medusa/1434271888/eye2.h264","test"); 
    s.setSource(ST_FILES,0);
    s.run(true,false,true); // return points, do not show video, set blocking run
    
    getch();
    endwin();
    return 0;
}
