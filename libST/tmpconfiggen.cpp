#include <opencv2/core/core.hpp>
#include <iostream>

int main()
{
    cv::FileStorage intrinsics("intr.yaml", cv::FileStorage::READ);
    cv::FileStorage extrinsics("extr.yaml", cv::FileStorage::READ);
    cv::FileStorage fs("parameters.yaml", cv::FileStorage::WRITE);
    fs << "calibrations";
    fs << "[" ;
    for(int i=0; i<4; i++) {
        fs << "{" ;
        fs << "l" << cv::Mat::zeros(4,1,CV_64F);
        fs << "c" << cv::Mat::zeros(2,1,CV_64F);
        fs << "t" << cv::Mat::zeros(4,4,CV_64F);
        fs << "}" ;
    }
    fs << "]" ;

    return 0;
}
