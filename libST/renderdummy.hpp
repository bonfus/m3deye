

#include "render.hpp"

class DummyVideoRender : public VideoRender
{
public:
    void render(cv::Mat & frame);
    void setDrawable(int dummy); 
    void setSourceResolution(int w, int h);
    inline void cleanup(){};
};


