// https://github.com/SergiusTheBest/plog#step-1-adding-includes
// http://www.drdobbs.com/cpp/201804215
// https://bitbucket.org/volkanozyilmaz/logcpp
#include "libST.hpp"

#define MAXEYES 10
#define LENEYEIDDIGITS 1 // 1 digit for eyes id, from 0 to 9
#define LEYENAME "leye"
#define REYENAME "reye"

#include "renderdummy.hpp"

#ifdef HAVE_X11
#include "renderx11.hpp"
#endif

ST::ST() : avahiInspector(NULL), _acq(false)
{
    FILELog::ReportingLevel() = ldebug4;
    //FILELog::ReportingLevel() = lerror;
    //FILE* log_fd = stderr;
    //Output2FILE::Stream() = log_fd;
    
    L_(ldebug2) << WHERE << "ST constructor.";
    

    render= new DummyVideoRender;

    // TODO check if have avahi with ifdef
    avahiInspector = new Avahi;
    
    

}

ST::~ST()
{
    L_(ldebug2) << WHERE << "ST destructor.";

    if (render!=NULL)
    {
        delete render;
    }


    if (avahiInspector!=NULL)
    {
        delete avahiInspector;
    }

        
}


bool ST::initRender(VideoRenderType rt)
{    
    if (render!=NULL)
    {
        delete render;
    }
    
    switch (rt)
    {

#ifdef HAVE_X11       
        case X11:
            render = new X11VideoRender;
            return true;
#endif            
            
        case WIN:
        case QT:
        case NONE:
        default:
            render = new DummyVideoRender;
            break;
    }
    return false;
    
    
}


/*!
 * \brief searchEyes This method is used to probe eyes ip addresses with Avahi or
 *        other methods to be implemented
 * \param timeout in milliseconds
 * \return true if at least a couple of eyes was found. False if avahi is not available or
 *         if no mathes between eyes was found.
 *
 * This method is used to automatically identify the addresses of the rpis. The protocol 
 * is based on the hostname and on the mountpoint which is composed from the name of the rpis services.
 * 
 * The name of the service must be EYENAME#-rtspmountpoint. Example leye1-rpi640x480.
 */
bool ST::searchEyes(int timeout)
{
    if (avahiInspector)
        avahiInspector->Run(timeout);
    else
        return false;
        
    // validate results
    bool foundR=false, foundL=false;
    int idx=0;
    int idL=0;
    int idR=0;
    
    EyeData_t * resL[MAXEYES];
    EyeData_t * resR[MAXEYES];
    
    // this function sucks!
    while (true)
    {
        EyeData_t * res = avahiInspector->getResults(idx);
        idx++;
        if (res == NULL)
            break;
        L_(ldebug2) << WHERE << "Found service " << res->name << ": "
                    << res->type <<", " << res->domain <<", " << res->host_name 
                    <<", " << res->address <<", " << res->port;
        if ((strncmp(res->name, LEYENAME, strlen(LEYENAME))==0))
        {
            foundL = true;
            resL[idL] = new EyeData_t;
            memcpy((void*) resL[idL] , (void*)res , sizeof(EyeData_t));
            idL++;
            L_(ldebug2) << WHERE << "Found leye.";
            continue;
        }
        
        if ((strncmp(res->name, REYENAME, strlen(REYENAME))==0))
        {
            foundR = true;
            resR[idR] = new EyeData_t;
            memcpy((void*)resR[idR] , (void*)res , sizeof(EyeData_t));
            idR++;
            L_(ldebug2) << WHERE << "Found reye.";
            continue;
        }
        
    }
    
    int lenLname = strlen(LEYENAME);
    int lenRname = strlen(REYENAME);
    
    if (foundL && foundR)
    {
        // prepare to set mount points
        for (int i=0; i<idL; i++)
        {
            if (resL[i]->name == NULL)
            {
                L_(lerror) << WHERE <<"Something very very very bad happened!";
            }

            resL[i]->mountpoint = strdup(resL[i]->name+lenLname+LENEYEIDDIGITS+1); //+1 is for the "-" in the name leye0-mountpoint
            if (resL[i]->mountpoint == NULL)
            {
                L_(lerror) << WHERE <<"Something very very very bad happened!";
            }
        }         
        for (int j=0; j<idR;j++)
        {
            
            if (resR[j]->name == NULL)
            {
                L_(lerror) << WHERE <<"Something very very very bad happened!";
            }
            resR[j]->mountpoint = strdup(resR[j]->name+lenRname+LENEYEIDDIGITS+1); //+1 is for the "-"
            if (resR[j]->mountpoint == NULL)
            {
                L_(lerror) << WHERE <<"Something very very very bad happened!";
            }                
        }

        //it's time to metch eyes
        bool success=false;
        
        for (int i=0; i<idL; i++)
        {
            for (int j=0; j<idR;j++)  
            {      
                // now make comparisions
                
                if (strlen(resL[i]->mountpoint) != (strlen(resR[j]->mountpoint)))
                {
                    if (j<(idR-1))
                        continue;
                    else
                    {
                        L_(linfo) << WHERE <<"Could not find couple for "<< resL[i]->name << " " <<  resR[j]->name;
                        continue;
                    }
                }
                
                if ((strcmp(resL[i]->mountpoint,
                            resR[j]->mountpoint)==0) && //strlen(resL[i]->mountpoint) is equal to strlen(resR[j]->mountpoint)
                    (strncmp(resL[i]->name+lenLname+LENEYEIDDIGITS,
                            resR[j]->name+lenRname+LENEYEIDDIGITS,
                            1)==0) )
                            
                {
                    if (!stMgr.checkEyesPresent(*(resL[i]), *(resR[j])))
                    {
                        stMgr.addEyes(*(resL[i]), *(resR[j]), strdup(resL[i]->mountpoint));
                        success=true;
                    }
                    break;
                }
                
                //if this is the last, nothing will be found
                if (j<(idR-1))
                    continue;
                else
                    L_(linfo) << WHERE <<"Could not find couple for "<< resL[i]->name << " " <<  resR[j]->name;
                
            }
        }
        // free up stuff!!!
        return success;
    }
    else
    {
        // free up stuff!!!
        return false;
    }
    

    
}


/*!
 * \brief addEyes This method is used to eyes address manually
 * \param hostnameL
 * \param addressL
 * \param nameL
 * \param mountpointL
 * \param rtsp_portL
 * \param com_portL
 * \param hostnameR
 * \param addressR
 * \param nameR
 * \param mountpointR
 * \param rtsp_portR
 * \param com_portR
 * \return true if eye was added. False otherwise (example, eye couple already present).
 *
 * This method is used to manually add the addresses of the rpis. 
 */
bool ST::addEyes(const char * hostnameL, const char * addressL, const char * nameL, 
                 const char * mountpointL, int rtsp_portL, int com_portL,
                 const char * hostnameR, const char * addressR, const char * nameR, 
                 const char * mountpointR, int rtsp_portR, int com_portR)
{
    EyeData_t leye;
    EyeData_t reye;
    
    // Left
    leye.host_name = new char [strlen(hostnameL) + 1];
    strcpy(leye.host_name, hostnameL);

    leye.name = new char [strlen(nameL) + 1];
    strcpy(leye.name, nameL);

    leye.address = new char [strlen(addressL) + 1];
    strcpy(leye.address, addressL);

    leye.mountpoint = new char [strlen(mountpointL) + 1];
    strcpy(leye.mountpoint, mountpointL);    

    leye.port = com_portL;

    leye.domain = NULL;
    leye.type = NULL;
    
    // Right
    reye.host_name = new char [strlen(hostnameR) + 1];
    strcpy(reye.host_name, hostnameR);

    reye.name = new char [strlen(nameR) + 1];
    strcpy(reye.name, nameR);

    reye.address = new char [strlen(addressR) + 1];
    strcpy(reye.address, addressR);    

    reye.mountpoint = new char [strlen(mountpointR) + 1];
    strcpy(reye.mountpoint, mountpointR);    


    reye.port = com_portR;

    reye.domain = NULL;
    reye.type = NULL;
    
    return stMgr.addEyes(leye, reye, std::string(nameL));    
}

/*!
 * \brief addFiles This method is used to add a couple of files
 * \param lvideo path of the left video.
 * \param rvideo path of th eright video.
 * 
 * This function adds a file source to the statusManager.
 */
bool ST::addFiles(const char* lvideo,const char * rvideo, const char * name)
{
    // his function needs to be rewritten
    FileData_t lfile;
    FileData_t rfile;
    
    // Left
    lfile.path = new char [strlen(lvideo) + 1];
    strcpy(lfile.path, lvideo);
    
    lfile.type = NULL;
    
    lfile.name = new char [strlen(name) + 1];
    strcpy(lfile.name, name);
    
    // Right
    rfile.path = new char [strlen(rvideo) + 1];
    strcpy(rfile.path, rvideo);
    
    rfile.type = NULL;
    
    rfile.name = new char [strlen(name) + 1];
    strcpy(rfile.name, name);
    
    
    stMgr.addFiles(lfile, rfile, std::string(name));
    return true;
}

/*!
 * \brief queryNewSources Checks if new sources are available
 * 
 * This function checks if new sources are avaiable.
 */
bool ST::queryNewSources()
{
    if (!stMgr.sourcesAvailable()) {
        return false;
    } else {
        return stMgr.queryNewSources();
    }
}

/*!
 * \brief getSources Returns all sources available
 * 
 * This function checks if new sources are avaiable
 */
int ST::getSources(Source_t ** data)
{
    if (!stMgr.sourcesAvailable())
    {
        *data = (Source_t*) malloc(sizeof(Source_t));
        (*data)[0].type = ST_NONE;
        (*data)[0].index = 0;
        std::string nonename = std::string("No Sources");
        (*data)[0].name = strdup(nonename.c_str());
        return 1;
    }
    
    std::vector<std::string> files;
    std::vector<std::string> eyes;
    stMgr.getSources(files, eyes);
    
    unsigned int tot_sources = eyes.size() + files.size();
    
    if (tot_sources == 0)
        return 0;
    
    *data = (Source_t*) malloc(tot_sources*sizeof(Source_t));
    
    int count = 0;
    for (unsigned int i=0; i<eyes.size(); i++)
    {
        (*data)[count].type = ST_EYES;
        (*data)[count].index = i;
        (*data)[count].name = strdup(eyes[i].c_str());
        count++;
    }
    for (unsigned int i=0; i<files.size(); i++)
    {
        (*data)[count].type = ST_FILES;
        (*data)[count].index = i;
        (*data)[count].name = strdup(files[i].c_str());
        count++;
    }
    return tot_sources;
}

/*!
 * \brief setSource 
 * \param type either ST_FILES or ST_EYES
 * \param index (referedd to the type!)
 * \return true if the statusManager succeds
 * 
 * This function selects a source. 
 */
bool ST::setSource(sourceType type, unsigned int index)
{
    return stMgr.selectSource(type, index);
}

bool ST::parse_config(const char * filename)
{
    return stMgr.parseSettings(filename);
}

bool ST::camConnect(bool closeConn)
{
    bool ret;
    
    std::string host_nameL, addressL;
    std::string host_nameR, addressR;
    int portL=0, portR=0;
    
    ret = stMgr.getLCtrlConn(host_nameL, addressL, portL);
    if (!ret)
        return false;
        
    // Now R eye
    ret = stMgr.getRCtrlConn(host_nameR, addressR, portR);
    if (!ret)
        return false;          
    
    ret = cmdSender[LEYE].conn(host_nameL.c_str(), addressL.c_str(), portL);
    if (!ret)
        return false;
  
    ret = cmdSender[REYE].conn(host_nameR.c_str(), addressR.c_str(), portR);
    if (!ret)
    {
        cmdSender[LEYE].clse();
        return false;
    }
    
    if (closeConn)
    {
        return (cmdSender[LEYE].clse() && cmdSender[REYE].clse());
    }
    return true;
    
}

bool ST::cam_adjust_property(CmdTypes what, int value, bool increment)
{
    L_(ldebug4) << "Setting camera property " << what << " to " << value << (increment ? " increment" : "");
    
    command_t cmd;
    cmd.cmd = what;
    cmd.value = (int32_t) value;
    cmd.increment = increment;
    
    return sendCmdToCameras(cmd);
}
   

bool ST::cam_save_settings()
{
    L_(ldebug4) << "Saving camera settings";
    command_t cmd;
    cmd.cmd = SAVE_SETTINGS;
    cmd.value = (int32_t) 0; //unused
    cmd.increment = false;   //unsused
    
    return sendCmdToCameras(cmd);    
}


bool ST::sendCmdToCameras(command_t cmd)
{
    bool ret;

    ret = camConnect(false); // do not close connection
    
    // connection is automatically closed on failure so no need to close!
    if(!ret)
    {
        L_(ldebug4) << "Cannot connect to cameras";
        return false;
    }    
    
    ret = cmdSender[LEYE].send_command(cmd);
    // if left eye was not succesful give up!
    if(!ret)
    {
        L_(ldebug4) << "Command failed";
        cmdSender[LEYE].clse();
        cmdSender[REYE].clse();
        return false;
    }
    
    ret = cmdSender[REYE].send_command(cmd);
    
    // connection can be closed now
    cmdSender[LEYE].clse();
    cmdSender[REYE].clse();
    return ret;
}


