#include "Utilities.hpp"
#include "lap.h"
#include "log.h"

#include "opencv2/calib3d/calib3d.hpp"

#define LARGE 6000.0  // this is required for LAPW

using namespace std;


cv::Point3f operator*(cv::Mat M, const cv::Point3f& p)
{ 
    cv::Mat_<double> src(3/*rows*/,1 /* cols */);

    src(0,0)=p.x;
    src(1,0)=p.y;
    src(2,0)=p.z;

    cv::Mat_<double> dst = M*src; //USE MATRIX ALGEBRA
    return cv::Point3f(dst(0,0),dst(1,0),dst(2,0));
}

cv::Point3f operator+(cv::Mat M, const cv::Point3f& p)
{ 
    cv::Mat_<double> src(3/*rows*/,1 /* cols */);

    src(0,0)=p.x;
    src(1,0)=p.y;
    src(2,0)=p.z;

    cv::Mat_<double> dst = M+src; //USE MATRIX ALGEBRA
    return cv::Point3f(dst(0,0),dst(1,0),dst(2,0));
}

int CountNanPoints(const vector<Point3f>& points3d)
{
    int nNan(0);
    for(auto aPoint : points3d)
        if(isnan(aPoint.x))
            nNan++;
    return nNan;
}

void Vec2Array(const vector<vector<double> > &vals, int N, cost** out)
{
    
    for(int i=0; (i < N); i++)
    {
        out[i] = new double[N];
        for(int j=0; (j < N); j++)
        {
            if (i < (int) vals.size() && j < (int) vals[i].size())
            {
                out[i][j] =vals[i][j];
            } else {
                out[i][j] = LARGE;
            }
        } 
    }
}

bool ReArrangePoints3dAndFillWithNans(const vector<Point3f> & points3dPreviousFrame, const vector<Point3f>& points3d, vector<Point3f> * outReararngedPoints, vector<int> *newIndices, const double distanceThreshMeters)
{
    //se punti hanno una dist di pi`u di quest athresh non vengono matchati comunque
    //double distanceThreshMeters = 0.05;
    // points3dPreviousFrame ha sempre nMarkers points e alcuni sono nan.
    //ponts3d ha <=nMarkers points ma tutti calcolati

    int nMarkers = points3dPreviousFrame.size();
    vector<Point3f> reorderedPoints(nMarkers, Point3f(nanf(""), nanf(""), nanf("")));
    vector<Point3f> nonMatchedPoints;
    int nNanPrevious = CountNanPoints(points3dPreviousFrame);
    int nNanThis = nMarkers - points3d.size();
    newIndices->clear();
    newIndices->resize(nMarkers,-1);
    if(nNanThis >= nNanPrevious)
    {
        for(int i(0); i< (int) points3d.size(); ++i)
        {
            double minDist = std::numeric_limits<double>::infinity();
            int minIdx(-1);
            for(int iPrev(0); iPrev < (int)points3dPreviousFrame.size(); ++iPrev)
            {
                if(isnan(points3dPreviousFrame[iPrev].x))
                    continue;

                double dist = cv::norm(points3d[i]- points3dPreviousFrame[iPrev]);
                if(dist < minDist && dist < distanceThreshMeters)
                {
                    minDist = dist;
                    minIdx = iPrev;
                }
            }
            if(minIdx != -1)
            {
                reorderedPoints[minIdx] = points3d[i];
                (*newIndices)[minIdx] = i;
            }
            else
                nonMatchedPoints.push_back(points3d[i]);
        }
    }
    else
    {
        std::vector<int> nonAssignedPoints(points3d.size());
        std::iota(nonAssignedPoints.begin(), nonAssignedPoints.end(), 0);

        for(int iPrev(0); iPrev < (int)points3dPreviousFrame.size(); ++iPrev)
        {

            if(isnan(points3dPreviousFrame[iPrev].x))
                continue;
            double minDist = std::numeric_limits<double>::infinity();
            int minIdx(-1);
            for(int i(0); i < (int)points3d.size(); ++i)
            {
                double dist = cv::norm(points3dPreviousFrame[iPrev]- points3d[i]);
                if(dist < minDist && dist < distanceThreshMeters)
                {
                    minDist = dist;
                    minIdx = i;
                }
            }
            if(minIdx != -1)
            {
                reorderedPoints[iPrev] = points3d[minIdx];
                (*newIndices)[iPrev] = minIdx;
                std::vector<int>::iterator position = std::find(nonAssignedPoints.begin(), nonAssignedPoints.end(), minIdx);
                if (position != nonAssignedPoints.end())
                    nonAssignedPoints.erase(position);
            }
        }
        for (auto nonAssIdx : nonAssignedPoints)
            nonMatchedPoints.push_back(points3d[nonAssIdx]);
    }


    // now we should have reordered points and non matched points
    //    vector<Point3f> reorderedPoints(nMarkers, Point3f(nanf(""), nanf(""), nanf("")));
    //    vector<Point3f> nonMatchedPoints;

    // sort non matched points per x crescenti
    vector <pair< Point3f, float> > temp;
    for(int i(0); i< (int)nonMatchedPoints.size(); ++ i)
        temp.push_back(std::pair<Point3f, float>(nonMatchedPoints[i],nonMatchedPoints[i].x));
    std::sort(temp.begin(), temp.end(), PairPoint3fCompare);
    for(int i(0); i< (int)nonMatchedPoints.size(); ++ i)
        nonMatchedPoints[i] = temp[i].first;

    int nNanreordered = CountNanPoints(reorderedPoints);
    int iNonMatch(0);
    for(int i(0); i < (int)reorderedPoints.size(); ++i)
    {
        if (iNonMatch >= (int)nonMatchedPoints.size())
            break;
        if(isnan(reorderedPoints[i].x))
        {
            reorderedPoints[i] = nonMatchedPoints[iNonMatch];
            ++iNonMatch;
        }
    }

    outReararngedPoints->clear();
    (*outReararngedPoints) = reorderedPoints;
    // nota che se ne manca solo uno e non siamo riusciti a matcharlo allora deve essere quello che manca --> ritorno true
    if(nNanreordered ==0 || nonMatchedPoints.size() ==0 || (nNanreordered == 1 && nonMatchedPoints.size() ==1)) // everything went well
        return true;
    else
        return false;
}

int FindBestMatchesAndConvertToMat(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, Mat* pixels2dL, Mat*pixels2dR)
{

    vector<Vec2d> ppL, ppR;
    const int nCorresp = FindBestMatchesLAPJV(errorsRepr, localMaximaL, localMaximaR, ppL, ppR);
    if(pixels2dL->data)
        pixels2dL->release();
    if(pixels2dR->data)
        pixels2dR->release();

    if(nCorresp == 0)
        return nCorresp;
    (*pixels2dL) = Mat::zeros(1,nCorresp,CV_64FC2);
    (*pixels2dR) = Mat::zeros(1,nCorresp,CV_64FC2);
    for(int i(0); i<(int)ppL.size(); ++i)
    {
        pixels2dL->at<Vec2d>(0,i) = ppL[i];
        pixels2dR->at<Vec2d>(0,i) = ppR[i];
    }
    return nCorresp;
}

int FindBestMatchesLAPJV(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, vector<Vec2d> & ppL, vector<Vec2d> & ppR)
{

    int N = std::max(errorsRepr.size(),errorsRepr[0].size());
    int n = 0; 
    cost costThresh = 20.0;
    
    cost **costs, *u, *v, lapcost;
    row *colsol;
    col *rowsol;
    rowsol = new col[N];
    colsol = new row[N];
    u = new cost[N];
    v = new cost[N];    
    costs = new double*[N];
    
    // cout << "repro error: " <<endl;
    // for(int i(0); i< errorsRepr.size(); ++i)
    // {
    //     for(int j(0); j< errorsRepr[i].size(); ++j)
    //         cout << errorsRepr[i][j] << "  " ;
    //     cout << endl;
    // }
    
    Vec2Array(errorsRepr,N, costs);

    // cout << "cost: " <<endl;
    // for(int i(0); i< N; ++i)
    // {
    //     for(int j(0); j< N; ++j)
    //         cout << costs[i][j] << "  " ;
    //     cout << endl;
    // }
   
    lapcost = lap(N, costs, rowsol, colsol, u, v);

    //cout << "rowsol ";
    //for (int iCand(0); iCand < N; ++ iCand)
    //    cout << " " <<rowsol[iCand];
    //cout << "colsol ";
    //for (int iCand(0); iCand < N; ++ iCand)
    //    cout << " " <<colsol[iCand];
    //cout <<endl;cout <<endl;
    //
    //cout << "lapcost " << lapcost << " u";
    //for (int iCand(0); iCand < N; ++ iCand)
    //    cout << " " <<u[iCand];
    //cout << "  v";
    //for (int iCand(0); iCand < N; ++ iCand)
    //    cout << " " <<v[iCand];        
    //cout <<endl;

    //if (lapcost > costThresh)
    //    N=0;
        
    for(int iCand(0); iCand < N; ++ iCand)
    {
        if(u[iCand]+v[rowsol[iCand]] < costThresh )
        {
            n++;
            ppL.push_back(localMaximaL[iCand].first);
            ppR.push_back(localMaximaR[rowsol[iCand]].first);
        }
    }
    
    delete [] rowsol;
    delete [] colsol;
    delete [] u;
    delete [] v;    
    for (int i = 0; i < N; i++)
          delete [] costs[i];
    delete [] costs;
    L_(ldebug4) << "Found " << n << " markers!" <<endl;
    return n;
}


int FindBestMatches(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, vector<Vec2d> & ppL, vector<Vec2d> & ppR)
{

    float errorThreshPixels = 20;

    /// /////////////////////////////////////////////
    /// /////////////////////////////////////////////
    ////////////////////////////// debug ////////////////////////
    //    cout << endl<<endl<<endl<<" ok gionni, cominciamo"<<endl;
    //    cout << " num markers trovati nell'immagine a sin: "<<localMaximaL.size()<<endl;
    //    cout << " num markers trovati nell'immagine a dx: "<<localMaximaR.size()<<endl;
    //    cout << " Se non ho trovato un cazzo in almeno una delle 2 immagini, meglio aggiustare threshold in chiamata a   GetLocalMaxima(blurredImageR, searchArea, 150, 0,&localMaximaR); in stereoTriangulation ( mettere, p. es, 100 al posto di 150)"<<endl;
    //
    //    cout << " taglia repro error : "<<errorsRepr.size()<< " , dovrebbe essere nMarkers x nMarkers. Se no albe è un babbo."<<endl;
    //    cout << "repro error: " <<endl;
    //    for(int i(0); i< errorsRepr.size(); ++i)
    //    {
    //        for(int j(0); j< errorsRepr[i].size(); ++j)
    //            cout << errorsRepr[i][j] << "  " ;
    //        cout << endl;
    //    }
    //    cout << " Per ogni riga/colonna, ci dovrebbe essere almeno un elemento <= di errorThreshPixels, se no albe è un babbo."<<endl;
    //    cout << " PS la threshold è "<< errorThreshPixels<<endl;
    ////cout << "premi un tasto per il prossimo frame!"<<endl;
    ////char a;
    ////cin >> a;
    /// /////////////////////////////////////////////
    /// /////////////////////////////////////////////
    /// /////////////////////////////////////////////

    vector< vector<int> > bestForLeft(localMaximaL.size());
    for (int iLeft = 0; iLeft < (int)localMaximaL.size(); iLeft++ )
    {
        for(int iRight(0); iRight<(int)localMaximaR.size(); ++iRight)
        {
            if(errorsRepr[iLeft][iRight] < errorThreshPixels && localMaximaL[iLeft].first[0] >= localMaximaR[iRight].first[0]) //i punti sulla foto di dx  devono trovarsi a sin !
                bestForLeft[iLeft].push_back(iRight);
        }
    }


    vector< vector<int> > bestForRight(localMaximaR.size());
    for(int iRight(0); iRight<(int)localMaximaR.size(); ++iRight)
    {
        for (int iLeft = 0; iLeft < (int)localMaximaL.size(); iLeft++ )
        {
            if(errorsRepr[iLeft][iRight] < errorThreshPixels && localMaximaL[iLeft].first[0] >= localMaximaR[iRight].first[0]) //i punti sulla foto di dx  devono trovarsi a sin !
                bestForRight[iRight].push_back(iLeft);
        }
    }
    for(int i(0); i< (int)bestForLeft.size(); ++i)
    {
        if(bestForLeft[i].empty())
            continue;
        else if (bestForLeft[i].size() ==1)
        {
            int bLeft = bestForLeft[i][0];
            if(bestForRight[bLeft].empty())
                continue;
            else
            {
                for(int iRight(0); iRight < (int)bestForRight[bLeft].size(); ++iRight)
                    if(bestForRight[bLeft][iRight] == i)
                    {
                        ppL.push_back(localMaximaL[i].first);
                        ppR.push_back(localMaximaR[bLeft].first);
                    }
            }
        }
        else //bestForLeft[i] ha più candidati
        {
            //      cout<< "la boucle de la muerte !"<<endl;
            // check se anche i candidati destri hanno gli stessi candidati (se no c'è qualcosa di strano e non facciamo niente)
            int nPointsOnSameLine = bestForLeft[i].size();
            bool bAllPointsHaveSameCandidates = true;
            for(int iCand(0); iCand< (int)bestForLeft[i].size(); ++iCand)
            {
                int bLeft = bestForLeft[i][iCand];
                if(bestForRight[bLeft] != bestForRight[bestForLeft[i][0]]) // tutti i best dei candidati destri devono essere uguali!
                {
                    bAllPointsHaveSameCandidates = false;
                    break;
                }
            }
            // check se tutti i cand sinistri hanno gli stessi candidati (se no c'è qualcosa di strano e non facciamo niente)

            if(bAllPointsHaveSameCandidates)
            {
                for(int iSecondLeft(0); iSecondLeft < (int)bestForLeft.size(); ++iSecondLeft)
                {

                    if(bestForLeft[iSecondLeft] != bestForLeft[i])
                    {
                        //                        cout << " yopx"<<endl;
                        //                        cout << iSecondLeft<<endl;
                        //                        for(int ii(0); ii< bestForLeft[iSecondLeft].size(); ++ii)
                        //                            cout<<bestForLeft[iSecondLeft][ii]<<"  ";
                        //                        cout <<endl;
                        //                        cout << i<<endl;
                        //                        for(int ii(0); ii< bestForLeft[i].size(); ++ii)
                        //                            cout<<bestForLeft[i][ii]<<"  ";

                        bAllPointsHaveSameCandidates = false;
                        break;
                    }
                }
            }
            if(bAllPointsHaveSameCandidates) // all the left-right candidates should have the same size and contain the same indices,
            {
                vector<int> idxLeft = bestForLeft[i];
                vector<int> idxRight = bestForRight[bestForLeft[i][0]];

                vector <pair< Vec2d, float> > candL, candR;
                for(int iCand(0); iCand < nPointsOnSameLine; ++ iCand)
                {
                    candL.push_back(std::pair< Vec2d, float>(localMaximaL[idxLeft[iCand]].first,localMaximaL[idxLeft[iCand]].first[0]));
                    candR.push_back(std::pair< Vec2d, float>(localMaximaR[idxRight[iCand]].first,localMaximaR[idxRight[iCand]].first[0]));
                }
                std::sort(candL.begin(), candL.end(), PairCompare);
                std::sort(candR.begin(), candR.end(), PairCompare);
                ///////////////////
                //                cout << " local maxima L: "<<endl;
                //                for(int iCand(0); iCand < localMaximaL.size(); ++ iCand)
                //                    cout << localMaximaL[iCand].first << " " << localMaximaL[iCand].second<<endl;
                //                cout << " local maxima R: "<<endl;
                //                for(int iCand(0); iCand < localMaximaR.size(); ++ iCand)
                //                    cout << localMaximaR[iCand].first << " " << localMaximaR[iCand].second<<endl;
                //                cout << " cand L : "<<endl;
                //                for(int iCand(0); iCand < nPointsOnSameLine; ++ iCand)
                //                    cout << candL[iCand].first << " " << candL[iCand].second<<endl;
                //                cout << " cand R: "<<endl;
                //                for(int iCand(0); iCand < nPointsOnSameLine; ++ iCand)
                //                    cout << candR[iCand].first << " " << candR[iCand].second<<endl;
                //////////////////

                for(int iCand(0); iCand < nPointsOnSameLine; ++ iCand)
                {
                    ppL.push_back(candL[iCand].first);
                    ppR.push_back(candR[iCand].first);
                }
            }
        }
    }
    return (int)ppL.size();
}

int FindBestMatchesInHistory(const int frameNumber, vector <pair< Vec2d, int> >& localMaximaLBefore, vector <pair< Vec2d, int> > &localMaximaRBefore, const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, Mat* pixels2dL, Mat* pixels2dR, const float distThreshold, const int memorySpan)
{
    
    // Memory fades...
    if ( ! localMaximaLBefore.empty())
    {
        assert(localMaximaLBefore.size()==localMaximaRBefore.size());
        vector< pair< Vec2d, int> >::iterator iL = localMaximaLBefore.begin();
        vector< pair< Vec2d, int>  >::iterator iR = localMaximaRBefore.begin();
        while(iL != localMaximaLBefore.end() || iR != localMaximaRBefore.end()) {
            pair< Vec2d, int> pointL = (*iL);
            pair< Vec2d, int> pointR = (*iR);
            if(pointL.second < (frameNumber-memorySpan) || pointR.second < (frameNumber-memorySpan)) {

                iL = localMaximaLBefore.erase(iL);
                iR = localMaximaRBefore.erase(iR);
            }
            else
            { ++iL; ++iR;
            }
        }
    }

    
    // localMaximaLBefore contiene la posizione e un intero che indica l'ultimo frame in cui e' stati trovao
    int nCorresp=0;
    bool needReboot = false;
    
    vector<Vec2d> ppL, ppR;
    
    // Se ci sono troppi punti e' meglio non permutare!
    if (localMaximaL.size() > 6 || localMaximaR.size() > 6)
    {
        L_(ldebug4) << "Too many points for permutation at frame "<<frameNumber<<endl;
        
        // resetto la memoria
        localMaximaLBefore.clear();
        localMaximaRBefore.clear();
        //return nCorresp;
    }
    
    // Non ci sono punti salvati, salviamoli!
    //  (se una dimensione e' 0 lo e' anche ;'altra!)
    
    if ( localMaximaLBefore.empty() || localMaximaRBefore.empty())
    {
        L_(ldebug4) << "NEED BOOTSTRAP...";
        needReboot = true;
        
    } else
    {
        
        // We have the same or less points
        
        if ( needReboot==false && (int)localMaximaL.size() <= (int)localMaximaLBefore.size()
                  && (int)localMaximaR.size() <= (int)localMaximaRBefore.size())
        {
            // ci sono tanti o meno massimi rispetto a quelli salvati
            
            int nPointsBefore=(int)localMaximaLBefore.size();
            
            assert(nPointsBefore==(int)localMaximaRBefore.size());
            
            vector<int> indexes;      // contiene gli indici per fare la permutazione
            vector<int> bestindexesL((int)localMaximaL.size());  // contiene gli indici che hanno la distanza minore
            vector<int> bestindexesR((int)localMaximaR.size());  // contiene gli indici che hanno la distanza minore
            
            
            // prepara indici per fare le permutazioni
            for(int i=0;i<nPointsBefore;++i)
            {
                indexes.push_back(i);
            }
            
            // fa i controlli con le permutazioni
            double CurDistanceL=1000000000000;
            double thisDistanceL=0;

            do
            {
                
                thisDistanceL=0;
                for (int i = 0; i < (int)localMaximaL.size(); i++ )
                {
                    
                    //prende localMaximaL[iLeft] e lo confronta con la permutazione corrente che e data da indexes[iLeft]
                    thisDistanceL += sqrt(
                                pow(localMaximaL[i].first[0]-localMaximaLBefore[indexes[i]].first[0],2)
                            +
                            pow(localMaximaL[i].first[1]-localMaximaLBefore[indexes[i]].first[1],2)
                            );
                }
                if (thisDistanceL < CurDistanceL)
                {
                    for(int j = 0; j < (int)localMaximaL.size(); j++ )
                        bestindexesL[j]=indexes[j];
                    CurDistanceL=thisDistanceL;
                }

            } while(next_permutation(indexes.begin(),indexes.end()));

            // ====================
            //  STESSA ROBA PER LA DX
            
            // fa i controlli con le permutazioni
            double CurDistanceR=1000000000000;
            double thisDistanceR=0;


            do
            {
                
                thisDistanceR=0;
                for (int i = 0; i < (int)localMaximaR.size(); i++ )
                {
                    
                    //prende localMaximaR[iReft] e lo confronta con la permutazione corrente che e data da indexes[iReft]
                    thisDistanceR += sqrt(
                                pow(localMaximaR[i].first[0]-localMaximaRBefore[indexes[i]].first[0],2)
                            +
                            pow(localMaximaR[i].first[1]-localMaximaRBefore[indexes[i]].first[1],2)
                            );
                }
                if (thisDistanceR < CurDistanceR)
                {
                    for(int j = 0; j < (int)localMaximaR.size(); j++ )
                        bestindexesR[j]=indexes[j];
                    CurDistanceR=thisDistanceR;
                }

            } while(next_permutation(indexes.begin(),indexes.end()));

            
            if (CurDistanceL < distThreshold && CurDistanceR < distThreshold )
            {
                nCorresp = std::min<int>((int)localMaximaR.size(), (int)localMaximaL.size());
                
                // cerco il punto c-esimo nella storia
                for (int c = 0; c < nPointsBefore; c++ )
                {
                    int foundL=-1;
                    int foundR=-1;
                    
                    for (int i = 0; i < (int)bestindexesL.size(); i++ )
                    {
                        if (c==bestindexesL[i])
                        {
                            foundL=i;
                            break;
                        }
                    }
                    for (int i = 0; i < (int)bestindexesR.size(); i++ )
                    {
                        if (c==bestindexesR[i])
                        {
                            foundR=i;
                            break;
                        }
                    }
                    if (foundL>= 0 && foundR >=0)
                    {
                        ppL.push_back(localMaximaL[foundL].first);
                        ppR.push_back(localMaximaR[foundR].first);
                        localMaximaLBefore[c].first = localMaximaL[foundL].first;
                        localMaximaLBefore[c].second = frameNumber;
                        localMaximaRBefore[c].first = localMaximaR[foundR].first;
                        localMaximaRBefore[c].second = frameNumber;
                    }
                }


            } else { // thresholds aer too large
                // I'm lost, REBOOT
                L_(ldebug4) << "RE...";
                needReboot=true;
            }
            
        } else {
            // I'm lost, REBOOT
            L_(ldebug4) << "RE...";
            needReboot=true;
        }
    } // fine controllo se ci sono o meno punti salvati

    if (needReboot)
    {
        L_(ldebug4) << "BOOTING" <<endl;
        nCorresp=FindBestMatchesLAPJV(errorsRepr, localMaximaL, localMaximaR, ppL, ppR);
        
        // resetto i vecchi dati
        localMaximaLBefore.clear();
        localMaximaRBefore.clear();
        for(int iCand(0); iCand < nCorresp; ++ iCand)
        {
            localMaximaLBefore.push_back(std::pair< Vec2d, int>(ppL[iCand], frameNumber));
            localMaximaRBefore.push_back(std::pair< Vec2d, int>(ppR[iCand], frameNumber));
        }

    }


    
    if(pixels2dL->data)
        pixels2dL->release();
    if(pixels2dR->data)
        pixels2dR->release();
    
    if(nCorresp == 0)
        return nCorresp;
    
    (*pixels2dL) = Mat::zeros(1,nCorresp,CV_64FC2);
    (*pixels2dR) = Mat::zeros(1,nCorresp,CV_64FC2);
    for(int i(0); i<(int)ppL.size(); ++i)
    {
        pixels2dL->at<Vec2d>(0,i) = ppL[i];
        pixels2dR->at<Vec2d>(0,i) = ppR[i];
    }
    return nCorresp;
    
    
}

//void UpdateHistory(const vector <pair< int, int> >& PointsMatches, const vector <pair< Vec2d, float> >& localMaximaLBefore, const vector <pair< Vec2d, float> > &localMaximaRBefore, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR)
//{
//    
//    if
//    for(int iRight(0); iRight<(int)localMaximaR.size(); ++iRight)
//    {
//
//    }  
//    
//    for (int iLeft = 0; iLeft < (int)localMaximaL.size(); iLeft++ )
//    {
//        localMaximaL[iLeft].first[0] 
//        localMaximaR[iRight].first[0]
//    }
//      
//}

int FindBestMatchesNaive(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, Mat* pixels2dL, Mat*pixels2dR)
{
    vector<int> bestForLeft(localMaximaL.size());
    for (int iLeft = 0; iLeft < (int)localMaximaL.size(); iLeft++ )
    {
        double minErr = 1e10;
        for(int iRight(0); iRight<(int)localMaximaR.size(); ++iRight)
        {
            if(errorsRepr[iLeft][iRight] < minErr)
            {
                minErr = errorsRepr[iLeft][iRight];
                bestForLeft[iLeft] = iRight;
            }
        }
    }

    vector<int> bestForRight(localMaximaR.size());
    for(int iRight(0); iRight<(int)localMaximaR.size(); ++iRight)
    {
        double minErr = 1e10;
        for (int iLeft = 0; iLeft < (int)localMaximaL.size(); iLeft++ )
        {
            if(errorsRepr[iLeft][iRight] < minErr)
            {
                minErr = errorsRepr[iLeft][iRight];
                bestForRight[iRight] = iLeft;
            }
        }
    }

    int nCandidates = std::min<int>((int)bestForLeft.size(), (int)bestForRight.size());
    vector<Vec2d> ppL, ppR;
    for(int i(0); i< nCandidates; ++i)
    {
        int bLeft = bestForLeft[i];
        int bRight = bestForRight[bLeft];
        if(bRight == i)
        {
            ppL.push_back(localMaximaL[i].first);
            ppR.push_back(localMaximaR[bLeft].first);
        }
    }

    const int nCorresp = ppL.size();
    if(pixels2dL->data)
        pixels2dL->release();
    if(pixels2dR->data)
        pixels2dR->release();

    if(nCorresp == 0)
        return nCorresp;

    (*pixels2dL) = Mat::zeros(1,nCorresp,CV_64FC2);
    (*pixels2dR) = Mat::zeros(1,nCorresp,CV_64FC2);
    for(int i(0); i<(int)ppL.size(); ++i)
    {
        pixels2dL->at<Vec2d>(0,i) = ppL[i];
        pixels2dR->at<Vec2d>(0,i) = ppR[i];
    }
    return nCorresp;
}


bool PairPoint3fCompare(const std::pair<cv::Point3f, float>& firstElem, const std::pair<cv::Point3f, float>& secondElem)
{
    return firstElem.second < secondElem.second;
}

bool PairCompare(const std::pair<cv::Vec2d, float>& firstElem, const std::pair<cv::Vec2d, float>& secondElem)
{
    return firstElem.second < secondElem.second;
}


//std::vector<std::pair<std::string, bool> > v;
//std::sort(v.begin(), v.end());

// TODO : re-write this function better faster stronger
void GetLocalMaxima(const cv::Mat ProcessImg,int MatchingSize, int Threshold, int GaussKernel, vector <pair< cv::Vec2d, float > >* vMaxLoc)
{
    int nMaxMaxima = 50;
    vMaxLoc->clear();

    if ((MatchingSize % 2 == 0))
        throw runtime_error("matching size must be odd !!! ");


    vMaxLoc->reserve(nMaxMaxima); // Reserve place for fast access
    //Mat ProcessImg = Src;
    int W = ProcessImg.cols;
    int H = ProcessImg.rows;
    int SearchWidth  = W - MatchingSize;
    int SearchHeight = H - MatchingSize;
    int MatchingSquareCenter = MatchingSize/2;

    if(GaussKernel > 1) // If You need a smoothing
    {
        if ((GaussKernel % 2 == 0))
            throw std::runtime_error("gaussian kernel size must be odd!");
        GaussianBlur(ProcessImg,ProcessImg,Size(GaussKernel,GaussKernel),0,0,4);
    }
    uchar* pProcess = (uchar *) ProcessImg.data; // The pointer to image Data

     int Shift = MatchingSquareCenter * ( W + 1);
     int k = 0;

    for(int y=0; y < SearchHeight; ++y)
    {
        int m = k + Shift;

        for(int x=0;x < SearchWidth ; ++x)
        {
            if (pProcess[m++] >= Threshold)
            {
                 Point LocMax;
                double minValue(1e6), maxValue(0);
                Mat mROI(ProcessImg, Rect(x,y,MatchingSize,MatchingSize));
                minMaxLoc(mROI,&minValue, &maxValue,NULL,&LocMax);

                if (LocMax.x == MatchingSquareCenter && LocMax.y == MatchingSquareCenter && maxValue>Threshold) // in teoria ultimo test inutile
                {
                    //					vMaxLoc.push_back(Point(x+LocMax.x,y + LocMax.y ));
                    pair < cv:: Vec2d, float > tempPair(Vec2d(x+LocMax.x,y + LocMax.y ), maxValue);
                    vMaxLoc->push_back(tempPair);
                    if((int)vMaxLoc->size() >= nMaxMaxima)
                        return;
                }
            }

        }
        k += W;
    }
}



void ExploseImageChannels(Mat &frame, Mat &frameChannels)
{
    Mat explosedFrame1;
    vector<Mat> channels(3);
    for(int i(0); i< 3; ++i)
        channels[i] = Mat::zeros(frame.rows, frame.cols, CV_8UC1);
    split(frame, &channels[0]);

    hconcat(channels[0], channels[1], explosedFrame1);
    hconcat(explosedFrame1, channels[2], frameChannels);
}





void FastGetLocalMaxima(const cv::Mat ProcessImg,int MatchingSize, int Threshold, int GaussKernel, int nMarkers, vector <pair< cv::Vec2d, float > >* vMaxLoc)
{
///////////////////
//temp checks !
    if(ProcessImg.type() != CV_8UC1)
        throw runtime_error("wtf image is wrong size ");
    if (nMarkers ==0)
        throw runtime_error("n markers is zer o :( ");
    if (!ProcessImg.data)
        throw runtime_error("empty image :( ");
    if (!MatchingSize)
        throw runtime_error("matching size is empty:( ");
    if (GaussKernel==0)
        throw runtime_error("gauss kernel  is zer o :( ");
//////////////////

    if ((MatchingSize % 2 == 0))
        throw runtime_error("matching size must be odd !!! ");
    int W = ProcessImg.cols;
    int H = ProcessImg.rows;

    if(GaussKernel > 1) // If You need a smoothing
    {
        if ((GaussKernel % 2 == 0))
            throw std::runtime_error("gaussian kernel size must be odd!");
        GaussianBlur(ProcessImg,ProcessImg,Size(GaussKernel,GaussKernel),0,0,4);
    }

    //1 . find centers of patches where to find
    // se ho trovato tutti i markers al previous frame cerco nuovi massimi li intorno,
    //altrimenti cerco in immagine downscalata !
    vector <pair< cv::Vec2d, float > > centersToLookAround;
    if (vMaxLoc->size() == nMarkers)
        centersToLookAround = *vMaxLoc;
    else
    {
        Mat smallImage;
        resize(ProcessImg, smallImage, Size(320, 240));
        GetLocalMaxima(smallImage, MatchingSize*320/W, Threshold, 0, &centersToLookAround);
        float scalex = W/smallImage.cols;
        float scaley = H/smallImage.rows;
        for(int i(0); i< (int)centersToLookAround.size(); ++i)
        {
            centersToLookAround[i].first[0] *= scalex;
            centersToLookAround[i].first[1] *= scaley;
        }
    }

    //2 . reassign local maxima refined
    vMaxLoc->clear();
    vMaxLoc->reserve(nMarkers);
    for(int i(0); i< (int)centersToLookAround.size(); ++i)
    {
        int centerx = centersToLookAround[i].first[0];
        int centery = centersToLookAround[i].first[1];
        int startx = (centerx-MatchingSize/2) >0 ? (centerx-MatchingSize/2) : 0;
        int starty = (centery-MatchingSize/2) >0 ? (centery-MatchingSize/2) : 0;;

        int widthROI = (startx+MatchingSize)< ProcessImg.cols ? MatchingSize : W -startx-1;
        int heightROI = (starty+MatchingSize)< ProcessImg.rows? MatchingSize : H - starty-1;

        Point LocMax;
        double minValue(1e6), maxValue(0);
        Mat mROI(ProcessImg, Rect(startx, starty, widthROI, heightROI));
        minMaxLoc(mROI,&minValue, &maxValue,NULL,&LocMax);
        if (maxValue >= Threshold)
        {
            pair < cv:: Vec2d, float > tempPair(Vec2d(startx+LocMax.x, starty + LocMax.y ), maxValue);
            vMaxLoc->push_back(tempPair);
            ///////////////////////////////////////////////
            // TODO temp check !!!!! REMOVE ME
            for (auto loc: (*vMaxLoc))
            {
                if(loc.first[0] < 0 || loc.first[0] >= W || loc.first[1] < 0 || loc.first[1] >= H || loc.second < Threshold || loc.second > 255 )
                    throw std::runtime_error("orco cazzo c'ho i dati scazzati ");
            }
            ////////////////////////////////////////////
            if((int)vMaxLoc->size() >= nMarkers)
                return;

        }
    }
}
