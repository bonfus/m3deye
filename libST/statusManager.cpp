#include "statusManager.hpp"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"



#include <vector>
#include <utility>
#include <string>

using namespace cv;
using namespace std;


bool operator==(const EyeData_t& lhs, const EyeData_t& rhs)
{
    if (lhs.name!=NULL && rhs.name !=NULL)
    {
        if (strcmp(lhs.name,rhs.name)!=0)
            return false;
    } else {
        if (lhs.name!=NULL || rhs.name !=NULL)
            return false;
    }

    if (lhs.type!=NULL && rhs.type !=NULL)
    {
        if (strcmp(lhs.type,rhs.type)!=0)
            return false;
    } else {
        if (lhs.type!=NULL || rhs.type !=NULL)
            return false;
    }    

    if (lhs.domain!=NULL && rhs.domain !=NULL)
    {
        if (strcmp(lhs.domain,rhs.domain)!=0)
            return false;
    } else {
        if (lhs.domain!=NULL || rhs.domain !=NULL)
            return false;
    }    

    if (lhs.host_name!=NULL && rhs.host_name !=NULL)
    {
        if (strcmp(lhs.host_name,rhs.host_name)!=0)
            return false;
    } else {
        if (lhs.host_name!=NULL || rhs.host_name !=NULL)
            return false;
    }    

    if (lhs.address!=NULL && rhs.address !=NULL)
    {
        if (strcmp(lhs.address,rhs.address)!=0)
            return false;
    } else {
        if (lhs.address!=NULL || rhs.address !=NULL)
            return false;
    }    

    if (lhs.mountpoint!=NULL && rhs.mountpoint !=NULL)
    {
        if (strcmp(lhs.mountpoint,rhs.mountpoint)!=0)
            return false;
    } else {
        if (lhs.mountpoint!=NULL || rhs.mountpoint !=NULL)
            return false;
    }    
    
    if (lhs.port!=rhs.port)
        return false;
    
    return true;
}

StatusManager::StatusManager() : _newSources(false)
{
    L_(ldebug2) << WHERE << "StatusManager constructor.";
    _current_source = std::pair<sourceType,int>(ST_NONE,0);
    _eyes.empty();
    _files.empty();
    setDefaultSettings();
}
StatusManager::~StatusManager()
{
    L_(ldebug2) << WHERE << "StatusManager destructor.";
    freeSettings();
}

/*!
 * \brief parseSettings This method is used to set the default settings and then acquire settings 
 *        from a yml files. It uses openCV methods.
 * \return true if settings were correctly parsed. false otherwise.
 *
 * This method is used to parse settings file. This must be modified to 
 * get the settings path from the client!
 */
bool StatusManager::parseSettings(const char* filename)
{
    
    setDefaultSettings();
    
    try
    {
        string   fileName(filename);
        FileStorage fs2(fileName, FileStorage::READ);
    
    
        // string videoLeft, videoRight, tsFileL, tsFileR, intrinsicsInputFile, extrinsicsInputFile, csvFileName;
        string intrinsicsInputFile, extrinsicsInputFile;
        //        string referenceSystemTransformFile;
        
    
        if (!fs2.isOpened())
            throw runtime_error(string("Aborting : Failed to open: !")  + fileName);
    
        fs2["goodSync"]>> _config.syncTimes.goodSync;
        fs2["medSync"]>> _config.syncTimes.medSync;
        fs2["badSync"]>> _config.syncTimes.badSync;
        
        fs2["nMarkers"]>> _config.nMarkers;
        //fs2["memorySightThreshold"]>> _distThreshold;
        fs2["memoryTimeSpan"]>> _config.memorySpan;
        fs2["PointsRearrangeThreshold"]>> _config.PointsRearrangeDistanceThreshMeters;
        //        fs2["referenceSystemTransformFile"]>> referenceSystemTransformFile;
        
        
    
        L_(linfo) <<" Tracking no more than "<<_config.nMarkers << " markers on videos.";
    
        FileNode calibrations = fs2["calibrations"];
        FileNodeIterator it = calibrations.begin(), it_end = calibrations.end();
        int idx = 0;
        std::vector<uchar> lbpval;
    
        _config.nTriParams = calibrations.size();
        _config.TriParams = new TriParams_t[_config.nTriParams];
        
    
        Mat R, T, E, F; 
        for( ; it != it_end; ++it, idx++ )
        {
            L_(ldebug3) << WHERE  << " Parsing calibration #" << idx << ": "<< endl;
            _config.TriParams[idx].frameWidth = (int)(*it)["width"];
            _config.TriParams[idx].frameHeight = (int)(*it)["height"];
            
            
            FileNode ks = (*it)["kernelSize"];
            
            if (ks.empty())
                _config.TriParams[idx].kernelSize = 5;
            else
                _config.TriParams[idx].kernelSize = (int)ks;
            
            FileNode sa = (*it)["searchArea"];
            if (sa.empty())
                _config.TriParams[idx].searchArea = 31;
            else
                _config.TriParams[idx].searchArea = (int)sa;
            
            FileNode sthr = (*it)["memorySightThreshold"];
            if (sthr.empty())
                _config.TriParams[idx].distThreshold = 10.;
            else
                _config.TriParams[idx].distThreshold = (float) sthr;
    
    
            
            Mat * Camera_MatrixL = new cv::Mat;
            
            (*it)["Camera_MatrixL"] >> *Camera_MatrixL;
            _config.TriParams[idx].internalParametersMatrixL = (void*) Camera_MatrixL;
            
            Mat * distortionCoeffL = new cv::Mat;
            (*it)["Distortion_CoefficientsL"]>> * distortionCoeffL;
            _config.TriParams[idx].distortionCoeffL = (void*) distortionCoeffL;
            
            Mat * internalParametersMatrixR = new cv::Mat;
            (*it)["Camera_MatrixR"] >> *internalParametersMatrixR;
            _config.TriParams[idx].internalParametersMatrixR = (void*) internalParametersMatrixR;
            
            Mat * distortionCoeffR = new cv::Mat;
            (*it)["Distortion_CoefficientsR"]>> *distortionCoeffR;
            _config.TriParams[idx].distortionCoeffR = (void*) distortionCoeffR;
            
            
            Mat *projMatrixL = new Mat (Mat::zeros(3,4,CV_64F));
            (*projMatrixL).at<double>(0,0)=1.; 
            (*projMatrixL).at<double>(1,1)=1.;
            (*projMatrixL).at<double>(2,2)=1.;
            
            _config.TriParams[idx].projMatrixL = (void*) projMatrixL;
        
            Mat * R = new cv::Mat;
            Mat * T = new cv::Mat;
            Mat * E = new cv::Mat;
            Mat * F = new cv::Mat;
            (*it)["R"] >> *R;
            (*it)["T"] >> *T;
            (*it)["E"] >> *E;
            (*it)["F"] >> *F;
            
            _config.TriParams[idx].R = (void *) R;
            _config.TriParams[idx].T = (void *) T;
            _config.TriParams[idx].E = (void *) E;
            _config.TriParams[idx].F = (void *) F;
        
            Mat * projMatrixR = new cv::Mat;
            
            hconcat(*R, *T, *projMatrixR );
            _config.TriParams[idx].projMatrixR = (void * ) projMatrixR;
    
            if( projMatrixL->type()!= CV_64F || projMatrixR->type() != CV_64F)
                throw std::runtime_error("opencv matrix type is messed up!");
            L_(ldebug4) << WHERE  << "Calibration #" << idx << ": Proj matrix R "<< *projMatrixR;
            L_(ldebug4) << WHERE  << "Calibration #" << idx << ": Proj matrix L "<< *projMatrixL;
        }
        fs2.release();
        return true;
    }
    catch (std::runtime_error &ex)
    {
        L_(lerror) << WHERE << "StatusManager: error parsing settings. Exception :"<<ex.what()<<endl;
    }
    catch (...)
    {
        L_(lerror) << WHERE << "StatusManager: error parsing settings.";
        return false;
    }
    return false; // this is never called
}


void StatusManager::setDefaultSettings()
{
    _config.PointsRearrangeDistanceThreshMeters=0.05;
    _config.memorySpan=1;
    _config.nMarkers=4;
    _config.nTriParams=0;
    _config.syncTimes.badSync=0;
    _config.syncTimes.medSync=0;
    _config.syncTimes.goodSync=0;
}


void StatusManager::freeSettings()
{
    L_(ldebug3) << WHERE << "Cleaning up settings variable.";
    for (int i=0; i<_config.nTriParams; i++)
    {
        L_(ldebug4) << WHERE << "Deleting TriParams "<<i<<".";
        delete (Mat*)_config.TriParams[i].internalParametersMatrixL;
        delete (Mat*)_config.TriParams[i].distortionCoeffL;
        delete (Mat*)_config.TriParams[i].internalParametersMatrixR;
        delete (Mat*)_config.TriParams[i].distortionCoeffR;
        delete (Mat*)_config.TriParams[i].projMatrixL;
        delete (Mat*)_config.TriParams[i].projMatrixR;
        delete (Mat*)_config.TriParams[i].R;
        delete (Mat*)_config.TriParams[i].T;
        delete (Mat*)_config.TriParams[i].E;
        delete (Mat*)_config.TriParams[i].F;
    }
    _config.nTriParams=0;
    
    for (unsigned int i=0; i < _eyes.size(); i++)
    {
        clean_eye(i);
    }
    
    for (unsigned int i=0; i < _files.size(); i++)
    {      
        clean_file(i);
    }


}

/*!
 * \brief StatusManager::addEyes This method adds a couple of eyes.
 * \param leye details of the left eye.
 * \param reye details of the right eye.
 * \param name name of the eyes couple.
 * \return false if something went wrong (very odd), true otherwise.
 *
 * This function adds a couple of eyes to the current sources. 
 */
bool StatusManager::addEyes(EyeData_t leye, EyeData_t reye, std::string name)
{
    try
    {
        EyesPair_t data = {leye, reye, name};
        _eyes.push_back(data);
        _newSources = true;
        return true;
    }
    catch(...) // Catches everything
    {
        L_(lerror) << WHERE << "Something very very wierd happened!!";
        return false;
    }
    
}

/*!
 * \brief StatusManager::addFiles This method adds a couple of files.
 * \param leye FileData_t details of the left file.
 * \param reye FileData_t details of the right file.
 * \param name of the acquisition.
 * \return false if something went wrong (very odd), true otherwise.
 *
 * This function adds a couple of files to the current sources. 
 * Here we don't check the validity of the entered data. This *must* be done on the client.
 */
bool StatusManager::addFiles(FileData_t leye, FileData_t reye, std::string name)
{
    try
    {
        FilesPair_t data = {leye, reye, name};
        _files.push_back(data);
        _newSources = true;
        return true;
    }
    catch(...) // Catches everything
    {
        L_(lerror) << WHERE << "Something very very wierd happened!!";
        return false;
    }    
}

/*!
 * \brief StatusManager::removeSource Removes current source.
 * \return false if no sources available, no source selected or ST_NONE selected, true otherwise.
 *
 * This method removes the currently selected source. Always update sources after calling this!
 */
bool StatusManager::removeSource()
{
    if (!canPlay())
        return false;
    
    sourceType type = _current_source.first;
    unsigned int index = _current_source.second;
    
    if (type == ST_NONE)
        return false;
    
    if (type == ST_EYES)
    {
        if (index >= _eyes.size())
            return false;
        
        clean_eye(index);
        _eyes.erase(_eyes.begin()+index);
        _current_source = std::pair<sourceType,int>(ST_NONE,0);
        return true;
        
    }
    if (type == ST_FILES)
    {
        if (index >= _files.size())
            return false;
            
        clean_file(index);
        _files.erase(_files.begin()+index);
        _current_source = std::pair<sourceType,int>(ST_NONE,0);
        return true;        
    }
    return false;
}

/*!
 * \brief StatusManager::canStream Whether a streaming source is available and selected.
 * \return false if no sources of type streaming are available, true otherwise.
 *
 * This method checks if the currently selected source is a couple of eyes.
 */
bool StatusManager::canStream()
{
    if (_eyes.size() > 0 && _current_source.first == ST_EYES)
        return true;
    return false;
}

/*!
 * \brief StatusManager::canReproduce Whether a file source is available and selected.
 * \return false if no sources of type file are available, true otherwise.
 *
 * This method checks if the currently selected source is a couple of files.
 */

bool StatusManager::canReproduce()
{
    if (_files.size() > 0 && _current_source.first == ST_FILES)
        return true;
    return false;
}

/*!
 * \brief StatusManager::canReproduce Whether at least one playable source (stream or file) is available and selected.
 * \return false if no sources available or no playable source is selected, true otherwise.
 *
 * This method checks if the currently selected source can be played.
 */
bool StatusManager::canPlay()
{
    if (_files.size() == 0 && _eyes.size() == 0)
        return false;
    if (_current_source.first == ST_NONE)
        return false;
    
    return true;    
}

/*!
 * \brief StatusManager::sourcesAvailable Whether at least one source is available.
 * \return false if no sources are available, true otherwise.
 *
 * This method checks if there is at least one source.
 */
bool StatusManager::sourcesAvailable()
{
    if (_files.size() == 0 && _eyes.size() == 0)
        return false;
    return true;    
}

/*!
 * \brief StatusManager::eyesAvailable Whether at least one couple of eyes.
 * \return false if no sources of type eyes are available, true otherwise.
 *
 * This method checks if there is at least one couple of eyes.
 */
bool StatusManager::eyesAvailable()
{
    if (_eyes.size() == 0)
        return false;
    return true;    
}

/*!
 * \brief StatusManager::queryNewSources Asks if new sources have been added since last query.
 * \return false if no new sources were added, true otherwise.
 *
 * This method checks if new sources have been added since the last call to this method. 
 */
bool StatusManager::queryNewSources()
{
    if (_newSources == true)
    {
        _newSources = false;
        return true;
    } else {
        return false;
    }
        
}

/*!
 * \brief StatusManager::getSources Report sources available (NEEDS REWRITE).
 * \param[in,out] files to be filled with files available
 * \param[in,out] eyes vector to be filled with eyes available
 * \return always true.
 *
 * This method provides two std::vector containing files and eyes couples.
 */
bool StatusManager::getSources(std::vector<std::string> & files, std::vector<std::string> & eyes)
{
    for (unsigned int i=0; i<_files.size(); i++)
    {
        files.push_back(_files[i].name);
    }
    for (unsigned int i=0; i<_eyes.size(); i++)
    {
        eyes.push_back(_eyes[i].name);
    }
    
    return true;
}

/*!
 * \brief StatusManager::selectSource set current source.
 * \param type either ST_EYES or ST_FILES
 * \param index index of the element in the private _eyes of _files vector. 
 * \return false if type ST_NONE is selected, if no sources are available, if index is ivalid. True otherwise.
 *
 * This method sets the current source. The index must be correctly mapped to the correct vector, either _eyes or _files.
 */
bool StatusManager::selectSource(sourceType type, unsigned int index)
{
    L_(ldebug4) << WHERE << "StatusManager: selecting source.";
    if (!sourcesAvailable())
    {
        L_(lwarning) << WHERE << "StatusManager: no selectable source.";
        return false;
    }
    
    if (type == ST_NONE)
    {
        L_(lwarning) << WHERE << "StatusManager: cannot select ST_NONE source.";
        return false;
    }
    
    if (type == ST_EYES)
    {
        if (index >= _eyes.size())
            return false;
        
        _current_source = std::pair<sourceType,int>(type,index);
        return true;
        
    }
    if (type == ST_FILES)
    {
        if (index >= _files.size())
            return false;
        
        L_(ldebug4) << WHERE << "StatusManager: setting current source to ST_FILES, index " << index;
        _current_source = std::pair<sourceType,int>(type,index);
        return true;        
    }
    return false; //this should never happen
}


/*!
 * \brief StatusManager::getLSource get uri of the left component of the current source.
 * \param lsource string containing the url of the left file/eye.
 * \return false if type ST_NONE is selected or if no sources are available. True otherwise.
 *
 * This method is used to prepare the uri for the gstreamer uridecodebin element. 
 * Uri are of type /some/path for files and rtsp://address:port/mountpoint for files.
 * This method provides the left component (eye or file).
 */
bool StatusManager::getLSource(string & lsource)
{
    if (!canPlay())
        return false;
    //useless double check! Remove it!
    if (_current_source.first == ST_NONE)
        return false;
        
    if (_current_source.first == ST_FILES)
    {
        lsource = (_files[_current_source.second].leye).path;
        return true;
    }
    if (_current_source.first == ST_EYES)
    {
        
        lsource = std::string("rtsp://") + (_eyes[_current_source.second].leye).address
         + ":8554/" + (_eyes[_current_source.second].leye).mountpoint;
        return true;
    }
    return false; //this should never happen!
}

/*!
 * \brief StatusManager::getRSource get uri of the right component of the current source.
 * \param rsource string containing the url of the right file/eye.
 * \return false if type ST_NONE is selected or if no sources are available. True otherwise.
 *
 * This method is used to prepare the uri for the gstreamer uridecodebin element. 
 * Uri are of type /some/path for files and rtsp://address:port/mountpoint for files.
 * This method provides the right component (eye or file).
 */
bool StatusManager::getRSource(string & rsource)
{
    if (!canPlay())
        return false;
    if (_current_source.first == ST_NONE)
        return false;
        
    if (_current_source.first == ST_FILES)
    {
        rsource = (_files[_current_source.second].reye).path;
        return true;
    }
    if (_current_source.first == ST_EYES)
    {
        
        rsource = std::string("rtsp://") + (_eyes[_current_source.second].reye).address
         + ":8554/" + (_eyes[_current_source.second].reye).mountpoint;
        return true;
    }
    return false; //this should never happen!
}

bool StatusManager::getLCtrlConn(string & hostname, string & address, int & port)
{
    if (!canStream())
        return false;    
    
    hostname = (_eyes[_current_source.second].leye).host_name;
    address = (_eyes[_current_source.second].leye).address;
    port = (_eyes[_current_source.second].leye).port;
    return true;
}

bool StatusManager::getRCtrlConn(string & hostname, string & address, int & port)
{
    if (!canStream())
        return false;
    
    hostname = (_eyes[_current_source.second].reye).host_name;
    address = (_eyes[_current_source.second].reye).address;
    port = (_eyes[_current_source.second].reye).port;
    return true;
}

/*!
 * \brief StatusManager::getSyncDescriptors get configuration options for sync semaphore.
 * \param times where parameters are written
 * \return false if configuration is not set or is invalid, true otherwise.
 *
 * This method provides the descriptors for the sync quality. These are the values retorted in the config file.
 */
bool StatusManager::getSyncDescriptors(SyncTimes_t & times)
{
    // check if they are defined
    if (_config.syncTimes.badSync >0 && 
        _config.syncTimes.medSync >0 && 
        _config.syncTimes.goodSync >0 && 
        _config.syncTimes.badSync >  _config.syncTimes.medSync && 
        _config.syncTimes.medSync >  _config.syncTimes.goodSync)
    {
        times = _config.syncTimes;
        return true;
    }
    return false;
}

/*!
 * \brief StatusManager::getTriangulationParameters get triangulation parameters for specified width and height
 * \param w width of the frame
 * \param h height of the frame
 * \param nMarkers
 * \param memorySpan
 * \param reordThr
 * \param searchArea
 * \param kernelSize
 * \param distThreshold
 * \param internalParametersMatrixL
 * \param distortionCoeffL
 * \param internalParametersMatrixR
 * \param distortionCoeffR
 * \param projMatrixL
 * \param projMatrixR
 * \param R
 * \param T
 * \param E
 * \param F
 * \return false if configuration not found, true otherwise.
 *
 * This method provides the descriptors for the sync quality. These are the values retorted in the config file.
 */

bool StatusManager::getTriangulationParameters (int w, int h, 
                int & nMarkers, int & memorySpan, double & reordThr,
                int &searchArea, int &kernelSize, float &distThreshold,
                void * &internalParametersMatrixL, void * &distortionCoeffL,
                void * &internalParametersMatrixR, void * &distortionCoeffR,
                void * &projMatrixL, void * &projMatrixR,
                void * &R, void * &T, void * &E, void * &F)
{
    nMarkers = _config.nMarkers;
    memorySpan = _config.memorySpan;
    reordThr = _config.PointsRearrangeDistanceThreshMeters;
    
    
    for (int i=0; i< _config.nTriParams; i++)
    {
        if ( _config.TriParams[i].frameWidth == w && _config.TriParams[i].frameHeight == h)
        {
            searchArea = _config.TriParams[i].searchArea;
            kernelSize = _config.TriParams[i].kernelSize;
            distThreshold = _config.TriParams[i].distThreshold;
            
            internalParametersMatrixL =  _config.TriParams[i].internalParametersMatrixL;
            distortionCoeffL = _config.TriParams[i].distortionCoeffL;
            internalParametersMatrixR = _config.TriParams[i].internalParametersMatrixR;
            distortionCoeffR = _config.TriParams[i].distortionCoeffR;
            projMatrixL = _config.TriParams[i].projMatrixL;
            projMatrixR = _config.TriParams[i].projMatrixR;
            R= _config.TriParams[i].R;
            T= _config.TriParams[i].T;
            E= _config.TriParams[i].E;
            F= _config.TriParams[i].F;
            L_(ldebug2) << WHERE << "Selected config: " << _config.TriParams[i].frameWidth <<"x"<<_config.TriParams[i].frameHeight<<endl;
            return true;
        }
    }
    return false;
}

/*!
 * \brief StatusManager::clean_eye frees memory
 * \param i index of the eye
 * \return false if index i is invalid, true otherwise.
 *
 * This method is used to free memory
 */
bool StatusManager::clean_eye(unsigned int i)
{
    if (i>=_eyes.size())
        return false;
        
    if (_eyes[i].leye.name != NULL)
    {
        delete _eyes[i].leye.name;
        _eyes[i].leye.name = NULL;
    }
    if (_eyes[i].leye.host_name != NULL)
    {
        delete _eyes[i].leye.host_name;
        _eyes[i].leye.host_name = NULL;
    }
    if (_eyes[i].leye.type != NULL)
    {
        delete _eyes[i].leye.type;
        _eyes[i].leye.type = NULL;
    }
    if (_eyes[i].leye.address != NULL)
    {
        delete _eyes[i].leye.address;
        _eyes[i].leye.address = NULL;
    }
    if (_eyes[i].leye.domain != NULL)
    {
        delete _eyes[i].leye.domain;
        _eyes[i].leye.domain = NULL;
    }
    if (_eyes[i].leye.mountpoint != NULL)
    {
        delete _eyes[i].leye.mountpoint;
        _eyes[i].leye.mountpoint = NULL;
    }
    
    // Right
    if (_eyes[i].reye.name != NULL)
    {
        delete _eyes[i].reye.name;
        _eyes[i].reye.name = NULL;
    }
    if (_eyes[i].reye.host_name != NULL)
    {
        delete _eyes[i].reye.host_name;
        _eyes[i].reye.host_name = NULL;
    }
    if (_eyes[i].reye.type != NULL)
    {
        delete _eyes[i].reye.type;
        _eyes[i].reye.type = NULL;
    }
    if (_eyes[i].reye.address != NULL)
    {
        delete _eyes[i].reye.address;
        _eyes[i].reye.address = NULL;
    }
    if (_eyes[i].reye.domain != NULL)
    {
        delete _eyes[i].reye.domain;
        _eyes[i].reye.domain = NULL;
    }
    
    if (_eyes[i].reye.mountpoint != NULL)
    {
        delete _eyes[i].reye.mountpoint;
        _eyes[i].reye.mountpoint = NULL;
    }
    
    return true;
}

/*!
 * \brief StatusManager::clean_file frees memory associated to file entry
 * \param i index of the file
 * \return false if index i is invalid, true otherwise.
 *
 * This method is used to free memory
 */
bool StatusManager::clean_file(unsigned int i)
{
    if (i>=_files.size())
        return false;
        
    if (_files[i].leye.name != NULL)
    {
        delete _files[i].leye.name;
        _files[i].leye.name = NULL;
    }
    if (_files[i].leye.path != NULL)
    {
        delete _files[i].leye.path;
        _files[i].leye.path = NULL;
    }
    if (_files[i].leye.type != NULL)
    {
        delete _files[i].leye.type;
        _files[i].leye.type = NULL;
    }
    // Right
    if (_files[i].reye.name != NULL)
    {
        delete _files[i].reye.name;
        _files[i].reye.name = NULL;
    }
    if (_files[i].reye.path != NULL)
    {
        delete _files[i].reye.path;
        _files[i].reye.path = NULL;
    }
    if (_files[i].reye.type != NULL)
    {
        delete _files[i].reye.type;
        _files[i].reye.type = NULL;
    } 
    return true;     
}

/*!
 * \brief StatusManager::checkEyesPresent checks if a couple of eyes is already present
 * \param leye left eye of the couple
 * \param reye right eye of the couple
 * 
 * \return false if the eyes couple is not found, true otherwise.
 *
 * This method checks if a couple of eyes is already present.
 */
bool StatusManager::checkEyesPresent(EyeData_t leye, EyeData_t reye)
{
    for (unsigned int i=0; i< _eyes.size(); i++)
    {
        int nequal=0;
        if (_eyes[i].reye == reye)
            nequal++;
        if (_eyes[i].leye == leye)
            nequal++;
        if (nequal==2)
            return true;
        if (nequal==1)
        {
            L_(lerror) << "One eye equal, the other different!! This should not happen!";
            return false;
        }
    }
    return false;
}

