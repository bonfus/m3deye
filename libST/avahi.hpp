

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

#include <avahi-client/client.h>
#include <avahi-client/lookup.h>

#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>

#include <string.h>
#include "types.hpp"

typedef struct AvahiData
{
    AvahiClient *client;
    AvahiServiceBrowser *sb;
    AvahiSimplePoll *simple_poll;
    EyeData_t * services;
    int nServices;
} AvahiData_t;

class Avahi {
public:
    Avahi();
    ~Avahi();
    bool Run(int timeout);
    EyeData_t * getResults(int idx);

private:
    bool Prepare();
    void Clean();    
    static void client_callback(AvahiClient *c, AvahiClientState state, void * userdata);
    static void browse_callback(AvahiServiceBrowser *b, AvahiIfIndex interface, AvahiProtocol protocol,
        AvahiBrowserEvent event,    const char *name,    const char *type,    const char *domain,
        AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,     void* userdata);
    static void resolve_callback(AvahiServiceResolver *r, AVAHI_GCC_UNUSED AvahiIfIndex interface,
        AVAHI_GCC_UNUSED AvahiProtocol protocol, AvahiResolverEvent event, const char *name,
        const char *type, const char *domain, const char *host_name, const AvahiAddress *address,
        uint16_t port, AvahiStringList *txt, AvahiLookupResultFlags flags, AVAHI_GCC_UNUSED void* userdata);
    
    AvahiData_t * _data;
    
};
