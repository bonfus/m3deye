#ifdef HAVE_GUI

#include "render.hpp"

#ifdef HAVE_QT


class QTVideoRender : public VideoRender
{
public:
    void render(IplImage * frame);
    void setDrawable(QLabel ** l);
private:    
    QLabel * _uiLabelQt;
}


void QTVideoRender::setDrawable(QLabel ** l)
{
    _uiLabelQt = *l;
    _videoInterface = QT;
}


void QTVideoRender::render(Mat & frame)
{
    IplImage *img = cvCloneImage(frame);
    QImage qimg;
    qimg = IplImage2QImage(img);
    _uiLabelQt->setPixmap(QPixmap::fromImage(qimg));
    cvReleaseImage(&img);
}
#endif

#endif
