#ifdef HAVE_X11
#include <stdio.h>
#include <string.h>

#include <opencv2/core/core.hpp>
#include "opencv2/opencv.hpp"
#include "render.hpp"
#include "renderx11.hpp"







#if defined(__QNX__) || defined(MINIX_SUPPORT)
Status XShmAttach(Display *display, XShmSegmentInfo *shminfo)
{
  return 0;
}

Status XShmDetach(Display *display, XShmSegmentInfo *shminfo)
{
  return 0;
}
#endif

#ifdef MINIX_SUPPORT
typedef long key_t;

struct ipc_perm
{
  key_t          key;
  unsigned short uid;
  unsigned short gid;
  unsigned short cuid;
  unsigned short cgid;
  unsigned short mode;
  unsigned short seq;
};

struct shmid_ds
{
  struct          ipc_perm shm_perm;
  int             shm_segsz;
  time_t          shm_atime;
  time_t          shm_dtime;
  time_t          shm_ctime;
  unsigned short  shm_cpid;
  unsigned short  shm_lpid;
  short           shm_nattch;
  unsigned short  shm_npages;
  unsigned long   *shm_pages;
};

#define IPC_PRIVATE ((key_t)0)
#define IPC_CREAT   0x0200
#define IPC_RMID    0x1000

int shmget(key_t key, int size, int shmflag)
{
  return -1;
}

void *shmat(int shmid, const void *shmaddr, int shmflag)
{
}

int shmctl(int shmid, int cmd, struct shmid_ds *buf)
{
  return -1;
}

int shmdt(const void *shmaddr)
{
  return -1;
}
#endif

/* YUV formats in preferred order. */
fourcc_t fourcc_list[] = {
  /* YUV 4:2:2 formats: */
  { FOURCC_UYVY },
  { FOURCC_YUY2 },
  { FOURCC_YVYU },
  /* YUV 4:1:1 formats: */
  { FOURCC_YV12 },
  { FOURCC_I420 },
  { FOURCC_IYUV }, /* IYUV is a duplicate of I420. */
};

int find_yuv_port(Display* display, XvPortID* port, fourcc_t* format)
{
  int i, j, k;

  /* XvQueryExtension */
  unsigned int version, release, request_base, event_base, error_base;

  /* XvQueryAdaptors */
  unsigned int num_adaptors;
  XvAdaptorInfo* adaptor_info = NULL;
  XvPortID port_id;

  /* XvListImageFormats */
  int num_formats;
  XvImageFormatValues* format_list = NULL;

  switch (XvQueryExtension(display, &version, &release,
			   &request_base, &event_base, &error_base))
  {
  case Success:
    break;
  case XvBadExtension:
    printf("XvQueryExtension returned XvBadExtension.\n");
    return 0;
  case XvBadAlloc:
    printf("XvQueryExtension returned XvBadAlloc.\n");
    return 0;
  default:
    printf("XvQueryExtension returned unknown error.\n");
    return 0;
  }

  switch (XvQueryAdaptors(display, DefaultRootWindow(display),
			  &num_adaptors, &adaptor_info))
  {
  case Success:
    break;
  case XvBadExtension:
    printf("XvQueryAdaptors returned XvBadExtension.\n");
    return 0;
  case XvBadAlloc:
    printf("XvQueryAdaptors returned XvBadAlloc.\n");
    return 0;
  default:
    printf("XvQueryAdaptors returned unknown error.\n");
    return 0;
  }

  /* Find YUV capable adaptor. */
  for (i = 0; i < (int)num_adaptors; i++) {
    if (!(adaptor_info[i].type & XvInputMask
	  && adaptor_info[i].type & XvImageMask))
    {
      continue;
    }

    format_list = XvListImageFormats(display, adaptor_info[i].base_id,
				     &num_formats);

    for (j = 0; j < (int)(sizeof(fourcc_list) / sizeof(*fourcc_list)); j++) {
      if (format->id && fourcc_list[j].id != format->id) {
        continue;
      }
      for (k = 0; k < num_formats; k++) {
        if (format_list[k].id != fourcc_list[j].id) {
          continue;
        }

        for (port_id = adaptor_info[i].base_id;
            port_id < adaptor_info[i].base_id + adaptor_info[i].num_ports;
            port_id++)
          {
            if (XvGrabPort(display, port_id, CurrentTime) != Success) {
              continue;
            }
            *port = port_id;
            *format = fourcc_list[j];
            XFree(format_list);
            XvFreeAdaptorInfo(adaptor_info);
            return 1;
          }
      }
    }

    XFree(format_list);
  }

  XvFreeAdaptorInfo(adaptor_info);
  printf("No suitable Xv YUV adaptor/port available.\n");
  return 0;
}


XvImage* create_yuv_image(Display* display, XvPortID port, fourcc_t format,
			  int width, int height, XShmSegmentInfo* shminfo)
{
  XvImage* image;

  if (shminfo) {
    if (!(image = XvShmCreateImage(display, port, format.id, NULL,
				   width, height, shminfo)))
    {
      printf("Unable to create shm XvImage\n");
      return NULL;
    }
      
    if ((shminfo->shmid = shmget(IPC_PRIVATE, image->data_size, IPC_CREAT | 0777)) == -1)
    {
      printf("Unable to allocate shared memory\n");
      XFree(image);
      return NULL;
    }
    if (!(shminfo->shmaddr = (char*) shmat(shminfo->shmid, 0, 0))) {
      printf("Unable to attach shared memory\n");
      XFree(image);
      shmctl(shminfo->shmid, IPC_RMID, 0);
      return NULL;
    }
    shminfo->readOnly = False;

    image->data = shminfo->shmaddr;

    if (!XShmAttach(display, shminfo)) {
      printf("XShmAttach failed\n");
      XFree(image);
      shmctl(shminfo->shmid, IPC_RMID, 0);
      shmdt(shminfo->shmaddr);
      return NULL;
    }

    /* Send image to X server. This instruction is required, since having
     * built a Shm XImage and not using it causes an error on XCloseDisplay. */
    XSync(display, False);

    /* Mark the segment to be automatically removed when the last
       attachment is broken (i.e. on shmdt or process exit). */
    shmctl(shminfo->shmid, IPC_RMID, 0);
  }
  else {
    if (!(image = XvCreateImage(display, port, format.id, NULL,
				width, height)))
    {
      printf("Unable to create XvImage\n");
      return NULL;
    }
    image->data = (char*) malloc(image->data_size);
  }

  return image;
}


void destroy_yuv_image(Display* display, XvImage* image,
		       XShmSegmentInfo* shminfo)
{
  if (shminfo) {
    XShmDetach(display, shminfo);
    XFree(image);
    shmdt(shminfo->shmaddr);
  }
  else {
    XFree(image);
  }
}


void display_yuv_image(Display* display, XvPortID port, Drawable d, GC gc,
		       XvImage* image,
		       XShmSegmentInfo* shminfo,
		       int src_x, int src_y,
		       unsigned int src_w, unsigned int src_h,
		       unsigned int dest_w, unsigned int dest_h,
		       double aspect_ratio)
{
  int dest_x = 0, dest_y = 0;

  /* Keep aspect ratio of src image. */
  if (dest_w*src_h < src_w*aspect_ratio*dest_h) {
    dest_y = dest_h;
    dest_h = dest_w*src_h/(src_w*aspect_ratio);
    dest_y = (dest_y - dest_h)/2;
  }
  else {
    dest_x = dest_w;
    dest_w = dest_h*src_w*aspect_ratio/src_h;
    dest_x = (dest_x - dest_w)/2;
  }

  if (shminfo) {
    XvShmPutImage(display, port, d, gc, image,
		  src_x, src_y, src_w, src_h,
		  dest_x, dest_y, dest_w, dest_h, False);
  }
  else {
    XvPutImage(display, port, d, gc, image,
	       src_x, src_y, src_w, src_h,
	       dest_x, dest_y, dest_w, dest_h);
  }
}






void X11VideoRender::setDrawable(int d)
{
  
  _drawable = d;
  _dpy = XOpenDisplay(NULL);
  if (_dpy == NULL) {
    printf("Cannot open Display.\n");
    exit (-1);
  }    
  
  
  fourcc_t format;
  format.id = FOURCC_YV12;
  
  
  if (find_yuv_port(_dpy, &_xv_port, &format) != 1)
  {  
    printf("Cannot find port!\n");
    return;
  }
  
  _gc = XCreateGC(_dpy, _drawable, 0, 0);
  
  if (_gc == NULL)  {
     printf("Bad Alloc\n");
     return ;
  }
  
  _yuv_image = create_yuv_image(_dpy, _xv_port, format, 432, 162, &yuv_shminfo);
  
  if (_yuv_image == NULL)  {
     printf("Cannot create img\n");
     return ;
  }  
  
  //printf("Data size: %d\n", _yuv_image->data_size);
  printf("Image size: %dx%d\n", _yuv_image->width,_yuv_image->height);
  printf("Data size: %d\n", _yuv_image->data_size);
  printf("Planes: %d\n", _yuv_image->num_planes);
  for (int i=0; i<_yuv_image->num_planes; i++)
  {
    printf("Pitch %d: %d\n", i, _yuv_image->pitches[i]);
    printf("Offsets %d: %d\n", i, _yuv_image->offsets[i]);
  }
  
  return;
  
}


void X11VideoRender::render(cv::Mat & frame)
{
  cv::Mat conversion;
  int _d;
  unsigned int _D, _ww, _hh;
  Window _dw;  

  
  
  cv::Mat dst;
  cv::Size s( _yuv_image->width, _yuv_image->height );
  
  resize( frame, dst, s, 0, 0, CV_INTER_LINEAR );
  
  cvtColor(dst, conversion, cv::COLOR_BGR2YUV_YV12);
  
  memcpy(_yuv_image->data , (uchar *) conversion.data, _yuv_image->data_size);  
  
  XGetGeometry(_dpy, _drawable, &_dw, &_d, &_d, &_ww, &_hh, &_D, &_D);
  
  display_yuv_image(_dpy, _xv_port, _drawable, _gc, _yuv_image, 
                    &yuv_shminfo,0,0,432,162,_ww,_hh,1.0);
  
  return;
          
}


void X11VideoRender::setSourceResolution(int w, int h)
{
}
void X11VideoRender::cleanup(){
  destroy_yuv_image(_dpy, _yuv_image, &yuv_shminfo);
}

X11VideoRender::~X11VideoRender(){
  cleanup();
  printf("Cleaned up\n");
}

#endif  
