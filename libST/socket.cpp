#include<stdio.h> //printf
#include<string.h>    //strlen
#include<string>  //string


#include<unistd.h>
#include <errno.h>
#include "types.hpp"
#include "socket.hpp"
#include "log.h"

using namespace std;

 
tcp_client::tcp_client()
{
    sock = -1;
}

/**
    Close connection
*/
bool tcp_client::clse()
{
    if(sock != -1)
    {
        close(sock);
        sock=-1;
        return true;
    }
    return false;
}

/**
    Connect to a host on a certain port number
*/
bool tcp_client::conn(const char* hostname , const char* address, int port)
{
    //create socket if it is not already created
    if(sock == -1)
    {
        //Create socket
        sock = socket(AF_INET , SOCK_STREAM , 0);
        if (sock == -1)
        {
            L_(lerror) << WHERE << ("Could not create socket. ");
        }
         
        L_(ldebug2) << WHERE << "Socket created";
    }
    else    {   /* OK , nothing */  }
     
    //setup address structure
    bool resolved = false;    

    struct hostent *he;
    struct in_addr **addr_list;

    if (inet_addr(address) != INADDR_NONE)
    {
        L_(ldebug2) << WHERE <<" Setting address: " << address;
        server.sin_addr.s_addr = inet_addr( address );
        resolved = true;
    }

    L_(ldebug2) << WHERE <<"Resolving hostname: " << hostname;
    //resolve the hostname, its not an ip address
    if (!resolved)
    {
        resolved = true;
        if  ((he = gethostbyname( hostname ) ) == NULL)
        {
            //gethostbyname failed
            //L_(lerror) << WHERE << herror("gethostbyname");
            L_(lerror) << WHERE <<"Failed to resolve hostname";
            resolved = false;
        }
        if (resolved)
        {
            //Cast the h_addr_list to in_addr , since h_addr_list also has the ip address in long format only
            addr_list = (struct in_addr **) he->h_addr_list;
        
            for(int i = 0; addr_list[i] != NULL; i++)
            {
                //strcpy(ip , inet_ntoa(*addr_list[i]) );
                server.sin_addr = *addr_list[i];
                
                L_(ldebug2) << WHERE << hostname<<" resolved to "<<inet_ntoa(*addr_list[i]);
                
                break;
            }
        }
    }
    
    if (!resolved) return false;
    
    server.sin_family = AF_INET;
    server.sin_port = htons( port );
     
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        L_(lerror) << WHERE << ("connect failed. ") << strerror(errno);
        return false;
    }
     
    L_(ldebug2) << WHERE <<"Connected";
    return true;
}
 
/**
    Send data to the connected host
*/
bool tcp_client::send_command(command_t cmd)
{
    
    char cmdbuf[10] = {0};
    
    switch (cmd.cmd)
    {
        case SET_BRIGHTNESS:
            cmdbuf[0] = EC_SET_BRIGHTNESS;
            break;
        case SET_CONTRAST:
            cmdbuf[0] = EC_SET_CONTRAST;
            break;
        case SET_SATURATION:
            cmdbuf[0] = EC_SET_SATURATION;
            break;
        case SET_SHUTTERSPEED:
            cmdbuf[0] = EC_SET_SHUTTERSPEED;
            break;
        case SAVE_SETTINGS:
            cmdbuf[0] = EC_SET_SHUTTERSPEED;
            break;
        default:
            L_(lerror) << WHERE <<"Command not valid!!!";
            return false;
    }
    if (cmd.increment)
        cmdbuf[1] = EC_ACTION_INCREMENT;
    else
        cmdbuf[1] = EC_ACTION_SET;
    
    //memcpy(cmdbuf+2,&cmd.value,4);

    char ptrint32[4] = {0};
    
    SerializeInt32(ptrint32,cmd.value);
    
    for(int i=2;i<6;++i)     // note da 2 a 6!!
        cmdbuf[i] = ptrint32[i-2];
    
    //Send some data
    if( send(sock , cmdbuf , 10 , 0) < 0)
    {
        //perror("Send failed : ");
        L_(lerror) << WHERE << ("Send failed : ") << strerror(errno);
        return false;
    }
    L_(ldebug2) << WHERE <<"Data sent";


    char buffer[5];
    //Receive a reply from the server
    if( recv(sock , buffer , 5 , 0) < 0)
    {
        L_(lerror) << WHERE << "recv failed";
        return false;
    } else {
    
        int8_t  * pfret =  (int8_t *) buffer;
        int8_t fret = *pfret;
        
        char bufint32[4] = {0};
        for(int i=1;i<5;++i)     // note da 1 a 5!!
            bufint32[i-1] = buffer[i];
            
        int32_t vret =  ParseInt32(bufint32);
        
        L_(ldebug2) << WHERE << "Function returned " << static_cast<int>(fret) << " and value set to "<<vret;
        if (fret == 0)
            return true;
        else
            return false;            
    }
    

}

 

