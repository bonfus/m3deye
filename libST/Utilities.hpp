#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <numeric>

#include <algorithm>
#include <math.h>
#include <utility>      // std::pair, std::make_pair

#include <vector>
#include <stdio.h>
#include <chrono>
#include <inttypes.h>

#include <iomanip>

#include "types.hpp"


#define SEMAPHORE_RED CV_RGB( 255, 0, 0 )
#define SEMAPHORE_YELLOW CV_RGB( 147, 147, 0 )
#define SEMAPHORE_GREEN CV_RGB( 0, 255, 0 )

using namespace std;
using namespace cv;

cv::Point3f operator*(cv::Mat M, const cv::Point3f& p);

cv::Point3f operator+(cv::Mat M, const cv::Point3f& p);


int CountNanPoints(const vector<Point3f>& points3d);
bool ReArrangePoints3dAndFillWithNans(const vector<Point3f> & points3dPreviousFrame, const vector<Point3f>& points3d, vector<Point3f> * outReararngedPoints, vector<int> *newIndices, const double distanceThreshMeters);

int FindBestMatches(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, vector<Vec2d> & ppL, vector<Vec2d> & ppR);
int FindBestMatchesInHistory(const int frameNumber, vector <pair< Vec2d, int> >& localMaximaLBefore, vector <pair< Vec2d, int> > &localMaximaRBefore, const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, Mat* pixels2dL, Mat* pixels2dR, const float distThreshold, const int memorySpan);

int FindBestMatchesLAPJV(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, vector<Vec2d> & ppL, vector<Vec2d> & ppR);
int FindBestMatchesAndConvertToMat(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, Mat* pixels2dL, Mat*pixels2dR);

int FindBestMatchesNaive(const vector<vector<double> > &errorsRepr, const vector <pair< Vec2d, float> >& localMaximaL, const vector <pair< Vec2d, float> > &localMaximaR, Mat* pixels2dL, Mat*pixels2dR);

void TriangulatePointsTest();

bool PairPoint3fCompare(const std::pair<cv::Point3f, float>& firstElem, const std::pair<cv::Point3f, float>& secondElem);
bool PairCompare(const std::pair<cv::Vec2d, float>& firstElem, const std::pair<cv::Vec2d, float>& secondElem);

void ReadTimeStampsFile(const string &fileName, vector<int> *indices, vector<int64_t> *timeStamps, vector<int> *deltat);

void GetLocalMaxima(const cv::Mat ProcessImg,int MatchingSize, int Threshold, int GaussKernel, vector <pair< cv::Vec2d, float > >* vMaxLoc);
void FastGetLocalMaxima(const cv::Mat ProcessImg,int MatchingSize, int Threshold, int GaussKernel, int nMarkers, vector <pair< cv::Vec2d, float > >* vMaxLoc);


void ExploseImageChannels(Mat &frame, Mat &frameChannels);


template<typename T> void ComputeMeanMaxMin(const vector<T> &v, T *mean, T * max, T *min)
{
	double sum = std::accumulate(v.begin(), v.end(), 0.0);
	*mean = sum / v.size();
	*max = *max_element(v.begin(), v.end());
	*min = *min_element(v.begin(), v.end());
	//	double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
	//	double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
	//	This is susceptible to overflow or underflow for huge or tiny values. A slightly better way to calculate the standard deviation is:

//	std::vector<double> diff(v.size());
//	std::transform(v.begin(), v.end(), diff.begin(), std::bind2nd(std::minus<double>(), *mean));
//	double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
//	*stdev = std::sqrt(sq_sum / v.size());

}








#endif
