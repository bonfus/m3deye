#ifndef STATUS_MANAGER_H
#define STATUS_MANAGER_H


//this should become a class
#include <vector>
#include <utility>
#include <string>
#include "types.hpp"
#include "log.h"

typedef struct EyesPair
{
    EyeData_t leye;
    EyeData_t reye;
    std::string name;
} EyesPair_t;

typedef struct FilesPair
{
    FileData_t leye;
    FileData_t reye;
    std::string name;
} FilesPair_t;

/*!
 * \brief StatusManager 
 *        handles sources for triangulation and visualization.
 *
 * This class contains and manages all the available sources.
 * In principle it works as follows:
 * Resources can be either eyes or files.
 * First one should add a source.
 * Aftwerwards one selects a source.
 * The selected source can be played, analyzed or delated.
 * Once the selected source is deleted, the user should be asked
 * to select another source.
 * 
 */

class StatusManager
{
public:
    StatusManager();
    ~StatusManager();
    bool parseSettings(const char * );

// add and manage sources
    bool addEyes(EyeData_t leye, EyeData_t reye, std::string name);
    bool checkEyesPresent(EyeData_t leye, EyeData_t reye);
    bool addFiles(FileData_t leye, FileData_t reye, std::string name);
    
    
// manipulate sources
    bool getSources(std::vector<std::string> & files, std::vector<std::string> & eyes);
    bool selectSource(sourceType type, unsigned int index);
    bool removeSource(); // removes current source
    bool queryNewSources();

// founctions to probe actual state
    bool canStream();
    bool canReproduce();
    bool canPlay();
    bool sourcesAvailable();
    bool eyesAvailable();
    
// functions to get info on the sources.
    bool getLSource(std::string& lsource);
    bool getRSource(std::string& rsource);

    bool getLCtrlConn(std::string & hostname, std::string & address, int & port);
    bool getRCtrlConn(std::string & hostname, std::string & address, int & port);
    
// triangulation parameters
    bool getTriangulationParameters (int w, int h,
                int & nMarkers, int & memorySpan, double & reordThr,
                int &searchArea, int &kernelSize, float &distThreshold,
                void * &internalParametersMatrixL, void * &distortionCoeffL,
                void * &internalParametersMatrixR, void * &distortionCoeffR,
                void * &projMatrixL, void * &projMatrixR,
                void * &R, void * &T, void * &E, void * &F);
                
    bool getSyncDescriptors(SyncTimes_t & );
private:
    void setDefaultSettings();
    void freeSettings();
    bool clean_eye(unsigned int);
    bool clean_file(unsigned int);
    // are new sources available?
    bool _newSources;
    Config_t _config;
    Status_t _status;
    std::vector<EyesPair_t> _eyes;
    std::vector<FilesPair_t> _files;
    std::pair<sourceType, unsigned int> _current_source;
};

#endif
