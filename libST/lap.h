/************************************************************************
*
*  lap.h
   version 1.0 - 21 june 1996
   author  Roy Jonker, MagicLogic Optimization Inc.
   
   header file for LAP
*
**************************************************************************/

/*************** CONSTANTS  *******************/

  #define BIG 100000
  #if !defined TRUE
  #define	 TRUE		1
  #endif
  #if !defined FALSE
  #define  FALSE		0
  #endif
/*************** TYPES      *******************/

  typedef int row;
  typedef int col;
  typedef double cost;
  typedef int boolean;
/*************** FUNCTIONS  *******************/

extern cost lap(int dim, cost **assigncost,
               int *rowsol, int *colsol, cost *u, cost *v);


