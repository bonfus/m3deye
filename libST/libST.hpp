
#include <thread>
#include <mutex>

#include <sigc++/sigc++.h>
#include <sigc++config.h>
#include "types.hpp"
#include "log.h"


#include "render.hpp"
#include "avahi.hpp"
#include "socket.hpp"
#include "statusManager.hpp"


/*!
 * \brief ST 
 *        StereoTriangulation library.
 *
 * This class is the interface to all the functions providing
 * access, control and evaluation of cameras and 3D points.
 * 
 * Sources are handled by the statusManager.
 */

class ST
{
public:
    ST();
    ~ST();
    bool parse_config(const char* path);

    // functions to add data sources
    bool searchEyes(int timeout);
    bool addEyes(const char * , const char * , const char * , const char * , int , int ,const char * , const char * , const char * , const char * , int , int );
    bool addFiles(const char* lvideo,const char * rvideo, const char * name);

    // functions to select sources
    int getSources(Source_t ** data);
    bool setSource(sourceType type, unsigned int index);
    bool queryNewSources();

    // function to control system
    bool run(const bool, const bool, const bool locking = false);
    bool show(const bool locking = false);
    bool stop();
    bool save(const char * Lfilename,const char * Rfilename);
        
    // Properties
    bool cam_adjust_property(CmdTypes what, int value, bool increment);
    bool cam_save_settings();
    
    
    // Video rendedrs functions
    bool initRender(VideoRenderType rt);
    bool freeRender();
    VideoRender * render;
    
    
    
    sigc::signal<void, Point3D_t *, int, int, int> points_ready;
    sigc::signal<void, stStatus> status;
    sigc::signal<void, stMessages> messages;
    
private:
    Avahi * avahiInspector;
    tcp_client cmdSender[2];  //this will become a class
    StatusManager stMgr;
    
    // These functions are used to control camera
    bool camConnect(bool closeConn);
    bool sendCmdToCameras(command_t cmd);
    
    // these functions do the dirty job, they are threaded!
    void display();
    void store(const char*, const char *);
    void triangulate(const bool, const bool );
    
    // this variable controls the threads
    volatile bool _acq;
    std::mutex _acq_mutex;
    std::thread _acqThread;
  
};
