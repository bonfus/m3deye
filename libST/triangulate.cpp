#include <thread>
#include <mutex>
#include "triangulate.hpp"
#include "Utilities.hpp"

#include "types.hpp"
#include "math.h" //isnan


#include "libST.hpp"
#include "GstInput.hpp"
  
// to estimate speed
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;


inline void prepareFrame(Mat frame1, Mat frame2, int w, int h, int sync, SyncTimes_t sTimes , Mat & frameToDisplay)
{
    if(sync>=0)
    {
        if (sync > sTimes.medSync) {
            cv::circle(frame2, cv::Point(w-(int)(0.1*w),(int)(0.1*h)), 12, SEMAPHORE_RED,-5);
        } else if (sync > sTimes.goodSync) {
            cv::circle(frame2, cv::Point(w-(int)(0.1*w),(int)(0.1*h)), 12, SEMAPHORE_YELLOW,-5);
        } else {
            cv::circle(frame2, cv::Point(w-(int)(0.1*w),(int)(0.1*h)), 12, SEMAPHORE_GREEN,-5);
        }
    }
    
    hconcat(frame1, frame2, frameToDisplay);
}

inline void DrawCross(Mat & image, const Point & p, const Scalar & color)
{
    int thickness = 1;
    int lunghness = 40;
    line(image, Point( p.x-lunghness, p.y), Point( p.x+lunghness, p.y), color, thickness);
    line(image, Point( p.x, p.y-lunghness), Point( p.x, p.y+lunghness), color, thickness);
}


inline cv::Scalar get_color(int id)
{
    id = id%5;
    if (id == 0) return CV_RGB(170,70,67)  ;   //(0,99,190)  ;
    if (id == 1) return CV_RGB(240,147,3)  ;   //(254,217,0) ;
    if (id == 2) return CV_RGB(112,153,49) ;   //(0,120,54)  ;
    if (id == 3) return CV_RGB(66,109,163) ;   //(255,111,32);
    if (id == 4) return CV_RGB(242,242,242);   //(88,15,139) ;
    throw std::invalid_argument("Invalid color ID !!!");
}

bool ST::run(const bool bSendPoints, const bool bShowVideo, const bool locking)
{

    // qui ci starebbe da controllare qualcosina...
    if (!stMgr.canPlay())
    {
        messages.emit(ST_NO_SOURCE);
        return false;
    }


    //check conditions
    if (_acqThread.joinable())
    {
        L_(ldebug2) << WHERE << "Joining acq thread.";
        _acqThread.join();
    }
    L_(ldebug2) << WHERE << "The system is currently " << ( (_acq==true) ? "working" : "idle" );
    if (_acq == false)
    {
        _acq_mutex.lock();
        _acq=true;
        _acq_mutex.unlock();                
        _acqThread = thread(&ST::triangulate, this, bSendPoints, bShowVideo);
        if (locking)
            _acqThread.join();
        else
            _acqThread.detach();
            
        return true;
    } else {
        messages.emit(ST_NOT_IDLE);
        return false;
    }

}

bool ST::stop()
{
    if (_acq==true)
    {
        _acq_mutex.lock();
        _acq=false;
        _acq_mutex.unlock();
        
        L_(ldebug2) << WHERE << "_acq set to false.";
        if (_acqThread.joinable())
        {
            L_(ldebug2) << WHERE << "Joining acq thread.";
            _acqThread.join();
        }
        return true;
    } else
    {
        L_(ldebug2) << WHERE << "Nothing to stop.";
        return false;
    }
}


bool ST::show(bool locking)
{ 
    //check conditions
    if (!stMgr.canPlay())
    {
        messages.emit(ST_NO_SOURCE);
        return false;
    }
        
    if (_acqThread.joinable())
    {
        L_(ldebug2) << WHERE << "Joining acq thread.";
        _acqThread.join();
    }
    if (_acq == false)
    {
        _acq_mutex.lock();
        _acq=true;
        _acq_mutex.unlock();
        
        _acqThread = thread(&ST::display, this);
        if (locking)
            _acqThread.join();
        else
            _acqThread.detach();

        return true;
    } else {
        messages.emit(ST_NOT_IDLE);
        return false;
    }

}

bool ST::save(const char * Lfilename, const char * Rfilename)
{ 
    //check conditions
    if (!stMgr.canStream())
    {
        messages.emit(ST_NO_SOURCE);
        return false;
    }
        
    if (_acqThread.joinable())
    {
        L_(ldebug2) << WHERE << "Joining acq thread.";
        _acqThread.join();
    }
    if (_acq == false)
    {
        _acq_mutex.lock();
        _acq=true;
        _acq_mutex.unlock();
        
        _acqThread = thread(&ST::store, this, Lfilename, Rfilename);
        _acqThread.detach();
        return true;
    } else {
        messages.emit(ST_NOT_IDLE);
        return false;
    }

}

void ST::store(const char * Lfilename, const char * Rfilename) 
{    
    //VideoCapture captureL;
    //VideoCapture captureR;
    L_(ldebug4) << WHERE << "Preparing to store data to " << Lfilename << " and " << Rfilename;
    
    GstInput capture;
    L_(ldebug2) << WHERE << "Trying to open video source...";

    //std::string connL = std::string("rtsp://")  + (_config.eyes[LEYE])->address + ":8554/test " + (_config.eyes[LEYE])->address + " 8554";
    //std::string connR = std::string("rtsp://")  + (_config.eyes[REYE])->address + ":8554/test " + (_config.eyes[REYE])->address + " 8554";
    //
    //
    //captureL.open(connL);
    //captureR.open(connR);
    
    string lsource;
    string rsource;
    bool ret = stMgr.getLSource(lsource);
    if (!ret)
    {
        L_(lerror) << WHERE << "Cannot obtain lsource";
        return;            
    }
    ret = stMgr.getRSource(rsource);
    if (!ret)
    {
        L_(lerror) << WHERE << "Cannot obtain rsource";
        return;
    }

    if(!capture.storeRTSP(lsource,rsource,Lfilename,Rfilename))
        return;
    
    stStatus m = {ST_SAVE, 0, 0};
    while(_acq)
    {
        L_(ldebug4) << WHERE << "Looping";
        sleep(1);
        ++m.seconds;
        status.emit(m);
    }
    L_(ldebug4) << WHERE << "Finisched storing.";
    _acq_mutex.lock();
    _acq=false;
    _acq_mutex.unlock();
    m.task = ST_IDLE;
    status.emit(m);
}
    
void ST::display() 
{
    //VideoCapture captureL;
    //VideoCapture captureR;
    GstInput capture;
    L_(ldebug2) << WHERE << "Trying to open video source...";

    //std::string connL = std::string("rtsp://")  + (_config.eyes[LEYE])->address + ":8554/test " + (_config.eyes[LEYE])->address + " 8554";
    //std::string connR = std::string("rtsp://")  + (_config.eyes[REYE])->address + ":8554/test " + (_config.eyes[REYE])->address + " 8554";
    //
    //
    //captureL.open(connL);
    //captureR.open(connR);
    
    string lsource;
    string rsource;
    
    
    bool ret = stMgr.getLSource(lsource);
    if (!ret)
    {
        L_(lerror) << WHERE << "Cannot obtain lsource";
        return;            
    }
    ret = stMgr.getRSource(rsource);
    if (!ret)
    {
        L_(lerror) << WHERE << "Cannot obtain rsource";
        return;
    }
    
    L_(ldebug4) << WHERE <<"Opening sources " << lsource << " "<< rsource;


    //declared here so we can free memory
    IplImage * frameL=NULL;
    IplImage * frameR=NULL;

    try
    {
        //captureL.open(lsource);
        //captureR.open(rsource);
        //captureL.open("/home/pie/medusa/1434271888/eye1.h264");
        //captureR.open("/home/pie/medusa/1434271888/eye2.h264");
        //if(!captureL.isOpened() || !captureR.isOpened())
        //    return;
        if(!capture.openRTSP(lsource,rsource))
            return;
        
        Mat imageL, imageR;
        // passed from capture
        
        Mat frameToDisplay;
        int sync;
        bool checkSync;
        
        SyncTimes_t sTimes= {0,0,0};
        checkSync = stMgr.getSyncDescriptors(sTimes);
        
        
        Clock::time_point t0 = Clock::now();
        Clock::time_point newt = Clock::now();   
        
        
        int iFrame(-1);
        int wL,hL,wR,hR;
        
        while(_acq)
        {
            iFrame++;


            if (!capture.grabFrames())
                break;
            if (!capture.retriveFrames(frameL, frameR, sync))
                break;
                
            imageL = Mat(frameL,false);
            imageR = Mat(frameR,false);
            
            if (checkSync) {
                sync = abs(sync);
            } else {
                sync = -1;
            }            
            
            if (iFrame==0)
            {
                if (!capture.getFrameSize(wL,hL,wR,hR))
                    throw runtime_error("Could not parse L or R resolutions!!");
                if (wL != wR || hL != hR )
                    throw runtime_error("Different resolutions for L and R!!");  
            }
            
            // is this really needed?
            if(!imageL.data || !imageR.data)
                break;

    
            if (iFrame > 0)
            {
                newt = Clock::now();
                milliseconds ms = std::chrono::duration_cast<milliseconds>(newt - t0);
                if ((iFrame % 20) == 0)
                {
                    L_(ldebug4) << "Time since 1st frame" << ms.count() << " ms, fps: "<< ((double)iFrame)/(ms.count()/1000.0f) << " sync: " << sync << " ms" << std::endl;
                }
            }
            
            prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
            render->render(frameToDisplay);
        }
        
        if (frameL!=NULL)
            cvReleaseImage(&frameL);
        if (frameR!=NULL)
            cvReleaseImage(&frameR);
        
        _acq_mutex.lock();
        _acq = false;
        _acq_mutex.unlock();
        
        stStatus m = {ST_IDLE,0,0};
        status.emit(m);
        
        return;
    } catch (std::runtime_error &ex)
    {
        L_(lerror) <<"Exception in display: "<<ex.what();
        
        if (frameL!=NULL)
            cvReleaseImage(&frameL);
        if (frameR!=NULL)
            cvReleaseImage(&frameR);
        
        _acq_mutex.lock();
        _acq=false;
        _acq_mutex.unlock();                            
        return;
    } catch (...)
    {
        L_(lerror) <<"Unexpected error in Display";

        if (frameL!=NULL)
            cvReleaseImage(&frameL);
        if (frameR!=NULL)
            cvReleaseImage(&frameR);
        
        _acq_mutex.lock();
        _acq=false;
        _acq_mutex.unlock();
        return;
    }
}

void ST::triangulate(const bool bSendPoints, const bool bShow )
{
    if ((bSendPoints==false) && (bShow==false))
        L_(lwarning) <<"Not showing and not sending...che lo faccio a fare?!";

    IplImage * frameL=NULL;
    IplImage * frameR = NULL;
        
    try
    {

        int searchArea = 31; //must be odd
        int kernelSize = 5;
        int nMarkers;
        int memorySpan;;
        float distThreshold;
        double PointsRearrangeDistanceThreshMeters;
        
        Mat internalParametersMatrixL, distortionCoeffL, internalParametersMatrixR, distortionCoeffR;
        Mat projMatrixL, projMatrixR; Mat R; Mat T; Mat E; Mat F;

        int sync;
        
        int wL = 0, wR = 0, hL = 0, hR = 0; 
        
        int pointCount=0;
        
        bool checkSync;
        
        //this will be removed
        SyncTimes_t sTimes = {0,0,0};
        checkSync = stMgr.getSyncDescriptors(sTimes);
        
        Mat imageL, imageR;
        Mat grayscaleImageL,grayscaleImageR, blurredImageL, blurredImageR, frameToDisplay;


        vector <pair< Vec2d, int> > localMaximaLBefore, localMaximaRBefore;
        vector<Point3f> points3dPreviousFrame;
        
        Point3D_t * send_points;
        
        // init capturing
        GstInput capture;
        
        L_(ldebug2) << WHERE << "Trying to open video source...";
        
        //std::string connL = std::string("rtsp://") + (_config.eyes[LEYE])->address + ":8554/rpi1296x972-42fps " + (_config.eyes[LEYE])->address + " 8554";
        //std::string connR = std::string("rtsp://") + (_config.eyes[REYE])->address + ":8554/rpi1296x972-42fps " + (_config.eyes[REYE])->address + " 8554";
        
        string lsource;
        string rsource;
        bool ret = stMgr.getLSource(lsource);
        if (!ret)
        {
            L_(lerror) << WHERE << "Cannot obtain lsource";
            return;            
        }
        ret = stMgr.getRSource(rsource);
        if (!ret)
        {
            L_(lerror) << WHERE << "Cannot obtain rsource";
            return;
        }

        
        //captureL.open(lsource);
        //captureR.open(rsource);
        if(!capture.openRTSP(lsource,rsource))
            return;

        //if(!captureL.isOpened() ||  !captureR.isOpened() )
        //{
        //    L_(lerror) << WHERE << "Cannot open sources: "<< lsource << " " << rsource << "!";
        //    return;
        //} else {
        //    // Acquire one frame to get images info
        //    captureL >> imageL;
        //    captureR >> imageR;       
        //    if(!imageL.data || !imageR.data)
        //    {
        //        L_(lerror) << WHERE << "Invalid data from sources: "<< lsource << " " << rsource << "!";  
        //        return;
        //    }
        //            
        //}


        vector <pair< Vec2d, float> > localMaximaL, localMaximaR;
        
        Clock::time_point t0 = Clock::now();
        Clock::time_point newt = Clock::now();
  

        int iFrame(-1);
        while(_acq)
        {
            iFrame++;
            //captureL >> imageL;
            //captureR >> imageR;
            if (!capture.grabFrames())
                break;
            if (!capture.retriveFrames(frameL, frameR, sync))
                break;
                
            imageL = Mat(frameL,false);
            imageR = Mat(frameR,false);            
            
            if (checkSync) {
                sync = abs(sync);
            } else {
                sync = -1;
            }    
            
            // is this needed?
            if(!imageL.data || !imageR.data)
                break;
            
            
            // searches parameters for triangulation
            if (iFrame == 0)
            {

                if (!capture.getFrameSize(wL,hL,wR,hR))
                    throw runtime_error("Could not parse L or R resolutions!!");
        
        
                if (wL != wR || hL != hR )
                    throw runtime_error("Different resolutions for L and R!!");
            
                L_(ldebug2) << WHERE << "Frame size: " << wL <<"x"<<hL;
        
        
                bool found = false;
                void * iPML=NULL, * cDL=NULL, * iPMR=NULL, * cDR=NULL, * pML=NULL,
                        * pMR=NULL, * pR=NULL, * pT=NULL,
                        * pE=NULL, * pF=NULL;
                
                found = stMgr.getTriangulationParameters(wL, hL, nMarkers, memorySpan, PointsRearrangeDistanceThreshMeters,
                                                        searchArea,kernelSize,distThreshold,
                                                        iPML, cDL, iPMR, cDR, pML, pMR, pR, pT, pE, pF );
                if (found)
                {
                        // per il maching dei punti di prima
                    points3dPreviousFrame=vector<Point3f>(nMarkers, Point3f(nanf(""), nanf(""), nanf("")));
        
                    internalParametersMatrixL = *((Mat *) iPML);
                    distortionCoeffL = *((Mat *) cDL);
                    internalParametersMatrixR = *((Mat *) iPMR);
                    distortionCoeffR = *((Mat *) cDR);
                    projMatrixL = *((Mat *) pML);
                    projMatrixR = *((Mat *) pMR);
                    R= *((Mat *) pR);
                    T= *((Mat *) pT);
                    E= *((Mat *) pE);
                    F= *((Mat *) pF);            
                }

                if (!found)
                {
                    messages.emit(ST_NO_TRIANGULATION_CONFIG_AVAILABLE);
                    throw runtime_error("Cannot find triangulation parameters!!");                
                }
            }
            

            if (iFrame > 0)
            {
                newt = Clock::now();
                milliseconds ms = std::chrono::duration_cast<milliseconds>(newt - t0);
                if ((iFrame % 20) == 0)
                {
                    L_(ldebug4) << "Time since 1st frame" << ms.count() << " ms, fps: "<< ((double)iFrame)/(ms.count()/1000.0f) << " sync: " << sync << " ms" << std::endl;
                }
            } 

            //throw std::runtime_error("could not acquire frame. Aborting. Video finished ?");
            cvtColor(imageL, grayscaleImageL, CV_BGR2GRAY);

//            GetLocalMaxima(grayscaleImageL, searchArea, 150, kernelSize, &localMaximaL);
            FastGetLocalMaxima(grayscaleImageL, searchArea, 150, kernelSize, nMarkers, &localMaximaL);


            if(localMaximaL.size() == 0)
            {
                L_(ldebug2) << WHERE << " Warning ! No left maximum detected. skipping frame. " << iFrame <<endl;
                std::fill(points3dPreviousFrame.begin(), points3dPreviousFrame.end(), Point3f(nanf(""), nanf(""), nanf("")));

                if (bShow)
                {
                    prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
                    render->render(frameToDisplay);
                }

                continue;
            }
            std::sort(localMaximaL.begin(), localMaximaL.end(), PairCompare);
            
            
            cvtColor(imageR, grayscaleImageR, CV_BGR2GRAY);
//            GetLocalMaxima(grayscaleImageR, searchArea, 150, kernelSize,&localMaximaR);
            FastGetLocalMaxima(grayscaleImageR, searchArea, 150, kernelSize, nMarkers, &localMaximaR);

            if(localMaximaR.size() ==0)
            {
                L_(ldebug2) << WHERE << " Warning ! No right maximum detected. skipping frame. " << iFrame <<endl;
                std::fill(points3dPreviousFrame.begin(), points3dPreviousFrame.end(), Point3f(nanf(""), nanf(""), nanf("")));


                if (bShow)
                {
                    prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
                    render->render(frameToDisplay);
                }
                continue;
            }
            
            std::sort(localMaximaR.begin(), localMaximaR.end(), PairCompare);

            if((int)localMaximaL.size() > nMarkers)
            {
                L_(ldebug2) << WHERE << "WARNING! Found too many local maxima in left image. Check lighting conditions!!! n local maxima found: "<< localMaximaL.size() <<endl;
                if (bShow)
                {
                    prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
                    render->render(frameToDisplay);
                }
                continue;
            }
            if((int)localMaximaR.size() > nMarkers)
            {
                L_(ldebug2) << WHERE << "WARNING! Found too many local maxima in right image. Check lighting conditions!!! n local maxima found: "<< localMaximaR.size() <<endl;
                if (bShow)
                {
                    prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
                    render->render(frameToDisplay);
                }
                continue;
            }

            vector<Vec3f> linesL, linesR;

            vector<vector<double>  > errorsRepr;
            for(int iLeft = 0; iLeft < (int)localMaximaL.size(); iLeft++ )
            {
                vector<double> errorsI;
                vector<Point2f> pt0;

                pt0.push_back(Point2f(localMaximaL[iLeft].first(0),localMaximaL[iLeft].first(1)));
                //TODO se undistorto i punti qui nn va. Serve solo a diminuire repro per ricerca corrispondenze ma non dovrebbe influenzare risulato finale...
                //                cout << "pt0 before and after : "<<pt0[0]<<"  ";
                // undistortPoints(pt0, pt0, internalParametersMatrixL, distortionCoeffL, Mat());
                //                cout <<pt0[0]<<endl;
                computeCorrespondEpilines(pt0, 1, F, linesL);

                for(int iRight(0); iRight<(int)localMaximaR.size(); ++iRight)
                {
                    vector<Point2f> pt1;
                    pt1.push_back(Point2f(localMaximaR[iRight].first(0),localMaximaR[iRight].first(1)));
                    //                    cout << "pt1 before and after : "<<pt1[0]<<"  ";
                    //          undistortPoints(pt1, pt1, internalParametersMatrixR, distortionCoeffR, Mat());// DUBBIO; aggiungere cm ultimo arg matrice interna ?
                    //                        cout <<pt1[0]<<endl;

                    computeCorrespondEpilines(pt1, 2, F, linesR);
                    //                    for (int i(0); i<linesR.size();++i)
                    //                        cout <<i<< linesR[i]<<"  ";

                    double err = fabs(pt0[0].x*linesR[0](0) + pt0[0].y*linesR[0](1) + linesR[0](2)) + fabs(pt1[0].x*linesL[0](0) + pt1[0].y*linesL[0](1) + linesL[0](2));
                    //                      cout << " error ("<<iLeft << ", "<<iRight<<" : " <<err<<endl;
                    errorsI.push_back(err);
                }
                errorsRepr.push_back(errorsI);
            }


            Mat pixels2dL, pixels2dLUndist;
            Mat pixels2dR, pixels2dRUndist;
            
            /////////////////////////
            // ok per check il riordino dei punti 3d perch'e spesso saltano
            // int nCorresp = FindBestMatchesAndConvertToMat(errorsRepr, localMaximaL, localMaximaR, &pixels2dL, &pixels2dR);
            //
            int nCorresp = FindBestMatchesInHistory(iFrame, localMaximaLBefore, localMaximaRBefore, errorsRepr, localMaximaL, localMaximaR, &pixels2dL, &pixels2dR, distThreshold, memorySpan);
            ////////////////////////

            if(nCorresp == 0)
            {
                std::fill(points3dPreviousFrame.begin(), points3dPreviousFrame.end(), Point3f(nanf(""), nanf(""), nanf("")));


                if (bShow)
                {
                    prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
                    render->render(frameToDisplay);
                }
            
                continue;
            }

            //////////////////////////////
            /// triangola punti
            ///
            cv::correctMatches(F, pixels2dL, pixels2dR, pixels2dL, pixels2dR);

            cv::undistortPoints(pixels2dL, pixels2dLUndist, internalParametersMatrixL, distortionCoeffL);
            cv::undistortPoints(pixels2dR, pixels2dRUndist, internalParametersMatrixR, distortionCoeffR);
            if(pixels2dLUndist.type() != CV_64FC2)
                throw runtime_error("opencv sucks");


            Mat points3dHomo;//(1, nMarkers, CV_64FC4);
            cv::triangulatePoints(projMatrixL, projMatrixR, pixels2dLUndist, pixels2dRUndist, points3dHomo);
            if(points3dHomo.type() != CV_64FC1)
                throw runtime_error("opencv sucks ++ ");

            vector<Point3f> points3d;
            //          convertPointsFromHomogeneous(points3dHomo, points3d);
            for (int i(0); i < nCorresp; ++i)
            {
                Point3f point3d(points3dHomo.at<double>(0,i)/points3dHomo.at<double>(3,i), points3dHomo.at<double>(1,i)/points3dHomo.at<double>(3,i), points3dHomo.at<double>(2,i)/points3dHomo.at<double>(3,i));
                points3d.push_back(point3d);
            }
            
            vector<Point3f> reorderedPoints;
            //            std::random_shuffle ( points3d.begin(), points3d.end() );
            vector<int> newIndices;
            bool reorderingOk = ReArrangePoints3dAndFillWithNans(points3dPreviousFrame, points3d, &reorderedPoints, &newIndices, PointsRearrangeDistanceThreshMeters);
            if(!reorderingOk && iFrame >=2)
                L_(ldebug2) << WHERE <<" # ATTENTION! Ordering of points could have changed here at frame "<< iFrame<<" !!!"<<endl;

            
            //calculate reprojection
            Vec3d rvec; Rodrigues(R ,rvec);
            Mat reprojectedPixels2dL, reprojectedPixels2dR;
            vector<double> reproErrorL, reproErrorR ;
            projectPoints(points3d, rvec, T, internalParametersMatrixR, distortionCoeffR, reprojectedPixels2dR);
            projectPoints(points3d, Mat::zeros(3,1, CV_64F), Mat::zeros(3,1, CV_64F), internalParametersMatrixL, distortionCoeffL, reprojectedPixels2dL);

            points3d = reorderedPoints;
            points3dPreviousFrame = reorderedPoints;
            
            
            if (bSendPoints) {
                // send point is a null terminated array
                send_points = new Point3D_t[nCorresp];
                pointCount = 0;
            }
            
            for (int i=0; i<nMarkers; i++)
            {
                if (bSendPoints && (!isnan(points3d[i].x))) {
                    if (pointCount < nCorresp)
                    {
                        send_points[pointCount].x = points3d[i].x;
                        send_points[pointCount].y = points3d[i].y;
                        send_points[pointCount].z = points3d[i].z;
                        send_points[pointCount].id = i;
                        pointCount++;
                    }
                }
                
                // some old stuff
                if(newIndices[i] == -1) {
                    reproErrorL.push_back(nanf(""));
                    reproErrorR.push_back(nanf(""));
                } else {
                    Vec2d pixel2dL = pixels2dL.at<Vec2d>(0,newIndices[i]);
                    Vec2f reproPixelL = reprojectedPixels2dL.at<Vec2f>(0,newIndices[i]);

                    Vec2d pixel2dR = pixels2dR.at<Vec2d>(0,newIndices[i]);
                    Vec2f reproPixelR = reprojectedPixels2dR.at<Vec2f>(0,newIndices[i]);

                    reproErrorL.push_back(sqrt((pixel2dL(0)- reproPixelL(0))*(pixel2dL(0)- reproPixelL(0))  + (pixel2dL(1)- reproPixelL(1))*(pixel2dL(1)- reproPixelL(1))));
                    reproErrorR.push_back(sqrt((pixel2dR(0)- reproPixelR(0))*(pixel2dR(0)- reproPixelR(0))  + (pixel2dR(1)- reproPixelR(1))*(pixel2dR(1)- reproPixelR(1))));
                    
                    // draw points if showinf images or saving frame!

                    if (bShow)
                    {
                        cv::circle(imageL, cv::Point(pixel2dL(0),pixel2dL(1)), 12, get_color(i),-5);
                        cv::circle(imageR, cv::Point(pixel2dR(0),pixel2dR(1)), 12, get_color(i),-5);
                        DrawCross(imageL, cv::Point(reproPixelL(0),reproPixelL(1)), get_color(i));
                        DrawCross(imageR, cv::Point(reproPixelR(0),reproPixelR(1)), get_color(i));
                    }
                }
            }
            if (bSendPoints) {
                points_ready.emit(send_points,nCorresp,iFrame,sync);
            }


            if (bShow){
                hconcat(imageL, imageR, frameToDisplay);
                //for (int i=0; i<nMarkers; i++)
                //{
                //    if(newIndices[i] == -1)
                //        continue;
                //    else
                //    {
                //        std::ostringstream str;
                //        str <<setprecision(2)<< "("<< points3d[i].x<<","<<points3d[i].y<<","<<points3d[i].z <<")"<<endl<<"repro error = "<<reproErrorL[i]<<"   " <<reproErrorR[i];
                //
                //        cv::putText(frameToDisplay, str.str(), cv::Point((reprojectedPixels2dL.at<Vec2f>(0,newIndices[i]))(0),(reprojectedPixels2dL.at<Vec2f>(0,newIndices[i]))(1)), CV_FONT_HERSHEY_PLAIN, 3, CV_RGB(0,255,0));
                //    }
                //}
                
                prepareFrame(imageL, imageR, wL, hL, sync, sTimes , frameToDisplay);
                render->render(frameToDisplay);
                
                
            }

        }
        L_(ldebug2) << WHERE << "Out of triangulation loop!" << endl;
        

    } catch (std::runtime_error &ex)
    {
        L_(lerror) <<"Exception :"<<ex.what();
        
        if (frameL!=NULL)
            cvReleaseImage(&frameL);
        if (frameR!=NULL)
            cvReleaseImage(&frameR);
        
        _acq_mutex.lock();
        _acq=false;
        _acq_mutex.unlock();                            
        return;
    } catch (...)
    {
        L_(lerror) <<"Unexpected exception!";
        if (frameL!=NULL)
            cvReleaseImage(&frameL);
        if (frameR!=NULL)
            cvReleaseImage(&frameR);
        
        _acq_mutex.lock();
        _acq=false;
        _acq_mutex.unlock();
        return;
    }

    if (frameL!=NULL)
        cvReleaseImage(&frameL);
    if (frameR!=NULL)
        cvReleaseImage(&frameR);
    
    _acq_mutex.lock();
    _acq=false;
    _acq_mutex.unlock();  
    
    // Emit end of file signal              
    messages.emit(ST_EOF);
    stStatus m = {ST_IDLE,0,0};
    status.emit(m);
    return;
}
