#include "GstInput.hpp"
#include "log.h"
#define PLAYBACK_DELAY_MS 50
#define PIPELINE_LATENCY_MS 500
#define APP_SINK_MAX_BUFFERS 50

using namespace cv;

static cv::Mutex gst_initializer_mutex;

/*!
 * \brief The gst_initializer class
 * Initializes gstreamer once in the whole process
 */
class gst_initializer
{
public:
    static void init()
    {
        gst_initializer_mutex.lock();
        static gst_initializer init;
        gst_initializer_mutex.unlock();
    }
private:
    gst_initializer()
    {
        gst_init(NULL, NULL);
//        gst_debug_set_active(1);
//        gst_debug_set_colored(1);
//        gst_debug_set_default_threshold(GST_LEVEL_INFO);
    }
};


void handleMessage(GstElement * pipeline);

GstInput::~GstInput()
{
    close();
}

GstInput::GstInput()
{
    global_clock=NULL;
    server1=NULL;
    server2=NULL;
    pipeline1=NULL;
    pipeline2=NULL;
    uridecodebin1=NULL;
    uridecodebin2=NULL;
    color1=NULL;
    color2=NULL;
    sink1=NULL;
    sink2=NULL;
    sink1f=NULL;
    sink2f=NULL;
    
    sample1=NULL;
    sample2=NULL;
    buffer1=NULL;
    buffer2=NULL;
    info1=new GstMapInfo;
    info2=new GstMapInfo;
    
    duration=-1;
    width1=-1;
    height1=-1;
    width2=-1;
    height2=-1;
    fps1=-1;
    fps2=-1;
    caps=NULL;
    
    // for opencv display
    frame1=NULL;
    frame2=NULL;  

}  

void
GstInput::source_created (GstElement * pipe, GstElement * source)
{
  g_object_set (source, "latency", PLAYBACK_DELAY_MS,
      "ntp-time-source", 3, "buffer-mode", 4, "ntp-sync", TRUE, NULL);
}

void GstInput::newPad(GstElement *myelement,
                       GstPad     *pad,
                       gpointer    data)
{
    GstPad *sinkpad;
    GstElement *color = (GstElement *) data;

    sinkpad = gst_element_get_static_pad (color, "sink");
    if (!sinkpad){
        //fprintf(stderr, "Gstreamer: no pad named sink\n");
        return;
    }

    gst_pad_link (pad, sinkpad);
    gst_object_unref (sinkpad);
}

void GstInput::on_pad_added (GstElement *src, GstPad *new_pad, GstElement *rtph264depay) {
  GstPad *sink_pad = gst_element_get_static_pad (rtph264depay, "sink");
  GstPadLinkReturn ret;
  GstCaps *new_pad_caps = NULL;
  GstStructure *new_pad_struct = NULL;
  const gchar *new_pad_type = NULL;

    g_object_set (G_OBJECT(src), "latency", PLAYBACK_DELAY_MS,
      "ntp-time-source", 3, "buffer-mode", 4, "ntp-sync", TRUE, NULL);	

  L_(ldebug4) << "Received new pad '"<< GST_PAD_NAME (new_pad) << "' from '" << GST_ELEMENT_NAME (src) << "':";

  /* Check the new pad's type */
  new_pad_caps = gst_pad_get_current_caps (new_pad);
  new_pad_struct = gst_caps_get_structure (new_pad_caps, 0);
  new_pad_type = gst_structure_get_name (new_pad_struct);
  if (!g_str_has_prefix (new_pad_type, "application/x-rtp")) {
    L_(ldebug4) << "  It has type '" << new_pad_type << "' which is not RTP. Ignoring.";
    /* Unreference the new pad's caps, if we got them */
    if (new_pad_caps != NULL)
        gst_caps_unref (new_pad_caps);
    
    /* Unreference the sink pad */
    gst_object_unref (sink_pad);
    return;
  }

  /* If our converter is already linked, we have nothing to do here */
  if (gst_pad_is_linked (sink_pad)) {
    L_(ldebug4) << "  We are already linked. Ignoring.";
    /* Unreference the new pad's caps, if we got them */
    if (new_pad_caps != NULL)
        gst_caps_unref (new_pad_caps);
    
    /* Unreference the sink pad */
    gst_object_unref (sink_pad);
    return;
  }

  /* Attempt the link */
  ret = gst_pad_link (new_pad, sink_pad);
  if (GST_PAD_LINK_FAILED (ret)) {
    L_(ldebug4) << "  Type is '" << new_pad_type << "' but link failed.\n";
    /* Unreference the new pad's caps, if we got them */
    if (new_pad_caps != NULL)
        gst_caps_unref (new_pad_caps);
    
    /* Unreference the sink pad */
    gst_object_unref (sink_pad);
    return;    
  } else {
    L_(ldebug4) << "  Link succeeded (type '" << new_pad_type << "').\n";
  }

}

bool GstInput::openFile(std::string Laddress, std::string Raddress)
{
    return open(Laddress, Raddress);
}

bool GstInput::openRTSP(std::string Laddress, std::string Raddress)
{
    return open(Laddress, Raddress);
}

bool GstInput::open(std::string Laddress, std::string Raddress)
{
    bool file = false;
    char *uriL = NULL;
    char *uriR = NULL;
    
    gst_initializer::init();
  
    if(!gst_uri_is_valid(Laddress.c_str()) || !gst_uri_is_valid(Raddress.c_str()))
    {
        uriL = realpath(Laddress.c_str(), NULL);
        uriR = realpath(Raddress.c_str(), NULL);
        if(uriL != NULL && uriR != NULL)
        {
            uriL = g_filename_to_uri(uriL, NULL, NULL);
            uriR = g_filename_to_uri(uriR, NULL, NULL);
            if(uriL != NULL && uriR != NULL)
            {
                file = true;
            }
            else
            {
                L_(lerror) << "GStreamer: Error opening file";
                close();
                return false;
            }
        }
    } else {
        file = false;
        uriL = g_strdup(Laddress.c_str());        
        uriR = g_strdup(Raddress.c_str());        
    }
    if (!file)
    {
        global_clock = gst_system_clock_obtain ();
        gst_net_time_provider_new (global_clock, "0.0.0.0", 8554);
        if (global_clock != NULL)
        L_(ldebug2) << ("Clock created!");
        else
        {
        L_(lerror) << ("Could not creaye clock!");
        return false;
        }
    }
    
    L_(ldebug2) << ("Creating pipelines");
    pipeline1 = gst_pipeline_new(NULL);
    pipeline2 = gst_pipeline_new(NULL);
    
    uridecodebin1 = gst_element_factory_make ("uridecodebin", NULL);
    uridecodebin2 = gst_element_factory_make ("uridecodebin", NULL);
    g_object_set (uridecodebin1, "uri", uriL, NULL);
    g_object_set (uridecodebin2, "uri", uriR, NULL);
    
    //sync if not file!
    if (!file)
    {
        g_signal_connect (uridecodebin1, "source-setup", G_CALLBACK (source_created), NULL);
        g_signal_connect (uridecodebin2, "source-setup", G_CALLBACK (source_created), NULL);
    }
    
    //Set latency if we have a stream
    if (!file)
    {
        /* Set this high enough so that it's higher than the minimum latency
        * on all receivers */
        gst_pipeline_set_latency (GST_PIPELINE (pipeline1), PIPELINE_LATENCY_MS * GST_MSECOND);
        gst_pipeline_set_latency (GST_PIPELINE (pipeline2), PIPELINE_LATENCY_MS * GST_MSECOND);
    }
    
    color1 = gst_element_factory_make("autovideoconvert", NULL);
    color2 = gst_element_factory_make("autovideoconvert", NULL);
    sink1 = gst_element_factory_make("appsink", NULL);
    sink2 = gst_element_factory_make("appsink", NULL);
    
    // split pipeline and save to file
    //sink1f = gst_element_factory_make("appsink", NULL);
    //sink2f = gst_element_factory_make("appsink", NULL);
    
    gst_bin_add_many(GST_BIN(pipeline1), uridecodebin1, color1, sink1, NULL);
    gst_bin_add_many(GST_BIN(pipeline2), uridecodebin2, color2, sink2, NULL);
    
    gst_pipeline_use_clock (GST_PIPELINE (pipeline1), global_clock);
    gst_pipeline_use_clock (GST_PIPELINE (pipeline2), global_clock); 
        
    g_signal_connect(uridecodebin1, "pad-added", G_CALLBACK(newPad), color1);
    g_signal_connect(uridecodebin2, "pad-added", G_CALLBACK(newPad), color2);

  //link elements
    if(!gst_element_link(color1, sink1)) {
        L_(lerror) <<  "GStreamer: cannot link color -> sink";
        gst_object_unref(pipeline1);
        pipeline1 = NULL;
        return false;
    }
  //link elements
    if(!gst_element_link(color2, sink2)) {
        L_(lerror) <<   "GStreamer: cannot link color -> sink";
        gst_object_unref(pipeline2);
        pipeline2 = NULL;
        return false;
    }


    //TODO: is 1 single buffer really high enough?
    if (!file)
    {
        gst_app_sink_set_max_buffers (GST_APP_SINK(sink1), APP_SINK_MAX_BUFFERS);
        gst_app_sink_set_max_buffers (GST_APP_SINK(sink2), APP_SINK_MAX_BUFFERS);
        gst_app_sink_set_drop (GST_APP_SINK(sink1), TRUE);
        gst_app_sink_set_drop (GST_APP_SINK(sink2), TRUE);
    } else {
        gst_app_sink_set_max_buffers (GST_APP_SINK(sink1), 1);
        gst_app_sink_set_max_buffers (GST_APP_SINK(sink2), 1);
        gst_app_sink_set_drop (GST_APP_SINK(sink1), FALSE);
        gst_app_sink_set_drop (GST_APP_SINK(sink2), FALSE);        
    }
    //do not emit signals: all calls will be synchronous and blocking
    gst_app_sink_set_emit_signals (GST_APP_SINK(sink1), 0);
    gst_app_sink_set_emit_signals (GST_APP_SINK(sink2), 0);


    caps = gst_caps_from_string("video/x-raw, format=(string){BGR, GRAY8}; video/x-bayer,format=(string){rggb,bggr,grbg,gbrg}");

    gst_app_sink_set_caps(GST_APP_SINK(sink1), caps);
    gst_app_sink_set_caps(GST_APP_SINK(sink2), caps);
    gst_caps_unref(caps);

    return true;
}

bool GstInput::storeRTSP(std::string Laddress, std::string Raddress, const char * Lfilename, const char * Rfilename)
{    
    gst_initializer::init();
  
    if(!gst_uri_is_valid(Laddress.c_str()) || !gst_uri_is_valid(Raddress.c_str()))
    {
        return false;
    }
    
    L_(ldebug4) << "Storing data to " <<  Lfilename << " " << Rfilename;
    
    global_clock = gst_system_clock_obtain ();
    gst_net_time_provider_new (global_clock, "0.0.0.0", 8554);
    if (global_clock != NULL)
    L_(ldebug2) << ("Clock created!");
    else
    {
    L_(lerror) << ("Could not creaye clock!");
    return false;
    }
    
    L_(ldebug4) << ("Creating pipelines");
    pipeline1 = gst_pipeline_new(NULL);
    pipeline2 = gst_pipeline_new(NULL);

    if(!pipeline1 || !pipeline2)
        return false;
    
    uridecodebin1 = gst_element_factory_make ("rtspsrc", NULL);
    uridecodebin2 = gst_element_factory_make ("rtspsrc", NULL);
    g_object_set (uridecodebin1, "location", Laddress.c_str(), NULL);
    g_object_set (uridecodebin2, "location", Raddress.c_str(), NULL);
    
    
    //sync    
    g_object_set (uridecodebin1, "latency", PLAYBACK_DELAY_MS,
      "ntp-time-source", 3, "buffer-mode", 4, "ntp-sync", TRUE, NULL);
    g_object_set (uridecodebin2, "latency", PLAYBACK_DELAY_MS,
      "ntp-time-source", 3, "buffer-mode", 4, "ntp-sync", TRUE, NULL);
    
    //Set latency if we have a stream
        /* Set this high enough so that it's higher than the minimum latency
        * on all receivers */
    gst_pipeline_set_latency (GST_PIPELINE (pipeline1), PIPELINE_LATENCY_MS * GST_MSECOND);
    gst_pipeline_set_latency (GST_PIPELINE (pipeline2), PIPELINE_LATENCY_MS * GST_MSECOND);
    
    GstElement * rtph264depay1 = gst_element_factory_make ("rtph264depay", NULL);
    GstElement * rtph264depay2 = gst_element_factory_make ("rtph264depay", NULL);
    
    GstElement * mpegtsmux1 = gst_element_factory_make ("mpegtsmux", NULL);
    GstElement * mpegtsmux2 = gst_element_factory_make ("mpegtsmux", NULL);

    GstElement * filesink1 = gst_element_factory_make ("filesink", NULL);
    GstElement * filesink2 = gst_element_factory_make ("filesink", NULL);
    
    g_assert (filesink1);
    g_assert (filesink2);
        
    g_object_set (G_OBJECT (filesink1), "location", Lfilename, NULL);
    g_object_set (G_OBJECT (filesink2), "location", Rfilename, NULL);  
    
    gst_bin_add_many(GST_BIN(pipeline1), uridecodebin1, rtph264depay1, mpegtsmux1, filesink1, NULL);
    gst_bin_add_many(GST_BIN(pipeline2), uridecodebin2, rtph264depay2, mpegtsmux2, filesink2, NULL);
    
    gst_pipeline_use_clock (GST_PIPELINE (pipeline1), global_clock);
    gst_pipeline_use_clock (GST_PIPELINE (pipeline2), global_clock); 
        
    //g_signal_connect(uridecodebin1, "pad-added", G_CALLBACK(newPad), color1);
    //g_signal_connect(uridecodebin2, "pad-added", G_CALLBACK(newPad), color2);
    
    
    if(!gst_element_link_many (rtph264depay1, mpegtsmux1, filesink1, NULL))
    {
        L_(lerror) << ( "Could not link pipeline 1");
        gst_object_unref(pipeline1);
        pipeline1 = NULL;
        return false;
    }
    if(!gst_element_link_many (rtph264depay2, mpegtsmux2, filesink2, NULL))
    {
        L_(lerror) << ( "Could not link pipeline 2");
        gst_object_unref(pipeline1);
        pipeline1 = NULL;
        return false;
    }
    
    g_signal_connect(uridecodebin1, "pad-added", G_CALLBACK(on_pad_added), rtph264depay1);
    g_signal_connect(uridecodebin2, "pad-added", G_CALLBACK(on_pad_added), rtph264depay2);
    
    // start the pipeline if it was not in playing state yet
    if(!this->isPipelinePlaying())
    {
        L_(ldebug4) << ("Starting pipelines");
        this->startPipeline();
    }
    handleMessage(pipeline1);
    handleMessage(pipeline2);
    return true;
}

//void saveRTSPFrame()
//{
//  gst_bin_iterate (GST_BIN (pipeline1)));
//  gst_bin_iterate (GST_BIN (pipeline2)));
//}



/*!
 * \brief GstInput::grabFrame
 * \return
 * Grabs a sample from the pipeline, awaiting consumation by retreiveFrame.
 * The pipeline is started if it was not running yet
 */
bool GstInput::grabFrames()
{
    if(!pipeline1 || !pipeline2)
        return false;

    // start the pipeline if it was not in playing state yet
    if(!this->isPipelinePlaying())
    {
        this->startPipeline();
    }

    // bail out if EOS
    if(gst_app_sink_is_eos(GST_APP_SINK(sink1)))
        return false;
    if(gst_app_sink_is_eos(GST_APP_SINK(sink2)))
        return false;

    // get frame
    if(sample1)
        gst_sample_unref(sample1);
    if(sample2)
        gst_sample_unref(sample2);

    sample1 = gst_app_sink_pull_sample(GST_APP_SINK(sink1));
    sample2 = gst_app_sink_pull_sample(GST_APP_SINK(sink2));

    if(!sample1 || !sample2)
        return false;

    buffer1 = gst_sample_get_buffer(sample1);
    buffer2 = gst_sample_get_buffer(sample2);
    if(!buffer1 || !buffer2)
        return false;

    return true;
}

bool GstInput::retriveFrames(IplImage *  & OutFrame1, IplImage *  & OutFrame2, int & sync)
{

    if(!buffer1 || !buffer2)
        return false;
    
    pts1 = buffer1->pts;
    pts2 = buffer2->pts;
    
    //construct a frame header if we did not have any yet
    if(!frame1)
    {
        L_(ldebug4) << "Creating frame 1\n";
        //some stuff for timing
        prev_pts1=pts1;
        
        // get pipeline curtime
        curtime1 = gst_element_get_base_time(pipeline1);
    
        GstCaps* buffer_caps = gst_sample_get_caps(sample1);
    
        // bail out in no caps
        assert(gst_caps_get_size(buffer_caps) == 1);
        GstStructure* structure = gst_caps_get_structure(buffer_caps, 0);
    
        // bail out if width or height are 0
        if(!gst_structure_get_int(structure, "width", &width1) ||
                !gst_structure_get_int(structure, "height", &height1))
        {
            gst_caps_unref(buffer_caps);
            return false;
        }
    
        int depth = 3;
    
        depth = 0;
        const gchar* name = gst_structure_get_name(structure);
        const gchar* format = gst_structure_get_string(structure, "format");
    
        if (!name || !format)
            return false;
    
        // we support 3 types of data:
        //     video/x-raw, format=BGR   -> 8bit, 3 channels
        //     video/x-raw, format=GRAY8 -> 8bit, 1 channel
        //     video/x-bayer             -> 8bit, 1 channel
        // bayer data is never decoded, the user is responsible for that
        // everything is 8 bit, so we just test the caps for bit depth
    
        if (strcasecmp(name, "video/x-raw") == 0)
        {
            if (strcasecmp(format, "BGR") == 0) {
                depth = 3;
            }
            else if(strcasecmp(format, "GRAY8") == 0){
                depth = 1;
            }
        }
        else if (strcasecmp(name, "video/x-bayer") == 0)
        {
            depth = 1;
        }
    
        if (depth > 0) {
            frame1 = cvCreateImageHeader(cvSize(width1, height1), IPL_DEPTH_8U, depth);
        } else {
            gst_caps_unref(buffer_caps);
            return false;
        }
    
        gst_caps_unref(buffer_caps);
    }
    
    
    //construct a frame header if we did not have any yet
    if(!frame2)
    {
        L_(ldebug4) << "Creating frame 2\n";
        prev_pts2=pts2;
        
        // get pipeline curtime
        curtime2 = gst_element_get_base_time(pipeline2);
        
        GstCaps* buffer_caps = gst_sample_get_caps(sample2);
    
        // bail out in no caps
        assert(gst_caps_get_size(buffer_caps) == 1);
        GstStructure* structure = gst_caps_get_structure(buffer_caps, 0);
    
        // bail out if width or height are 0
        if(!gst_structure_get_int(structure, "width", &width2) ||
                !gst_structure_get_int(structure, "height", &height2))
        {
            gst_caps_unref(buffer_caps);
            return false;
        }
    
        int depth = 3;
    
        depth = 0;
        const gchar* name = gst_structure_get_name(structure);
        const gchar* format = gst_structure_get_string(structure, "format");
    
        if (!name || !format)
            return false;
    
        // we support 3 types of data:
        //     video/x-raw, format=BGR   -> 8bit, 3 channels
        //     video/x-raw, format=GRAY8 -> 8bit, 1 channel
        //     video/x-bayer             -> 8bit, 1 channel
        // bayer data is never decoded, the user is responsible for that
        // everything is 8 bit, so we just test the caps for bit depth
    
        if (strcasecmp(name, "video/x-raw") == 0)
        {
            if (strcasecmp(format, "BGR") == 0) {
                depth = 3;
            }
            else if(strcasecmp(format, "GRAY8") == 0){
                depth = 1;
            }
        }
        else if (strcasecmp(name, "video/x-bayer") == 0)
        {
            depth = 1;
        }
    
        if (depth > 0) {
            frame2 = cvCreateImageHeader(cvSize(width2, height2), IPL_DEPTH_8U, depth);
        } else {
            gst_caps_unref(buffer_caps);
            return false;
        }
    
        gst_caps_unref(buffer_caps);
    }
    
    
    // gstreamer expects us to handle the memory at this point
    // so we can just wrap the raw buffer and be done with it
    
    // the data ptr in GstMapInfo is only valid throughout the mapifo objects life.
    // TODO: check if reusing the mapinfo object is ok.
    gboolean success = gst_buffer_map(buffer1,info1, (GstMapFlags)GST_MAP_READ);
    if (!success){
      //something weird went wrong here. abort. abort.
      //fprintf(stderr,"GStreamer: unable to map buffer");
      return false;
    }
    frame1->imageData = (char*)info1->data;
    gst_buffer_unmap(buffer1,info1);
    
    success = gst_buffer_map(buffer2,info2, (GstMapFlags)GST_MAP_READ);
    if (!success){
      //something weird went wrong here. abort. abort.
      //fprintf(stderr,"GStreamer: unable to map buffer");
      return false;
    }
    frame2->imageData = (char*)info2->data;
    gst_buffer_unmap(buffer2,info2);
    
    //clock_gettime(CLOCK_MONOTONIC, &lasttime);
    //g_print ("Frames %d sync: %lf ms start_time %lf s FPS 1: ~%lf FPS 2: ~%lf FPS prog: ~%lf \n", frames , ((double)((int)(pts2-pts1))/1000000.), ((double)((int)(curtime2-curtime1))/1000000000.), 1./(((double)(pts1-prev_pts1))/1000000000.), 1./(((double)(pts2-prev_pts2))/1000000000.), 
    //    ((double)frames)/(((double)lasttime.tv_sec + 1.0e-9*lasttime.tv_nsec) - ((double)starttime.tv_sec + 1.0e-9*starttime.tv_nsec)) );
    
    sync = ((double)((int)(pts2-pts1))/1000000.);
    prev_pts1=pts1;
    prev_pts2=pts2;
    
    fps1 = 1./(((double)(pts1-prev_pts1))/1000000000.);
    fps2 = 1./(((double)(pts2-prev_pts2))/1000000000.);
    
    OutFrame1 = frame1;
    OutFrame2 = frame2;
    
    return true;
}

bool GstInput::getFrameSize(int & w1, int & h1, int & w2, int & h2)
{
    w1 = width1;
    h1 = height1;
    w2 = width2;
    h2 = height2;
    if (w1>0 && h1>0 && w2>0 && h2>0)
        return true;
    else
        return false;
}

/*!
 * \brief GstInput::isPipelinePlaying
 * \return if the pipeline is currently playing.
 */
bool GstInput::isPipelinePlaying()
{
    GstState current1, pending1;
    GstState current2, pending2;
    
    GstClockTime timeout = 5*GST_SECOND;
    if(!GST_IS_ELEMENT(pipeline1) || !GST_IS_ELEMENT(pipeline2)) {
        return false;
    }

    GstStateChangeReturn ret = gst_element_get_state(GST_ELEMENT(pipeline1),&current1, &pending1, timeout);
    if (!ret){
        //fprintf(stderr, "GStreamer: unable to query pipeline state\n");
        return false;
    }
    ret = gst_element_get_state(GST_ELEMENT(pipeline2),&current2, &pending2, timeout);
    if (!ret){
        //fprintf(stderr, "GStreamer: unable to query pipeline state\n");
        return false;
    }

    return (current1 == GST_STATE_PLAYING && current2 == GST_STATE_PLAYING);
}

/*!
 * \brief GstInput::startPipeline
 * Start the pipeline by setting it to the playing state
 */
void GstInput::startPipeline()
{
    //fprintf(stderr, "relinked, pausing\n");
    bool error = false;
    GstStateChangeReturn status1 = gst_element_set_state(GST_ELEMENT(pipeline1), GST_STATE_PLAYING);
    GstStateChangeReturn status2 = gst_element_set_state(GST_ELEMENT(pipeline2), GST_STATE_PLAYING);
    if (status1 == GST_STATE_CHANGE_ASYNC || status2 == GST_STATE_CHANGE_ASYNC)
    {
        // wait for status update
        GstState st1;
        GstState st2;
        status1 = gst_element_get_state(pipeline1, &st1, &st2, GST_CLOCK_TIME_NONE);
        status2 = gst_element_get_state(pipeline2, &st1, &st2, GST_CLOCK_TIME_NONE);
    }
    if (status1 == GST_STATE_CHANGE_FAILURE) 
    {
        handleMessage(pipeline1);
        gst_object_unref(pipeline1);
        pipeline1 = NULL;
        L_(lerror) << "GStreamer: unable to start pipeline 1";
        error=true;
    }
    if (status2 == GST_STATE_CHANGE_FAILURE)
    {
        handleMessage(pipeline2);
        gst_object_unref(pipeline2);
        pipeline2 = NULL;
        L_(lerror) << "GStreamer: unable to start pipeline 2";        
        error=true;
    }
    if (error)
    {
        L_(lerror) << "GStreamer: could not start. Stopping!";
        stopPipeline();
        handleMessage(pipeline1);
        handleMessage(pipeline2);
        return;
    }
    L_(ldebug4) << ("Pipelines started!");
    handleMessage(pipeline1);
    handleMessage(pipeline2);
    //printf("state now playing\n");
}


/*!
 * \brief GstInput::stopPipeline
 * Stop the pipeline by setting it to NULL
 */
void GstInput::stopPipeline()
{
    //bool error = false;
    //fprintf(stderr, "restarting pipeline, going to ready\n");
    if (pipeline1 != NULL)
    {
        if(gst_element_set_state(GST_ELEMENT(pipeline1), GST_STATE_NULL) ==
                GST_STATE_CHANGE_FAILURE) {
            L_(lerror) << "GStreamer: unable to stop pipeline1\n";
            gst_object_unref(pipeline1);
            pipeline1 = NULL;
            //error = true;
        }
    }
    if (pipeline2 != NULL)
    {
        if(gst_element_set_state(GST_ELEMENT(pipeline2), GST_STATE_NULL) ==
                GST_STATE_CHANGE_FAILURE) {
            L_(lerror) << "GStreamer: unable to stop pipeline2\n";
            gst_object_unref(pipeline2);
            pipeline2 = NULL;
            //error = true;
        } 
    }   
}

/*!
 * \brief GstInput::restartPipeline
 * Restart the pipeline
 */
void GstInput::restartPipeline()
{
    handleMessage(pipeline1);
    handleMessage(pipeline2);

    this->stopPipeline();
    this->startPipeline();
}

/*!
 * \brief GstInput::close
 * Closes the pipeline and destroys all instances
 */
void GstInput::close()
{
    L_(ldebug2) << ("Closing GstInput!");

    // clean last fram
    if(buffer1)
        gst_buffer_unref(buffer1);
    if(buffer2)
        gst_buffer_unref(buffer2);    
    
    if (isPipelinePlaying())
        this->stopPipeline();

    if(pipeline1) {
        gst_element_set_state(GST_ELEMENT(pipeline1), GST_STATE_NULL);
        gst_object_unref(GST_OBJECT(pipeline1));
        pipeline1 = NULL;
    }
    if(pipeline2) {
        gst_element_set_state(GST_ELEMENT(pipeline2), GST_STATE_NULL);
        gst_object_unref(GST_OBJECT(pipeline2));
        pipeline2 = NULL;
    }
    
    // is this needed? Or the pipeline destroys the clock?
    L_(ldebug2) << ("Deleting global clock");
    
    if(global_clock!=NULL)
        g_object_unref(global_clock);

    duration = -1;
    width1 = -1;
    height1 = -1;
    width2 = -1;
    height2 = -1;
    fps1 = -1;
    fps2 = -1;
}


/*!
 * \brief handleMessage
 * Handles gstreamer bus messages. Mainly for debugging purposes and ensuring clean shutdown on error
 */
void handleMessage(GstElement * pipeline)
{

    GError *err = NULL;
    gchar *debug = NULL;
    GstBus* bus = NULL;
    GstStreamStatusType tp;
    GstElement * elem = NULL;
    GstMessage* msg  = NULL;

    bus = gst_element_get_bus(pipeline);

    while(gst_bus_have_pending(bus)) {
        msg = gst_bus_pop(bus);

        //printf("Got %s message\n", GST_MESSAGE_TYPE_NAME(msg));

        if(gst_is_missing_plugin_message(msg))
        {
            L_(lerror) << "GStreamer: your gstreamer installation is missing a required plugin\n";
        }
        else
        {
            switch (GST_MESSAGE_TYPE (msg)) {
            case GST_MESSAGE_STATE_CHANGED:
                GstState oldstate, newstate, pendstate;
                gst_message_parse_state_changed(msg, &oldstate, &newstate, &pendstate);
                //fprintf(stderr, "state changed from %s to %s (pending: %s)\n", gst_element_state_get_name(oldstate),
                //                gst_element_state_get_name(newstate), gst_element_state_get_name(pendstate));
                break;
            case GST_MESSAGE_ERROR:
                gst_message_parse_error(msg, &err, &debug);
                L_(lerror) << "GStreamer Plugin: Embedded video playback halted; module " << 
                                gst_element_get_name(GST_MESSAGE_SRC (msg)) << " reported: "<< err->message;

                g_error_free(err);
                g_free(debug);

                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_NULL);
                break;
            case GST_MESSAGE_EOS:
                //fprintf(stderr, "reached the end of the stream.");
                break;
            case GST_MESSAGE_STREAM_STATUS:
                gst_message_parse_stream_status(msg,&tp,&elem);
                //fprintf(stderr, "stream status: elem %s, %i\n", GST_ELEMENT_NAME(elem), tp);
                break;
            default:
                //fprintf(stderr, "unhandled message %s\n",GST_MESSAGE_TYPE_NAME(msg));
                break;
            }
        }
        gst_message_unref(msg);
    }

    gst_object_unref(GST_OBJECT(bus));
}
