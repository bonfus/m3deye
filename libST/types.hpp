
#ifndef TYPES_H_
#define TYPES_H_

#include <inttypes.h>

#define LEYE 0
#define REYE 1

enum sourceType
{
    ST_NONE,
    ST_EYES,
    ST_FILES
};

enum stTask {
    ST_IDLE,
    ST_SHOW,
    ST_SAVE,
    ST_LIVE
};

struct stStatus
{
    stTask task;
    int frames;
    int seconds;
};

enum stMessages
{
    ST_NO_SOURCE,           // No sources available. Action cannot be performed
    ST_NOT_IDLE,            // The system is not in idle state, action cannot be performesd
    ST_CONNECTION_PROBLEM,  // 
    ST_NO_TRIANGULATION_CONFIG_AVAILABLE,
    ST_FATAL_ERROR,
    ST_EOF
};



typedef struct EyeData
{
    char *name;
    char *type;
    char *domain;
    char *host_name;
    char *address;
    char *mountpoint;
    uint16_t port;
} EyeData_t;

typedef struct FileData
{
    char *name;
    char *type;
    char *path;
} FileData_t;

typedef struct Source
{
    char * name;
    unsigned int index;
    sourceType type;
} Source_t;


//typedef struct Point3D
//{
//    float x;
//    float y;
//    float z;
//    int id;
//} Point3D_t;            // OLD DEFINITION

#ifndef POINTS3D_TYPE
#define POINTS3D_TYPE
//typedef struct Point3D {
//union {
//  float data[3];
//  struct {float x; float y; float z;};
//};
//int id;
//} Point3D_t;

typedef struct Point3D
{
    float x;
    float y;
    float z;
    int id;
} Point3D_t;            // OLD DEFINITION

#endif

typedef struct TriangulationParameters
{
    int frameWidth;
    int frameHeight;
    int searchArea; // = 31; //must be odd
    int kernelSize; //= 5;
    void * internalParametersMatrixL;
    void * distortionCoeffL;
    void * internalParametersMatrixR;
    void * distortionCoeffR;
    void * projMatrixL;
    void * projMatrixR;
    void * R;
    void * T;
    void * E;
    void * F;
    float distThreshold;//=30.0;
} TriParams_t;

typedef struct SyncTimes
{
    int badSync;
    int medSync;
    int goodSync;
} SyncTimes_t;

typedef struct Config
{
    int nMarkers;
    int memorySpan;//=10;
    double PointsRearrangeDistanceThreshMeters;
    SyncTimes_t syncTimes;
    TriParams_t * TriParams;
    int nTriParams;
} Config_t;

typedef struct Status
{
    bool isPlaying;
    bool haveEyes;
    bool canConnect;
    bool haveSettings;
} Status_t;

enum CmdTypes
{
    SET_BRIGHTNESS,
    SET_CONTRAST,
    SET_SATURATION,
    SET_SHUTTERSPEED,
    SAVE_SETTINGS
};

typedef struct command
{
    CmdTypes cmd;
    int32_t value;
    bool increment;
} command_t;

#endif
