#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
#include<netdb.h> //hostent

typedef enum  protocol {
    EC_SAVE_SETTINGS,
    EC_SET_SHUTTERSPEED,
    EC_SET_BRIGHTNESS,
    EC_SET_CONTRAST,
    EC_SET_SATURATION,
    EC_ACTION_INCREMENT,
    EC_ACTION_SET,
    EC_QUIT
} protocol;



static inline void SerializeInt32(char (&buf)[4], int32_t val)
{
    uint32_t uval = val;
    buf[0] = uval;
    buf[1] = uval >> 8;
    buf[2] = uval >> 16;
    buf[3] = uval >> 24;
}

static inline int32_t ParseInt32(const char (&buf)[4])
{
    // This prevents buf[i] from being promoted to a signed int.
    uint32_t u0 = buf[0], u1 = buf[1], u2 = buf[2], u3 = buf[3];
    uint32_t uval = u0 | (u1 << 8) | (u2 << 16) | (u3 << 24);
    return uval;
}


/**
    TCP Client class
*/
class tcp_client
{
private:
    int sock;
    struct sockaddr_in server;

public:
    tcp_client();
    bool conn(const char*,const char*, int);
    bool clse();
    bool send_command(command_t cmd);
};

