#include "avahi.hpp"
#include "log.h"

#define ST_AVAHI_LOOKFOR "_rtspctrl._tcp"
//#define ST_AVAHI_LOOKFOR "_ssh._tcp"

void Avahi::resolve_callback(
    AvahiServiceResolver *r,
    AVAHI_GCC_UNUSED AvahiIfIndex interface,
    AVAHI_GCC_UNUSED AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {

    AvahiData_t * data = (AvahiData_t*) userdata;
    assert(r);

    /* Called whenever a service has been resolved successfully or timed out */

    switch (event) {
        case AVAHI_RESOLVER_FAILURE:
            fprintf(stderr, "(Resolver) Failed to resolve service '%s' of type '%s' in domain '%s': %s\n", name, type, domain, avahi_strerror(avahi_client_errno(avahi_service_resolver_get_client(r))));
            break;

        case AVAHI_RESOLVER_FOUND: {
            char a[AVAHI_ADDRESS_STR_MAX], *t;
            
            fprintf(stderr, "Service '%s' of type '%s' in domain '%s':\n", name, type, domain);
            
            
            data->services = (EyeData_t*) realloc (data->services, (data->nServices+1) * sizeof(EyeData_t));
            
            data->services[data->nServices].name = strdup(name);
            data->services[data->nServices].port = port;
            data->services[data->nServices].domain = strdup(domain);
            data->services[data->nServices].host_name = strdup(host_name);
            data->services[data->nServices].type = strdup(type);
            data->services[data->nServices].mountpoint = NULL;
            
            
            avahi_address_snprint(a, sizeof(a), address);
            data->services[data->nServices].address = strdup(a);
            
            data->nServices = data->nServices + 1;
            
            
            t = avahi_string_list_to_string(txt);
            fprintf(stderr,
                    "\t%s:%u (%s)\n"
                    "\tTXT=%s\n"
                    "\tcookie is %u\n"
                    "\tis_local: %i\n"
                    "\tour_own: %i\n"
                    "\twide_area: %i\n"
                    "\tmulticast: %i\n"
                    "\tcached: %i\n",
                    host_name, port, a,
                    t,
                    avahi_string_list_get_service_cookie(txt),
                    !!(flags & AVAHI_LOOKUP_RESULT_LOCAL),
                    !!(flags & AVAHI_LOOKUP_RESULT_OUR_OWN),
                    !!(flags & AVAHI_LOOKUP_RESULT_WIDE_AREA),
                    !!(flags & AVAHI_LOOKUP_RESULT_MULTICAST),
                    !!(flags & AVAHI_LOOKUP_RESULT_CACHED));
                
            avahi_free(t);
        }
    }

    avahi_service_resolver_free(r);
    
}

void Avahi::browse_callback(
    AvahiServiceBrowser *b,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiBrowserEvent event,
    const char *name,
    const char *type,
    const char *domain,
    AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,
    void* userdata) {
    
    AvahiData_t * data = (AvahiData_t*) userdata;
    AvahiClient *c = data->client;
    assert(b);

    /* Called whenever a new services becomes available on the LAN or is removed from the LAN */

    switch (event) {
        case AVAHI_BROWSER_FAILURE:
            
            fprintf(stderr, "(Browser) %s\n", avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            avahi_simple_poll_quit(data->simple_poll);
            return;

        case AVAHI_BROWSER_NEW:
            L_(ldebug2) << WHERE << "(Browser) NEW: service '" << name << "' of type '" << type << "' in domain '" << domain << "'";

            /* We ignore the returned resolver object. In the callback
               function we free it. If the server is terminated before
               the callback function is called the server will free
               the resolver for us. */

            if (!(avahi_service_resolver_new(c, interface, protocol, name, type, domain, AVAHI_PROTO_UNSPEC, (AvahiLookupFlags) 0, resolve_callback, data)))
                L_(lerror) << WHERE << "Failed to resolve service '" << name << "': " << avahi_strerror(avahi_client_errno(c));
            //avahi_simple_poll_quit(data->simple_poll);
            break;

        case AVAHI_BROWSER_REMOVE:
            L_(ldebug2) << WHERE << "(Browser) REMOVE: service '" << name << "' of type '" << type << "' in domain '" << domain << "'";
            // still not implemented
            break;

        case AVAHI_BROWSER_ALL_FOR_NOW:
        case AVAHI_BROWSER_CACHE_EXHAUSTED:
            L_(ldebug2) << WHERE << "(Browser) " << (event == AVAHI_BROWSER_CACHE_EXHAUSTED ? "CACHE_EXHAUSTED" : "ALL_FOR_NOW");
            break;
    }
}

void Avahi::client_callback(AvahiClient *c, AvahiClientState state, void * userdata) {
    assert(c);
    AvahiData_t * data = (AvahiData_t*) userdata;
    /* Called whenever the client or server state changes */
    
    if (state == AVAHI_CLIENT_FAILURE) {
        L_(lerror) << WHERE << "Server connection failre: " << avahi_strerror(avahi_client_errno(c));
        avahi_simple_poll_quit(data->simple_poll);
    }
}

Avahi::Avahi() {
        
    _data = new AvahiData_t;
    _data->client = NULL;
    _data->sb = NULL;
    _data->simple_poll = NULL;
    _data->nServices=0;
    _data->services=NULL;

}

Avahi::~Avahi()
{
    Clean();
}
bool Avahi::Run(int timeout)
{   
    //avahi_simple_poll_loop(_data->simple_poll);
    
    // Clean up variables and set NULL
    L_(ldebug2) << WHERE << "Avahi cleaning...";
    Clean();
    
    
    L_(ldebug2) << WHERE << "Avahi preparing...";
    if (Prepare())
    {
        L_(ldebug2) << WHERE << "Avahi running...";
    } else {
        L_(lerror) << WHERE << "Problem preparing Avahi...";
        return false;
    }
    
    bool withError = false;

	while (timeout > 0)
	{
		timeout -= 100;
        const int ret = avahi_simple_poll_iterate(_data->simple_poll, 100);

        if(ret == 1) {
            withError = true;
            L_(lerror) << WHERE << "Error during poll iteration";
            break;
        }
        else if(ret < 0)
        {
            L_(lerror) << WHERE << "Error during event processing: error code " << ret;
        }
	}
    return !(withError);

}

bool Avahi::Prepare()
{
    int error;
    
    L_(ldebug4) << WHERE << "Prepare.";

    /* Allocate main loop object */
    if (!(_data->simple_poll = avahi_simple_poll_new())) {
        L_(lerror) << "Failed to create simple poll object.";
        return false;
    }

    /* Allocate a new client */
    _data->client = avahi_client_new(avahi_simple_poll_get(_data->simple_poll), (AvahiClientFlags) 0, client_callback, _data, &error);

    /* Check wether creating the client object succeeded */
    if (!_data->client) {
        L_(lerror) << "Failed to create client: " << avahi_strerror(error);
        return false;
    }
    
    /* Create the service browser */
    if (!(_data->sb = avahi_service_browser_new(_data->client, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, ST_AVAHI_LOOKFOR, NULL, (AvahiLookupFlags) 0, browse_callback, _data))) {
        L_(lerror) << "Failed to create service browser: " << avahi_strerror(avahi_client_errno(_data->client));
        return false;
    }
    return true;
}


EyeData_t * Avahi::getResults(int idx)
{
    L_(ldebug4) << WHERE << "avahi getResults.";
    
    if (idx < _data->nServices)
        return &(_data->services[idx]);
    else
        return NULL;
}


void Avahi::Clean() 
{
    
    L_(ldebug4) << WHERE << "Clean.";
    
    /* Cleanup things */
    if (_data->sb)
        avahi_service_browser_free(_data->sb);
    
    if (_data->client)
        avahi_client_free(_data->client);

    if (_data->simple_poll)
        avahi_simple_poll_free(_data->simple_poll);
    
    if ( _data->nServices > 0)
        free(_data->services);
        
    _data->client = NULL;
    _data->sb = NULL;
    _data->simple_poll = NULL;
    _data->nServices=0;
    _data->services=NULL;

}
