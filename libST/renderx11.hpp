#ifdef HAVE_X11

#ifndef RENDER_X11_H_
#define RENDER_X11_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>
#include <X11/extensions/XShm.h>

extern int 	XShmQueryExtension(Display*);
extern int 	XShmGetEventBase(Display*);
extern XvImage  *XvShmCreateImage(Display*, XvPortID, int, char*, int, int, XShmSegmentInfo*);



// ---NEWROBBA

#ifndef __QNX__
#ifndef MINIX_SUPPORT
#include <sys/ipc.h>
#include <sys/shm.h>
#endif
#else
#include "qnxipc.h"
#include "qnxshm.h"
#endif
#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>

#include "render.hpp"


/* 

/* A FOURCC consists of four bytes that can be interpreted either as
    a four-character string or as a four-byte integer. */
typedef union {
    int id;
    char label[4];
} fourcc_t;

#ifdef WORDS_BIGENDIAN

/* YUV 4:2:2 formats: */
#define FOURCC_UYVY 0x55595659
#define FOURCC_YUY2 0x59555932
#define FOURCC_YVYU 0x59565955
/* YUV 4:1:1 formats: */
#define FOURCC_YV12 0x59563132
#define FOURCC_I420 0x49343230
#define FOURCC_IYUV 0x49595556

#else

/* YUV 4:2:2 formats: */
#define FOURCC_UYVY 0x59565955
#define FOURCC_YUY2 0x32595559
#define FOURCC_YVYU 0x55595659
/* YUV 4:1:1 formats: */
#define FOURCC_YV12 0x32315659
#define FOURCC_I420 0x30323449
#define FOURCC_IYUV 0x56555949

#endif


int find_yuv_port(Display* display, XvPortID* port, fourcc_t* format);

XvImage* create_yuv_image(Display* display, XvPortID port, fourcc_t format,
			  int width, int height, XShmSegmentInfo* shminfo);

void destroy_yuv_image(Display* display, XvImage* image,
		       XShmSegmentInfo* shminfo);

void display_yuv_image(Display* display, XvPortID port, Drawable d, GC gc,
		       XvImage* image,
		       XShmSegmentInfo* shminfo,
		       int src_x, int src_y,
		       unsigned int src_w, unsigned int src_h,
		       unsigned int dest_w, unsigned int dest_h,
		       double aspect_ratio);



class X11VideoRender : public VideoRender
{
public:
    ~X11VideoRender();
    void render(cv::Mat & frame);
    void setDrawable(int d);
    void setSourceResolution(int w, int h);
    void cleanup();
private:
    XvImage * _yuv_image;
    Display * _dpy;
    Drawable _drawable;
    GC _gc;
    XShmSegmentInfo	yuv_shminfo;
    XvPortID	_xv_port;    
};


#endif //RENDER_X11_H_

#endif // HAVE_X11
