#include <cv.h>
#include <vector>
#include <gst/gst.h>
#include <gst/net/gstnet.h>
#include <gst/gstbuffer.h>
#include <gst/video/video.h>
#include <gst/app/gstappsink.h>
#include <gst/pbutils/missing-plugins.h>

// for timings
#include <time.h>

class GstInput
{
  public:
    GstInput();
    ~GstInput();
    bool openRTSP(std::string laddress, std::string raddress);
    bool openFile(std::string laddress, std::string raddress);
    bool storeRTSP(std::string laddress, std::string raddress, const char * Lfilename, const char * Rfilename);
    bool grabFrames();
    bool retriveFrames(IplImage *  & OutFrame1, IplImage *  & OutFrame2, int & sync);
    bool getFrameSize(int & w1, int & h1, int & w2, int & h2);
  private:
    bool open(std::string laddress, std::string raddress);
    bool reopen();
    void close();
    bool isPipelinePlaying();
    void startPipeline();
    void stopPipeline();
    void restartPipeline();
    void setFilter(const char* prop, int type, int v1, int v2 = 0);
    void removeFilter(const char *filter);
    static void source_created (GstElement * pipe, GstElement * source);
    static void newPad(GstElement * myelement, GstPad * pad,gpointer data);
    static void on_pad_added (GstElement *src, GstPad *new_pad, GstElement *rtph264depay);
    GstClock *global_clock;
    gchar *server1;
    gchar *server2;
    gint clock_port1;
    gint clock_port2;
    GstElement *pipeline1;
    GstElement *pipeline2;
    GstElement *uridecodebin1;
    GstElement *uridecodebin2;
    GstElement *color1;
    GstElement *color2;
    GstElement *sink1;
    GstElement *sink2;
    GstElement *sink1f;
    GstElement *sink2f;
    
    GstSample *  sample1;
    GstSample *  sample2;
    GstBuffer * buffer1;
    GstBuffer * buffer2;
    GstMapInfo*   info1;
    GstMapInfo*   info2;
    
    gint64        duration;
    gint          width1;
    gint          width2;
    gint          height1;
    gint          height2;
    double        fps1;
    double        fps2;
    
    GstClockTime  pts1;
    GstClockTime  prev_pts1;
    GstClockTime  curtime1;  
    GstClockTime  pts2;
    GstClockTime  prev_pts2;
    GstClockTime  curtime2;  
  
    GstCaps*      caps;
    
    // for opencv display
    IplImage*     frame1;
    IplImage*     frame2;  
  
    // frame buffers, in the future
    //std::vector<char *, GstClockTime> LFrameBuffer;
    //std::vector<char *, GstClockTime> RFrameBuffer;
    
    // for timings, JUST DEBUG
    struct timespec starttime;
    struct timespec lasttime;  
  
};

