#ifndef RENDER_H
#define RENDER_H
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>



typedef union {
    float ratio;
    int width;
    int height;
} aspect_ratio_t;


/* YUV formats in preferred order. */
//aspect_ratio_t aspect_ratios_list[] = {{ratio: 1.3333, width: 800, heigh: 600},{ratio: 1.3333, width: 800, heigh: 600}};




enum VideoRenderType
{
    NONE,
    QT,
    X11,
    WIN
};

class VideoRender
{
public:
    VideoRender();
    virtual ~VideoRender(){};
    virtual void render(cv::Mat & frame)=0;
    virtual void setDrawable(int d)=0;
    virtual void setSourceResolution(int w, int h)=0;
    virtual void cleanup()=0;
};
#endif

